The week Dan and John talked about

* John feeling acidic on the cusp of a cold ([[[Aging]]])
* John's mom wanting to sell her house asking for a sawzall ([[[Family]]])
* Bathrooms with electric hand blowers ([[[Attitude and opinion]]])
* Facts about LSD, acid and other drugs, getting "dosed" ([[[Drugs]]])
* John getting dosed on LSD in the past and meeting a Husky dog ([[[Drugs]]])
* Follow-up on John's plans to sell things on Craigslist ([[[Objects]]])
* The Roadwork [https://patreon.com/roadwork Patreon]

The show title refers to the feeling John had on an LSD-trip when he felt it was going the wrong way.