When John's [[[daughter]]] was little she said some words slightly wrong which was so cute that it has become part of John's and Merlin's vocabulary. 

Examples: 

||aminals||animals||
||bastic||basket||
||bosposably||supposedly||
||breckdast||breakfast||
||backack||backpack||
||interdasting||interesting||
||let me ax you dis||let me ask you this||
||ponadoes||potatoes||
||punkin||pumpkin||
||skapetti||spaghetti||