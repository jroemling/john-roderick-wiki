This week, Ken and John talk about:
[[toc]]

+ John’s family around 1970 living in Kingston (OM419)

In the summer of 1970 John’s sister was not born yet and John was 18 months old, wearing a blue corduroy overall. They were living in Kingston, Washington right across the straight from beautiful Edmonds, hometown of Ken and of Rick Steves. John was having a lot of tea parties with Bonnie, the girl next door, and they would convene every day. That was the big Tonka toy era and John had a dump truck. There are a lot of black & white photographs of the two of them sitting at a little table.

Their house sat up on a bluff on the water, overlooking the ferry terminal. It is still there, but it has been dramatically modified with a blue aluminum roof //(maybe 10939 NE West Kingston Rd?)// and when you come in with the ferry you can really see it now over to the left halfway up the bluff. They call it high bank now instead of mid bank, which is important when you buy waterfront property in Washington.

When John’s family lived there it was just a ramshackle bunch of sheds that had been connected. When they bought it it had 20 rooms, but they were all 6x11 //(180x330cm, ca 6 sqm)// and they had to tear out all the walls. Who built this and why? Who wanted this David Lynch-ian burrow? It was in Kingston, what are they doing out there?

+ Old photo and video mediums (OM419)

At the time around 1970 John's parents were still using only black & white cameras and there are no color pictures of John from that period. Maybe there is some Kodachrome stuff that other people took, but until they moved to Alaska John’s parents were classic black & white people. There is 8mm film of him that never got transferred to video and is now gradually decaying. Ken never had anything pre-VHS and even those would need to be transferred now.

There are also audio cassettes of John babbling. His mom was fascinated by him sitting in his room all by himself and talking to himself for hours. Who could have predicted? A couple of times she would put a microphone in there and every 30 minutes he would stop for a Squarespace ad.

+ Ken’s parents around 1970 (OM419)

Ken was not born yet in the summer of 1970, he wasn’t even a twinkle in his parent’s eye. They weren’t even married yet, they got married in 1972, and he knows that because they saw What’s Up, Doc? on their honeymoon. They could have gotten with The Godfather, but they didn’t. They both went to Meadowdale High in Edmond, just a few miles away, and maybe they could see John’s family’s house from across the Meadowdale beach.

John wants to know the closest he was to Ken’s parents at any point in the late 1960s / early 1970s because surely he was getting off the ferry at one point when they were on their way to High School and they passed within 50 feet of each other and they were like: ”Look at that weird kid in their corduroy overalls and the big beard, just talking!” Future couples will be able to do that if they were holding a device that tracked their movements for decades.

There are stories of couples who after they got married find out that there are photos of them on Main Street at Disney Land where one of them is in the background of the other’s photo. It happens from time to time and Disney Land is often the nexus, or a presidential inauguration.

+ Portland in 1970 (OM419)

John’s aunt and uncle were living in Portland in 1970. They had a house in Gearhart //(see RL264)// and John’s family would go down there. Later they would talk about Portland in the early 1970s.

Ken was in Portland last weekend. There was an exhibit at an Oregon Historical Museum about early 20th century Portland beach culture. People would go to Seaside and Gearhart, and it would take the better part of a day to get there on some weird tram. Then the husbands would show up later on the weekend on the daddy train, which means something different in Portland today.