This week, Merlin and John talk about:
[[toc]]

**The Problem:** The love has gone out of pizza, referring to an article about an AI pizza maker, a robot that is automatically putting the toppings on the pizza and soon there will be no more pizzas made by humans and the love has already gone out of pizza.

The show title refers to a model for predicting hurricanes.

[[include inc:raw]]

+ Weather in Seattle, John following weather people on Twitter (RL368)

It is a little bit snowy in Seattle. John started following the weather-prediction nerds. As the technology has improved there is more and more science behind predicting the weather, which attracts a certain kind of person, not just meteorologists, but amateurs and prosumers. John’s mom is riding on the weather train while sometimes there are also amateur nights when there is a hurricane for example, like a (Mike) Squires. ”It is an Epiphone, it is not a full Gibson!” John is interested in forecast models, but also in the kind of people who are interested in forecast modeling, and there are a lot of different twitters, so John started following weather-nerd Twitter.

There are different models for predicting hurricanes, like the Euro model, the global forecasting system model, the Slovenian Model, there is a lot going on! There is a low and there is a high and the Arctic air is coming down the Fraser River Valley, colliding with the low over here, so these people have been seeing this coming for two weeks. You can tell that they want a weather disaster, they are so horny for the Arctic blast. They don’t want it to be a nice sunny day, but they want to see the heavens fall.

Merlin’s mom is in a hurricane area, so he has to monitor this stuff to make sure she is okay. You can see big events ramping up and when it stops getting worse, those people are so disappointed because it hasn’t been the most significant weather event this area has seen in 80 years. Today is very unusual in Seattle, according to the Seattle weather blog 16 minutes ago it was snowing in Downtown Seattle. In Everett there are two feet of snow on the ground. Big flakes in Kent, which sounds like a Guided by Voices song. But where John is it is bone-dry and 37 degrees (3°C), only a span of 20 miles apart.

John’s mom sits up in her Treetop Aerie and surveys the world ans is always talking about how Seattle is a convergent zone and she sees weather come in from here and there and she can pick out at 45th in Wallingford //(just North of the University of Washington)// there is a weather line and everything South of it is clear and everything North of it is wet. She loves that stuff! There was a period of about two years where when dusk would fall she would get in her car and follow the crows. She would position herself down South where she had lost them the last time and wait for them to come and chase them further.

This morning John woke up, thinking there is either 4 feet of snow on the ground and it is 17 degrees (-8°C) or the whole thing was bunkum, three weeks of people reading the tea leaves and throwing animal bones into a fire and it ends up being impossible to predict the weather and that is what happened and Northwest weather Twitter is so mad, rattling their jingle sticks. John is delighted in it, although some of his friends at the other side of town are snowed in. It is interesting to live where John is living now because he is in a different weather zone. There is Seattle weather and there is the weather station at the airport and those report a different world. John is now living in the airport world instead of the Downtown world and goes through a little adjustment period.

Merlin compares weather reports with election polls. A national election poll is just as useful than a national weather report. Also, the more precise your measuring device, the longer the coastline of Scotland gets. In the same way, there is a lot of weather in Seattle and also: By the time you report it, it is already old. Merlin uses Dark Sky, which does quite good hyper-local forecast and it predicts an inch of rain on Thursday which is quite a lot and by the time Thursday comes around it is usually less than that.

There are seats in congress that you can get elected to with 50.000 votes, but for other seats you need 500.000 votes to get elected, which is pretty astonishing. John got 30.000 votes when he ran for office.

Here are some weather models: 
* ECMWF HRES, +/- 9 km
* ECMWF EPS, +/- 14 km
* ICON, +/- 13 km
* GFS (Global Forecasting System), the one John uses, +/- 23 km
* GEFS +/- 46 km
* UKMO
* GEM
* Access-G
* ARPEGE
* for America only you have the NAM and the HRRR, +/- 3 km
* In Europe there is Super HD and the Swiss EU 4x4
* AROME
* Cosmo D2
* euro4
* ICON-EU
* HIRLAM FMI
* HIRLAM KNMI

All of these models are currently giving different forecasts and projections of what is going to happen in the weather. ”What is that going to do?” has to be the third-most common thing that goes into a human being’s head. If Merlin would walk out the door of his office and turn left and right he would see something that is going to inspire his brain to say: ”What is that going to do?” Last year a car ran into a building down by the hand-job place. They fixed it, but they still haven’t painted it.

One time John was driving by the airplane museum and the car in front of him drifted slowly over the lanes in front of him and smashed into the elevator of the airplane museum. The driver passed out and the car went slowly to the side like it was making a turn //(see RW15)//

+ Camera lenses (RL368)

Somebody posted a thing the other day with a link to somebody who was talking about Inbox Zero like it was their idea. Then there is the occasional semi-annual take-down about why the thing is not a thing. Merlin has always been fascinated by cameras, he is a piker and he is taking some great photos. He is interested in lenses. If you have three different lenses you probably have that crappy super-plastic zoom that people like to use, the lens that comes with your camera.

Then you buy a 50mm prime lens that will be very specific about the field you are capturing, it is about what the human eye sees and is pretty undistorted with a nice fuzzy background when you get good with it. Then you get a Beastie Boys fisheye lens, a very low millimenter lens, sub-20mm, and another kind of lens is a macro lens where you can zoom in on something. Those give you three very different versions of the truth. With the 50mm you don’t have zoom, but your zoom is your feet, like in life. A lot of Merlin’s best photos are taken with a pretty OK prosumer 50mm lens.

If you take your Beastie Boys lens because you want to get everything in the picture you are going to get this obviously distorted and spherically looking thing. Like real estate photography, there is a Tumblr called Terrible Real Estate Agent Photos //(see [https://terriblerealestateagentphotos.com/ here])// But the great real estate photography by professional real estate photographers, they have lenses that enable them to show the scope of the room without it appearing to have fisheye distortion, but it subtly distorts the proportions of the room.

Merlin concludes that every decision you make involves you moving up and down some axes: If you want to see everything it has to be distorted, if you want to see something in huge detail it is going to be limited. That is also how weather or polls work. Time takes a cigarette and puts it in your mouth, it is the cilantro you didn’t know you had. If you go out, maybe it is flooding, maybe it is not, this goes straight back to Heraclitus who said you never step into the same river twice. John thought that was Siddhartha (by Hesse, John is correct, it is from that book, but it is taken from Heraclitus). Maybe John is thinking of Wes Anderson who uses a 40mm lens. There are very rarely straight vertical lines in a Wes Anderson movie. With time, Merlin takes a picture of his kid with his 50mm, but now she is older. 

Merlin had mentioned sneaker net, where you have to take some document from one computer to another and because there was no network you had to put it on a floppy disc and walk there. That is example of a thing that John either never heard of before or he has heard it 1000 times and had no idea what people were talking about. John thinks that is hilarious and people don’t give computer people enough credit for being funny. There is a thing called The Jargon File where you can find a lot of these.

Whether you are talking about weather, photographs, floppy discs or elections, you have to understand what you are trading. If you want extreme specificity you get a very limited picture, plus time. If you want to have everything, no matter what you choose you give something away. And if you put your photos on a SanDisk Ultra 2 GB memory card, then time will eventually make those photos impossible to access. The same with CDs: If you buy a copy of R.E.M.’s Murmur, that is going to be on a CD that has been burned at a place, but if you get it off Limewire and burned it at your home, all you are doing is distributing ink on plastic and if you pop that in right now... It also goes for Reckoning //(also by R.E.M.)//, ”They crowded up to Lenin with their noses worn off” //(lyrics Harborcoat by R.E.M.)//, you are not going to get back to Rockville //(song by R.E.M.)//

+* weather (cont)

Merlin wonders what 14 km resolution on a weather report means. John has an app called MyRadar and when he got it it seemed like that was the hottest thing going, but now radar is table-stakes. You can sit and watch fairly within the last hour up to 5 minutes ago, not quite realtime, and you see a little dot with your position. It is pretty cool because when big storms are happening you can follow them there. Right now John is in a tiny window of nothing happening, but he doesn’t know about the resolution, it appears to be showing every individual raindrop. Hindsight vision is always 20/20. Nobody needs yesterday’s weather report, sell it to the San Francisco Chronicle!

You always get the predicted temperature range and the probability of precipitation. The meaning of this according to the National Weather Service is simply the probability that this forecast grid will receive at least 0.01 inches of rain, the percentage chance that it will rain over a given area, not necessarily over your house.

The average global rate of rainfall is on the order of 10^6 m^3/s and the residence time of a water molecule in the atmosphere is about 10 days, which suggests that it is always raining somewhere. There are 2000 thunderstorms occurring at any given time and 100 lightning strikes per second on Earth at any given moment. The first thing John thinks of when talking about weather report is Jacob Astorius (?) or Al Di Meola, and the second thing is how this affects John. Merlin saw him in concert once.

+ Skinny mirrors (RL368)

Merlin sends John a thing about lenses that shows what different types of lenses do to a human head. There is this old saying that television adds 20 pounds, but that is not just because you got successful and could afford food, but the kinds of lenses they are using. You can now even buy skinny mirrors for your house. John lives in a world where mirror-buying decisions are based largely on how they perform according to the skinny mirror tests. It is 100% a season-to-taste effect, like you can’t say what pornography is, but you know it when you see it. Merlin has been in hotels where he knew that is not how he looks.

John feels instinctively that he doesn’t want a skinny mirror, but Merlin would love to have one! John wants the hard, unvarnished truth! Give me the unvarnished truth and then he will put varnish on it! You can’t look in the mirror with the unvarnished truth and then just walk out in the world with the unvarnished truth, that is like ”I am mad as hell and I don’t give a damn!” //(actually: ”I’m mad as hell and I’m not going to take it anymore” by the longtime news anchor Howard Beale in the 1975 film Network)// Like the great Irish //(Merlin says Scottish)// philosopher George Berkeley //(Merlin says Charles)//, we are talking about post-Descartes and we are talking about perception. You have to look out for your chicken!

+ Chickens (RL368)

You get to choose the varnish and for a lot of people the only varnish they put on is a hat, like putting a hat on a chicken. Merlin will put a helmet on a chicken, but John disagrees. They had done an episode of the Omnibus about a chicken that had been beheaded //(see OM132: Mike, the Headless Chicken)// and lived for 18 months until he choked on a kernel of corn. They continue to talk about how he could have been saved.

John’s mom did not have a lot of kind words for chickens and she still doesn’t. She would denigrate the chicken as a creature on God’s Earth. Dave Bazan’s wife Ann-Christine has chickens that she cares for like they were cats that live outside. She loves them! John knows a lot of people who pet and cuddle their chickens. John has found that although they are fun to hold, he does not see in a chicken’s eyes any recognition of anything. You don’t look into a chicken’s eyes and see a consciousness. They will get letters from people who say that chicken have curiosity.

+ AI Pizza maker (RL368)

This morning there was [https://www.geekwire.com/2020/video-pizza-making-robot-arrives-ces-feed-hungry-attendees-thanks-seattle-startup/ an article] about an AI pizza maker that was just a machine making pizza, but there was no AI about it. It wasn’t even a good machine and wasn’t even making good pizza. Surely a young person thought that no-one had ever invented a pizza-machine, which is characteristic of that period in a person’s life from 25-35 years. Like Elon Musk trying to reinvent subways by building a tunnel. If a pizza making machine comes into your head, why would you look if there already were pizza-making machines, but the first thing you would do is try to get funding. You are going to want to market it before you make it. 

Making a pizza machine is pretty hard, not the part where you put the stuff on the pizza, but taking some wheat and some water to make pizza dough and forming it into a pizza shape. What we need some AI for is a pizza throwing machine. John would pay for that! Merlin interjects that a lot of pizza throwing is sense of feel. The hard part was already done, the dough was just laid out by people or some other machine and this video was just showing an arm come out to squirt sauce, moving like one of these 3D-printers.

The article was emblematic of a future world where this machine is going to make pizzas and within a year you won’t be able to find a pizza made by a human. All the love has gone out of pizza already, except for the rare occasion. John can’t think of a place where you get pizzas made with love. Merlin’s family eats pizza and the problem is that pizza has a very low floor: If it is bad it can be super-fucking bad! Sometimes when they get pizza Merlin will get meatballs, which is a high-floor item: It is hard to fuck up a meatball. John feels like he eats fucked-up meatballs all the time. San Francisco is not a good pizza town and Merlin misses the pizzas of his childhood.

Merlin went to the Apple Store yesterday and then went to McDonals as a reward and got a Quarter Pounder. It was really good, it was with fresh meat, he paid with Apple Pay and it was great!

+ Denny’s Double Cheeseburger is the best hamburger (RL368)

John says that the best hamburger, eat the pickles out of the jar and don’t put them on the hamburger, better than the one your mom makes back when moms made hamburgers, better than from the guy with the apron that says: ”Kiss the cook!”, better than a hamburger from a hamburger truck, is the Double Hamburger from Denny’s. People John’s generation have been up and down with the Rolling Stones, except in this case it is Denny’s. When they were young they would go to Denny’s to smoke and to sit with your friends all night long. Then there was a phase in the 1990s when Denny’s was racist, before things were even called racist. Not Sambo, that was a different thing, although Merlin concatenates those. He might be an Indian boy on top of an elephant.

Denny’s was racist in the 1990s because there was an exposé that revealed that at a management level they would give different service to African Americans. Merlin thinks of Cracker Barrell (?) discriminating against the gay, that was also a thing, but they were upfront about it, while Denny’s denied that they would do that. They cleaned up their act a long time ago. Then for a long time they wouldn’t eat Domino’s because he was an anti-abortionist, that was when Merlin was in college. John took two Domino’s to a Willy Nelson (?) concert one time and he was the hero of the night, at least for the five people standing around him.

Merlin and his friend went out bowling and then went out to dinner and they got pizza for the table, the technology that keeps on giving, and unsurprisingly it was a huge hit. They all agreed on cheese and Merlin had two Old Fashioned (cocktail).

John thinks you can’t be in America and not find a Denny’s. He doesn’t know if there is a Denny’s in every American State. John has not eaten a hamburger in every Denny’s and a lot of them are franchises, so it is possible that some franchise owner is short-cutting it, but he has been to a lot of Denny’s and compared the hamburgers. Nobody is more embarrassed than John that he even went into a Denny’s in the first place, let alone ordered a hamburger, let alone wanting to brag about it, but he is bragging about it now because the only other people in Denny’s are people who have to be there, while he is there by choice.

It is like a DMV with eggs, but once you go there you realize that people who are there are actually happy to be there because it is the last place where you can get a burger, a milk shake, a pie, fries, you can’t smoke anymore, but there is every chance that your server is going to have multiple piercings or ear gauges. Denny’s has come all the way and it is extremely diverse! It is like a microcosm, a melting pot, and they are melting delicious cheese.

The Double Cheeseburger looks like on the website and tastes even better than it looks. It is a 40-napkin burger: you take a bite out of it and have to take a shower. You have to wear a raincoat when you eat it, or a grease coat. You might look like a flasher, but everybody is welcome at Denny’s. If you eat this hamburger, the Swiss Euro 4x4 model is going to predict tremendous precipitation of your taste buds! Merlin is getting a little wet in his zone right now.