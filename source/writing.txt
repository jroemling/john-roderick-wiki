John is an avid writer and has written many articles for various online publications.

+ Book publications

* 2010-09-29 [https://www.publicationstudio.biz/books/electric-aphorisms/ Electric Aphorisms by John Roderick], available as Softcover.
[!-- Old link: https://publication-studio.myshopify.com/products/john-roderick-literature-fiction-nonfiction?variant=18797940934 --]

+ Videos

//John recorded a video series called [[[Hey Seattle |#HEYSEATTLE]]] for [https://www.youtube.com/channel/UCgSt-U04-NkYglXh0O4DRSA VISITseattle.tv] at the beginning of 2017//

* 2019-02-10 [https://www.instagram.com/p/BtrcJrWl7Wm/ Couch Riffs], playing Bringin' On The Heartbreak by Def Leppard, with Mike Squires
* 2018-03-19 [https://www.youtube.com/watch?v=EzLiYCqEfGk KCTS9 Ad] together with Ken Jennings
* 2016-09-10 [https://xoxofest.com/2016/projects/john-roderick XOXO 2016: John Roderick], cited as [[[XO2016]]]
* 2015-05-17 [https://www.youtube.com/watch?v=gqgRPvIZ59g John Roderick - Seattle City Council pos. 8]
* 2015-04-06 [https://www.youtube.com/watch?v=iYAi4llOUw8 Roderick on the Line live on JoCo Cruise 2015]
* 2014-12-12 [https://www.youtube.com/watch?v=ifnVYNcqzQc&t=1053s Lennon or McCartney], starting at 17:33
* 2013-03-01 [https://www.youtube.com/watch?v=-RqS-ulgd2I John Roderick and Friends at Seattle Interactive Conference 2012]
* 2012-12-04 [https://www.youtube.com/watch?v=qwFOGWWri6E Ken Jennings' Because I Said So! Labs]
* 2012-10-30 [https://www.youtube.com/watch?v=8a_wzZAyqno David Shing interviews John Roderick and Friends at Seattle Interactive Conference]
* 2012-04-05 [http://www.livewireradio.org/content/clips-episode-179-susan-orlean-john-roderick-and-portland-cello-project Live Wire Radio]
* 2011-11-07 [https://youtu.be/gX2eEICejB0 Jonathan Coulton - Nemeses] Official Music Video
* 2009-04-24 [https://vimeo.com/4320388 The Long Winters in…Packin’ Heat] by Adam Pranica
* 2008-12-08 [https://www.amazon.com/Long-Winters-Live-Showbox-Pranica/dp/B01GUOYMT4 DVD: The Long Winters LIVE at the Showbox] by Adam Pranica

+ Regular columns

++ Seattle Weekly
//John’s regular monthly column at [http://archive.seattleweekly.com/search/?q=John%20roderick&type=news&sort=PUBLISHED&dir=DESC&days=all Seattle Weekly] (link shows more than that)//
//Other articles by John published in the [http://www.seattleweekly.com/author/user-john-roderickseattleweekly-com/ Seattle Weekly].//

* 2013-02-27 [https://www.seattleweekly.com/music/for-those-of-us-who-grew-up-in-the-shadow-of-the/ Punk Rock is Bullshit] (see another copy [http://www.johnroderick.com/new-page-1 here] and listen to John's comment on [[[RL62]]])
* 2012-10-02 [http://archive.seattleweekly.com/music/reverb/872570-129/reverbmonthly Answers & Advice: Showbiz Ain't Friend-Biz!]
* 2012-07-31 [http://www.seattleweekly.com/home/872935-129/reverbmonthly Answers & Advice for Jay Inslee and Aerosmith]
* 2012-07-02 [http://www.seattleweekly.com/home/873091-129/reverbmonthly Answers & Advice: Blow Harder!]
* 2012-06-06 [http://archive.seattleweekly.com/home/921570-129/johnroderickreverbresidency Answers & Advice: Helping Rosanne Cash With Porta-Potties, and more]
* 2012-06-05 [http://www.seattleweekly.com/home/873270-129/reverbmonthly Answers & Advice: Cash to Roderick]
* 2012-05-01 [http://www.seattleweekly.com/home/873494-129/reverbmonthly Answers & Advice: Surviving Forklife]
* 2012-04-03 [http://www.seattleweekly.com/home/873679-129/reverbmonthly Answers & Advice: Coming to America]
* 2012-02-28 [http://www.seattleweekly.com/music/roderick-irony-or-seriousness/ Roderick: Irony or Seriousness?]
* 2012-01-31 [http://www.seattleweekly.com/music/answers-advice-real-gangster/ Answers & Advice: Real Gangster]
* 2011-10-19 [http://www.seattleweekly.com/music/john-roderick-is-a-regular-columnist-for-reverb-his-band-the-long/ Reverb: Title unknown]
* 2011-10-04 [http://www.seattleweekly.com/music/answers-advice-seattles-stalinism/ Answers & Advice: Seattle's Stalinism]
* 2011-09-06 [http://www.seattleweekly.com/music/centro-matic-guided-by-veterans/ Centro-Matic: Guided by Veterans]
* 2011-03-08 [http://www.seattleweekly.com/music/joy-division/ Joy Division]
* 2011-03-01 [http://www.seattleweekly.com/music/hacking-it/ Hacking It]
* 2011-02-22 [http://www.seattleweekly.com/music/roderick-reason-vs-faith/ Roderick: Reason vs Faith]
* 2010-08-31 [http://www.seattleweekly.com/music/myth-61-revisited/ Myth 61 Revisited]
* 2009-09-01 [http://www.seattleweekly.com/music/bumbershoot-america%C2%92s-next-long-winters-drummer/ Bumbershoot: America's Next Long Winters Drummer]
* 2009-02-03 [http://www.seattleweekly.com/music/john-roderick-meets-the-new-croc/ John Roderick Meets the New Croc]
* 2008-03-11 [http://www.seattleweekly.com/music/why-i%C2%92d-rather-hang-with-alan-parsons-than-my-indie-rock-peers/  Why I'd Rather Hang With Alan Parsons than my Indie Rock Peers]

++ The Stranger
//all articles by John on [https://www.thestranger.com/authors/3911/john-roderick The Stranger]//

* 2017-05-19 [https://www.thestranger.com/slog/2017/05/19/25154959/remembering-chris-cornell-as-he-was-on-the-brink-of-soundgarden-dropping-1991s-badmotorfinger Remembering Chris Cornell]
* 2015-07-10 [https://www.thestranger.com/blogs/slog/2015/07/10/22521714/guest-editorial-tim-burgesss-gun-tax-proposal-is-pure-politics-and-not-a-real-solution Tim Burgess's Gun Tax Proposal Is Pure Politics and Not a Real Solution]
* 2001-12-20 [https://www.thestranger.com/seattle/o-tenenbaums/Content?oid=9620 O, Tenenbaums - Gene Hackman's Royal Blues]
* 2001-05-31 [https://www.thestranger.com/seattle/family-union/Content?oid=7541 Family Union - Romantic Protest Movie Strikes a Chord]
* 2000-06-01 [https://www.thestranger.com/seattle/activist-afterglow/Content?oid=4065 Activist Afterglow - WTO Documentary Asks the Right Questions]
* 2000-05-11 [https://www.thestranger.com/seattle/sexy-grandpa/Content?oid=3912 Sexy Grandpa - When Old People Fuck Like Rabbits]

++ Talkhouse
//all articles by John on [http://www.talkhouse.com/artist/john-roderick/ Talkhouse]//

* 2015-12-08 [http://www.talkhouse.com/john-roderick-the-long-winters-talks-the-criteria-for-a-good-christmas-song/ Criteria for a Good Christmas Song]
* 2014-12-20 [http://www.talkhouse.com/john-roderick-the-long-winters-talks-holiday-albums/ John Roderick Talks Christmas Music]
* 2014-04-24 [http://www.talkhouse.com/john-roderick-long-winters-talks-eels-the-cautionary-tales-of-mark-oliver-everett/ Eels - The Cuationary Tales of Mark Oliver Everett]
* 2014-01-24 [http://www.talkhouse.com/john-roderick-talks-the-2014-grammy-awards/ The 2014 Grammy Awards]
* 2013-12-05 [http://www.talkhouse.com/john-roderick-talks-kelly-clarksons-wrapped-in-red/ Kelly Clarkson - Wrapped in Red]
* 2013-05-03 [http://www.talkhouse.com/john-roderick-of-the-long-winters-talks-michael-bubles-to-be-loved/ Michael Bublé - To Be Loved]

+ Other articles written by John

* 2017-01-01 [http://www.seattleschild.com/The-Kids-Are-Alright/ The Kids Are Alright] on Seattle's Child
* 2015-07-15 [https://www.geekwire.com/2015/commentary-seattle-needs-new-political-leaders-who-get-tech-but-arent-running-windows-vista/ Seattle needs new political leaders who get tech, but aren’t running Windows Vista] on GeekWire
* 2014-05-28 [http://www.laweekly.com/music/why-im-not-a-fan-4751304 Why I'm Not a Fan] on LA Weekly
* 2013-11-07 [http://www.rollingstone.com/music/news/how-an-indie-rock-label-saved-my-life-20131107 How an Indie-Rock Label Saved My Life] on RollingStone

+ Articles about John

* 2018-03-27 [https://www.seattlemet.com/articles/2018/3/27/ken-jennings-and-john-roderick-are-preserving-history-for-after-the-apocalypse Ken Jennings and John Roderick Are Preserving History for After the Apocalypse] on SeattleMet
* 2017-12-07 [http://www.newsweek.com/apocalypse-coming-ken-jennings-podcast-here-prepare-us-740324 The Apocalypse is Coming (Apparently) and Ken Jennings' Podcast is Here to Prepare Us] in Newsweek
* 2017-11-25 [http://knkx.org/post/meet-seattle-musician-who-wants-make-america-great-again-through-song Meet The Seattle Musician Who Wants To 'Make America Great Again' Through Song] on KNKX
* 2016-11-18 [http://magnetmagazine.com/2016/11/18/magnet-classics-the-making-of-the-long-winters-when-i-pretend-to-fall/ Magnet Classics: The Making of The Long Winters’ When I Pretend to Fall”] on Magnet Magazine
* 2016-11-03 [http://www.1077theend.com/blogs/branden/long-winters-release-first-new-song-10-years-and-its-about-donald-trump The Long Winters release first new song in 10 years and it's about Donald Trump] on 107.7 the end 
* 2015-11-23 [https://www.indivisible.us/john-roderick/ John Roderick, Musician, Songwriter, and Podcaster] on Indivisible
* 2015-07-01 [https://www.seattlemet.com/articles/2015/7/1/john-roderick-takes-the-political-stage John Roderick Takes the Political Stage] on SeattleMet
* 2015-05-12 [https://www.billboard.com/articles/news/6561345/john-roderick-seattle-city-council-death-cab-for-cutie-the-long-winters Seattle Rock Vet John Roderick Hangs Up His Guitar for the Political Trail] on Billboard
* 2015-05-12 [https://www.stereogum.com/1801333/the-long-winters-john-roderick-is-running-for-seattle-city-council/news/ The Long Winters’ John Roderick Is Running For Seattle City Council] on Stereogum
* 2015-01-21 [https://www.thebillfold.com/2015/01/the-business-of-creative-careers-john-roderick-musician/ The Business of Creative Careers: John Roderick, Musician] on Billfold
* 2014-01-27 [https://www.filson.com/blog/profiles/john-roderick-filson 30 Years in Filson: A Musician’s Story] on the Filson blog
* 2013-08-19 [http://www.themonarchreview.org/the-monarch-drinks-with-john-roderick/ The Monarch Drinks With John Roderick] on The Monarch Review
* 2013-05-02 [http://cariluna.com/writer-with-kids-john-roderick/ Writer, with Kids: John Roderick] on Cari Luna
* 2013-01-01 [http://pmc-mag.com/2013/01/john-roderick/ AS YOU DO] on PMc Magazine
* 2012-12-25 [https://www.fretboardjournal.com/video/twelve-days-fretboard-john-roderick/ Twelve Days of Fretboard: John Roderick] on Fretboard Journal
* 2012-12-14 [http://www.citywinery.com/newyork/one-christmas-at-a-time-an-evening-with-jonathan-coulton-and-john-roderick-12-14.html One Christmas at a Time- An Evening with Jonathan Coulton and John Roderick - 12/14] on City Winery
* 2012-11-30 [http://www.dallasobserver.com/music/the-long-winters-john-roderick-talks-christmas-music-becoming-mayor-of-seattle-7076431 The Long Winters' John Roderick Talks Christmas Music, Becoming Mayor of Seattle] on Dallas Observer
* 2011-09-26 [http://www.cityartsmagazine.com/issues-seattle-2011-10-qa-john-roderick-long-winters/ Q&A: John Roderick of the Long Winters] on CityArts
* 2011-03-08 [http://archive.seattleweekly.com/home/915539-129/story.html Stream Shelby Earl's New John Roderick-Produced Record, Burn the Boats] on Seattle Weekly
* 2011-01-21 [http://www.43folders.com/2011/01/21/meet-hotrod John Roderick on String Art Owls, Copper Pipe, and Bono's Boss] on 43 Folders
* 2010-07-08 [https://www.thestranger.com/lineout/archives/2010/07/08/this-one-time-john-roderick-drove-me-all-over-seattle-and-told-me-everything-he-knows-about-the-city This One Time John Roderick Drove Me All Over Seattle...] on The Stranger
* 2010-07-05 [https://www.newyorker.com/magazine/2010/07/05/hello-lamppost Hello Lamppost!] on The New Yorker about the concert where John played with Aimee Mann
* 2008-12-16 [https://www.pastemagazine.com/blogs/lists/2008/12/5-lyrics-that-prove-john-roderick-is-a-better-song.html 5 Lyrics That Prove John Roderick Is A Better Songwriter Than Your Favorite Songwriter] on PastE
* 2007-06-21 [https://www.riverfronttimes.com/musicblog/2007/06/21/the-long-winters-john-roderick-doesnt-hate-everything-or-the-commander-eats-aloud The Long Winters' John Roderick Doesn't Hate Everything -- or, The Commander Eats Aloud] on Riverfront Times
* 2005-06-01 [https://www.believermag.com/issues/200506/?read=interview_roderick Interview with JOHN RODERICK] on Believer

//Several articles in [https://houselist.bowerypresents.com/tag/john-roderick/ The Bowery Presents]//

+ More resources

* [http://www.azquotes.com/author/45383-John_Roderick Quotes by John Roderick]
* [http://www.thelongwinters.com/blogs/john-roderick The Long Winters Blog]
* [https://www.setlist.fm/setlists/john-roderick-43d69bd7.html Set Lists by John Roderick]
* [https://www.setlist.fm/search?query=the+long+winters Set Lists by The Long Winters]
* [http://new.thelongwinterslibrary.com/ The Long Winters Library and Archive] by Liesbeth Rijnierse
* [https://journal.brettchalupa.com/2017/06/29/roderick-on-the-line-listening-guide/ Roderick on the Line Listening Guide] by Brett Chalupa
* [https://www.amazon.com/Roderick-Line-Guide-Erin-Dawson-ebook/dp/B07BCTRXWY Roderick on the Line: A Guide] by Erin Dawson
* [http://rotl.tumblr.com/ Roderick on the Line Tumblr] by Daniel Jackson and Jade Gordon
* [http://fuckyeahjohnroderick.tumblr.com Jolly Good! John Roderick], a fan blog about Roderick on the Line by Jade Gordon
* [https://fyroadwork.tumblr.com/ fyroadwork], a fan blog about Road Work by Jade Gordon
* [https://soundcloud.com/poly915/rotl-all-the-great-show-titles-0-to-299 All The Great Show Titles], a supercut by Paul DeCarli
* [https://soundcloud.com/poly915/heres-the-thing-about-roderick-on-the-line Here's the Thing about Roderick on the Line], a supercut by Paul DeCarli
* [https://twitter.com/NoContextRodOn No Context Roderick On The Line], a Twitter profile posting random snippets
* [https://twitter.com/OmnibusOOC Omnibus out of Context], a Twitter profile posting random quotes
* [https://open.spotify.com/user/1230923339/playlist/2MmwKIoFKB3WR28qm6seo8?si=A1bJ30B2TWeV1AukrJEFAQ All song references from Roderick on the Line], a Spotify playlist by Skip Recap

++ Other websites similar to this one

* [https://fluiddata.com/channel/170500/roderick-on-the-line Roderick on the Line Podcast Search] by the FluidDATA Podcast Search Engine
* [http://podcastsearch.david-smith.org/shows/7 Roderick on the Line Podcast Search] by [https://twitter.com/_davidsmith _David Smith]
* [http://wecancutthisout.com We can cut this out] by [https://twitter.com/prestemon Eric Prestemon]
* [https://roderickon.fandom.com/wiki/Roderick_on_the_Line_Wiki Roderick on the Line Wiki] 
* [https://github.com/devinbrady/transcribe-podcast/blob/master/transcribe_podcast.py Transcribe Podcast script] using Google Cloud Services ([https://cloud.google.com/speech-to-text/docs/async-recognize doc here])
* [https://drive.google.com/drive/folders/1fU9M_niPYPzdDKiXH1wh-mO6qF9iMaZz?fbclid=IwAR3Nv1F2BSZeoQ0lj6fyM9whi9M87hL16TEaNSNQGGEOdP6MxVWXHAA_fDM Episode Transcription Initiative] by [https://www.facebook.com/wilson.angus.m Angus Wilson], see [https://www.facebook.com/groups/garysvan/posts/1084415171768667/ original post] (membership in Gary's Van required)