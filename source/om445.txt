This week, Ken and John talk about: 
[[toc]]

+ Formative natural disasters, earthquakes (OM445)

While John was living in Alaska as a kid there were two big earthquakes: One when he was in 5th-6th grade and one when he was in 8th grade, and both of them had a profound effect. The big 1964 9.0 record Earthquake was super-present in everyone’s mind and the wreckage of it was all around them. In 1974 it had only been 10 years. Whole villages had been swamped and had never been restored. The tidal wave picked up locomotives and moved them blocks into town, tore up the railroad tracks. You could see where things are trashed.

In particular there is a park by the airport called Earthquake park which used to be a neighborhood called Turnagain and during the 1964 earthquake the had ground liquified and the whole neighborhood just slid into the sea. Rather than rebuild it they left it as a park. People died, although surprisingly few, cracks opening up, people were falling in them. It was somber when John was a kid. Now it is so long ago, but there are explanatory plaques.

John’s uncle and aunt were living in Alaska in 1964 and they both had profound earthquake stories. A crack opened up and came at their house. John's uncle was in the mountains at the time and experienced an avalanche. Everybody had a story! When John experienced his first big earthquake in 5th or 6th grade that knocked him out of his chair, he was in the library in the school. You could hear it before you feel it, it sounds like a rumbling, and then it hits, the books spilled off the shelves. They had been raised to think that this would be The Big One. Even after the shaking stops you are under pure adrenalin!

In 8th grade John was sitting in class and he saw a wave come across the floor and pull up the tiles as it swung through the class and it knocked everybody off their chairs, they all fell down. John’s dad was driving and the phone poles were waving. Both of those earthquakes rocked John’s world.

Ken has never been in an Earthquake that was more than: ”Was that a truck going by?”, once in Los Angeles and one in Seattle. During the one in 2001 John was watching his neighbor’s chimney peel away from their house and completely fall on the house next door.

You never forget those in Alaska where the ground wasn’t solid anymore. Ken always wanted to be in one, but the overriding thing that happens is panic that this is The Big One and your city is gone, which is not unrealistic in Seattle or Alaska or Los Angeles. The one in Alaska shook for 5 minutes! No rollercoaster ride is that long!

Ken was 5 or 6 years old when Mount Saint Helens erupted in 1980 and he remembers the ash on the cars looking like snow, but when you are a kid you don’t think it is weird because there are volcanos in your dinosaur books and now there was one a couple hours outside of town.

John was 11 and his brother used to live on the slopes of Mount Saint Helens in a little moss-covered hippie cottage, he was driving logging trucks for a living at the time, and John went to visit him in the mid-1970s and when it blew up John had a very personal understanding what the side of that mountain looked like, but he was in Alaska at the time and didn’t get to see the ash.

Ken was already an adult by the time Hurricane Katrina happened and that was very visceral where you could sense what is happening to a city and a people, a ”you were there” moment, because when he was a kid the big hurricanes like the Andrews and the Hugos were just a quick succession on the cable news. 

Ken was watching an A’s Giants game on TV from Korea the day of the Loma Prieta Earthquake and suddenly Bob Costas or Greg Gumbel were like: ”Woah!” and then the picture went out. None of those really stopped Ken and made him think the way a natural disaster can.

John always secretly longs for them and when they happen he always guiltily wishes they had been bigger. He wants to ride the dragon as the world comes to an end. Ken thinks that this is one of the many things that he has never experienced that he is confident that he would be amazing at where others would be traumatized. Some people shriek and some people are very collected, and you don’t know which one you are until the world goes crazy.

+ The Exxon Valdez disaster (OM445)

Ken had the same kind of feeling during the Exxon Valdez news cycles, seeing all the poor birds and sea animals with college students trying to brush all the gunk off of them and showing the numbers of how many are going to die. That really shaped him, but this was not a natural disaster, God did not put a drunk captain on the big oil tanker.

John’s mom worked for the pipeline at the time in Alaska. She was not quite part of the first response, but she knew all the people, she knew everybody in Valdez, and she knew all the systems because she ran the computers for the pipeline. Afterwards the investigation came and six guys in blue suits filed into her office and she was like: ”Want to see the files?”

She walked them to the file cabinet where she had all of the disaster routines, everything that she had practiced 1000 times, and she was like: ”You are going to have to look elsewhere for where the mistakes were made, because they weren’t made here!” - ”Thank you, ma'm!” John’s mom is the most convincing person to turn away the men in black, and they just moved on down the hall. 

+ John being on top of the World Trade Center 11 days before 9/11 (OM445)

Ken had literal nightmares after 9/11, even though he was in his 20s. They were  acrophobic dreams with lots of height and girders. It was again not a natural disaster, it was perpetrated by man, but places you feel safe include Downtown skyscrapers and airplanes and Ken had spent lots of time in both and the idea that both of them were fundamentally unsure the way you felt about your classroom floor at school did a number on his psyche for a little while. When he went to New York for the first time a few years later there were already trucks scooping stuff out of a big square pit.

John was living in New York that summer and he went to the top of the World Trade Center on August 31st before he went back to Seattle because The Long Winters’ first album was done and he needed to put a band together to tour. In New York that would have been super-complicated because everything is really expensive.

He was used to living out in Seattle where you just leave your drum-kit set up, you go in any time because you have your own detached garage that has all your Christmas lights in it, while in New York you paid for a practice space by the hour and you had to put all your stuff in a locker and John just couldn’t get his head around it.

John had been living in New York for 6 months and he had never done any of the 4 clichéd things. Instead of going out to Coney Island he had a hot dog, rode the Cyclone, loved it, and went to the top of the World Trade Center with a friend. While they were up there they were talking about how you would blow this up because people had already tried it before and 11 days later it was gone! Not long after that they were on tour and John went back there when it was still on fire. It was a really bad scene!

+ Being on a site shortly before a disaster occurs (OM445)

Years before 9/11 John had been to the top of the Empire State Building, but she didn’t show up //(reference to the movie Sleepless in Seattle)//, he walked around and wondered how you could throw yourself off this. You couldn’t get over these fences! John was also there to see the dirigible docking station. Ken 100% went to the Empire State Building during his first time in Manhattan unabashed, not because he had seen Sleepless in Seattle, but because he had seen King Kong. He might not even have seen An Affair To Remember.

Forest fires are a thing now and the last time Ken was in Manhattan it was all hazy and you could not see the Brooklyn skyline because of California and British Columbia wildfires.

John was in New York with John Hodgman the day before Hurricane Sandy. They were driving in and all the big electronic signs on the Highway in New Jersey that normally say ”20 minutes to New York” said: ”Get off the Highway! Take shelter immediately!” and the sky was a weird color, but it was perfectly calm. They drove to the New York airport and got on a plane and the whole time they were wondering if they would cancel the flight, but they got off and flew to the West Coast and then the Hurricane hit that night.

In the Northwest they have Tsunami Sirens and just a couple of weeks ago there was an advisory when the Tanga volcanic event happened. Your phone will go off and Ken’s parents in Sequim are close enough to the ocean and their phone said that they were above the waterline, but they should keep watching this space. They also have volcano warnings, depending on where you are, and the best parts of Washington are where the Volcano Lahar and the tsunami meet. There is also the Seattle Fault, and they all meet in Tacoma Washington.

John has been in Lisbon and it looks like it has been there forever, like a place in the Star Wars universe that has been artificially aged, but the historic old town of Lisbon is not newer than November 1755.

+ John finding a sidewalk elevator to the Seattle Underground Tour (OM445)

One time John was in Pioneer Square, walking down the sidewalk, and in the middle of the sidewalk in front of the Merchant’s Café there was a railing and he realized it was an elevator that had come up through the sidewalk. Those metal plates open up like a clamshell and an elevator comes up, but this elevator was just siting unguarded, John stood on it, there was a button, John pushed it, and it took him down.

He was in the Seattle Underground Tour and nobody was there, so John gave himself the Seattle Underground Tour, creeping around, sneaking around down there. There were rats, it was freaky! This story sounds perfect for John and if it hadn’t happened to him he would have to make it up. He had never seen the Seattle Underground Tour, but he can’t go on the tour, being John Roderick.

+ John thinking about living in Portugal (OM445)

John is one of the Americans who wants to retire to inexpensive Portugal, he keeps hearing that Portugal is the cheapest place to live in Europe, and all the Indie Rock stars of his generation are zeroing in on Portugal. He is already in communication with their Portuguese listeners and he got a couple of emails from them, saying that it is not super-easy to live in Portugal.

Nobody speaks English there, and he can’t think he is just going to move to Faro and just live the life of a king. John has looked at real estate there and for 350.000 EUR you get a pretty livable place in Southern Portugal. In Porto a little further North there are 5-story buildings made in 1802 right in the center of town for 600.000 EUR and John was really coveting that place.

+ Mega tsunamis, trying to retire cheaply in Europe (OM445)

John’s sister is a super-mega-tsunami truther and when experts first thought that the big eruption on the Canary Islands was going to create a mega-tsunami that could wipe out Florida she was running around for a couple of days, claiming that this was The Big One. Then there were all these stories where seismologists were saying that this was not how it is going to play out. It was another one of these where John wished it was bigger than it turned out to be because who wouldn’t want a 500 ft wave to wash over Florida? It is Ken’s dream, it would solve a lot of problems in America.

John has a friend who lives on the other side in Tampa and he wouldn’t want to see her washed away, but otherwise? Also the drummer of Nada Surf lives in Florida and John doesn’t want to see him washed away either. Ken prefers the golf side as well.

Ken’s sister in law is thinking about retiring to Malta, looking for the retire-cheap place with a nice climate. John’s place has always been Trieste because it is so close to everything. Slovenia is right there, you can be in Vienna in 4 hours, you can be in Belgrade in 5 hours, you can go everywhere! Venice is 2 hours by train. John has been looking at property there, too, and he found a little house in the center of Trieste that is a bachelor pat. He shouldn’t talk it up because it is going to raise prices.

+* Other

John doesn’t like horror movies and he doesn’t even want to know their plots.

The other day John tried to convince his daughter that the rebels in Star Wars were actually the bad guys and the empire just wanted to bring peace and stability to the universe, and she was so outraged!

When John was in Tallinn he posted on the Internet that he was there and someone in Tallinn said they listen to Omnibus and offered to meet for coffee. John agreed, the guy worked for the government, he was a very well dressed young man in a tie, he had all The Long Winters records and he had seen them perform. At some point in their conversation John said that it is always fun to meet a fan that lives in a place where John would never have guessed, and the guy said: ”Whoa, I didn’t say I was a fan!” It was such a Northern European thing!

Ken’s book Mapheads is out in Estonian. John thinks it is a good book and he really enjoyed it.