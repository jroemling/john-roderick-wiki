This week, Merlin and John talk about
* various bands they have listened to through the ages (see below)
* John's relationship with Chris Gills ([[[Family]]])
* John's mom's musical taste ([[[Family]]])
* John's imaginary torture chamber ([[[Dreams and Fantasies]]])
* The concert when John started to like Marilyn Manson ([[[Music]]])
* Saving the stranded [[[GMC RV]]]

**The Problem:** "This was [https://en.wikipedia.org/wiki/Whitesnake Whitesnake] level not-good", referring to the album  [https://en.wikipedia.org/wiki/Celebrity_Skin Celebrity Skin] by [https://en.wikipedia.org/wiki/Hole_(band) Hole].

The show title refers to John's coat he wore to the Marilyn Manson concert he is talking about during the show.

They open the show singing. John had a previous recording session open on his computer when Merlin called and heard the most amazing echo through his headphones. He had some bass reduction, some chorus, some echo, some reverb and a little bit of EQ  (like [https://en.wikipedia.org/wiki/Neil_Hamburger Neil Hamburger] meets [https://en.wikipedia.org/wiki/My_Bloody_Valentine_(band) My Bloody Valentine]) and sounded like he was in a greek temple under water in the future.

Merlin and John banter about music for most of the show. They mention amongst others: 
* John has never been [https://en.wikipedia.org/wiki/Gothic_rock Goth], because Goth is ridiculous! It is a generational thing and he was 2 years too old. If you graduated in 1988, then Goth might have felt like a reasonable choice in 1984. 
* [https://en.wikipedia.org/wiki/Iron_Maiden Iron Maiden], 
* [https://en.wikipedia.org/wiki/Disintegration_(The_Cure_album) Disintegration] by [https://en.wikipedia.org/wiki/The_Cure The Cure], 
* [https://en.wikipedia.org/wiki/The_Stone_Roses The Stone Roses], 
* The [https://en.wikipedia.org/wiki/Happy_Mondays Happy Mondays] (they are [https://en.wikipedia.org/wiki/Gaslighting gaslighting] the entire culture), 
* [https://en.wikipedia.org/wiki/The_smiths The Smiths],
* John and Merlin's audience is probably 24 years old and is streaming [https://en.wikipedia.org/wiki/Tom_Petty Tom Petty] right up against [https://en.wikipedia.org/wiki/Marilyn_Monroe Marilyn Monroe], like in the picture you see in certain bars with [https://en.wikipedia.org/wiki/Bob_Marley Bob Marley] sitting with [https://en.wikipedia.org/wiki/James_Dean James Dean] around a pool table with some cats, all painted in velvet, and they are all smoking cigars. 
* [https://en.wikipedia.org/wiki/The_Psychedelic_Furs The Psycedelic Furs], they are just one song better than [https://en.wikipedia.org/wiki/A_Flock_of_Seagulls A Flock of Seagulls],
* [https://en.wikipedia.org/wiki/INXS INXS], 
* [https://en.wikipedia.org/wiki/Def_Leppard Def Leppard], 
* [https://en.wikipedia.org/wiki/Van_Halen Van Halen],
* [https://en.wikipedia.org/wiki/Ian_McCulloch_(singer) Ian McCulloch] of [https://en.wikipedia.org/wiki/Echo_%26_the_Bunnymen Echo & the Bunnymen].  
* [https://en.wikipedia.org/wiki/Godspeed_You!_Black_Emperor Godspeed You! Black Emperor] has loudness as their sixth instrument. 
* [https://en.wikipedia.org/wiki/Limp_Bizkit Limp Bizkit] with their grossly [https://en.wikipedia.org/wiki/Misogyny misogynistic], genre-bending garbage-person music is the biggest crime against humanity.
* [https://en.wikipedia.org/wiki/Linkin_Park Linkin Park], 
* [https://en.wikipedia.org/wiki/Korn Korn], 
* [https://en.wikipedia.org/wiki/Slipknot_(band) Slipknot], 
* The [https://en.wikipedia.org/wiki/Dwarves_(band) Dwarves], 
* [https://en.wikipedia.org/wiki/They_Might_Be_Giants They Might Be Giants], 
* [https://en.wikipedia.org/wiki/Marilyn_Manson_(band) Marilyn Manson]
* [https://en.wikipedia.org/wiki/Live_Through_This Live Through This] by [https://en.wikipedia.org/wiki/Hole_(band) Hole] was a good record, John really loved it, but he is not sure about the band in general. [https://en.wikipedia.org/wiki/Eric_Erlandson Eric Erlandson], [https://en.wikipedia.org/wiki/Patty_Schemel Patty Schemel] and [https://en.wikipedia.org/wiki/Melissa_Auf_der_Maur Melissa auf der Maur] (who was only in the band because they lost their other bass player to the drugs) were all good musicians, but [https://en.wikipedia.org/wiki/Courtney_Love Courtney] was just so problematic. By the point they had made the record [https://en.wikipedia.org/wiki/Celebrity_Skin Celebrity Skin], she had done 10.000 interviews about it and she was touting it as the new [https://en.wikipedia.org/wiki/Rumours_(album) Rumors]. As it came out, it was not even up to the standard of the third [https://en.wikipedia.org/wiki/Whitesnake Whitesnake] album, it was no good, it was Whitesnake-level-not-good. 
* [https://en.wikipedia.org/wiki/Hole_(band) Hole] has the [https://en.wikipedia.org/wiki/No_Doubt No Doubt]-problem, they always had to defend that they were a band and not just [https://en.wikipedia.org/wiki/Gwen_Stefani Gwen Stefani], like everybody is an equal member in [https://en.wikipedia.org/wiki/Destiny%27s_Child Destiny's Child]. 
* When John sees a stadium rock show, he assumes that every single thing he sees is choreographed and part of the show. He saw [https://en.wikipedia.org/wiki/Metallica Metallica] one time when they set the main [https://en.wikipedia.org/wiki/Public_address_system PA] on fire and they had to turn the house lights on and move the audience back 80 feet to get the fire department came in, and John was like "This is all part of the show", but of course it wasn't, you would have to fill in a lot of forms every night if that would be part of the show. 
* In 1989 there was a moment where Goth got hard and was not sad anymore. It wasn't mad either, but resigned to the fact that vivisection (?) was in our future. It was reporting on a future reality that they could see where we all became cyborgs that shit in a bag. It is the one part they couldn't engineer out of us. We would become Cyborgs "Cleep Cloop" that weren't clean. That was the crop circle era where animal mutilation was happening. 
* The banter a bit about 1980:s HipHop, which Merlin finds peak HipHop.
* Merlin thinks John is trolling him through this whole segment and is making it more difficult for him that it needs to be.