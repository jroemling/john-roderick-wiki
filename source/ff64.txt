//Intro by John Roderick//

I'm so relieved and glad that we here at Friendly Fire have decided to cordon off a donor-only feed for so-called pork chop movies. I believe that war movies are an actual thing and while a concrete definition may elude our grasp, I am confident to paraphrase Supreme Court Justice Potter Stewart that I know them when I see them. In that sense, Predator does not qualify as a war movie. Obviously! From now on movies like this will be sequestered along with other of their ilk in a special place: Our donor only feed. 

But am I not human? Do I not also, like Adam and Ben, thrill to watching professional wrestlers coated in baby oil spew an impossible quantity of bullets downrange from insultingly implausible weapons at a special effects UFO that looks like a reggae hermit crab? Of course I do! I like it when Wolverine goes Snikt. I like it when Tony Stark says he's the Batman. I like it when Spock uses his life saver to defeat the enemy Kling Clangs. I like all that stuff, they just aren't war movies! So now we have a place for them. I want you to know that we appreciate it. Also, I'm super-glad that now I can feel good about watching these fun stupid movies instead of feeling guilty that I am betraying some sacred covenant I've made with you, the listener, to watch war movies only. But now for the last time I will betray that covenant one last time and bring you Predator - not a war movie.

Adam and Ben really feel it is important in these introductions for me to give an overview of the plot of the movie, so listeners who haven't seen it can follow along without getting confused. They sent me these detailed notes about the production every week, because we often watch movies with a lot going on. But imagine how I howled when I received by special courier the packet of background information about Predator. Ben practically drafted a CIA fact sheet on El Salvador in the expectation that you, the listener, tuned into this episode wondering about the historical context of this movie. So I will break it down for you so that you go into this podcast armed with all the facts about the movie Predator. 

First of all, there are two future governors of American States in this movie. That feels important! Also, none of this movie is true. No-one in Special Forces smoke cigars in the jungle. Do you know how far away your enemy would be able to smell a cigar? Especially if your enemy was a UFO! Also the people making this movie were clearly addicted to cocaine. Did you know there is no I in team but there is an I in Tim, and two Is and titties, also every mammal has two eyes. The UFO in this movie also has two eyes and two hands and legs and feet and so forth because ”Hey, let's not go crazy dreaming up what aliens look like, am I right?”

Anyway, you know that thing in commander movies where every member of the team has a specialty and every guy has a signature hat or mustache or miniature owl perched on his shoulder? Well this movie has that. Not the owl! The mustaches! Although the owl would have been cool. You know how the government is always portrayed as criminally inefficient and corrupt and the real heroes are our group of streetwise gentlemen soldiers who, although they only just learned of the mission 30 minutes ago, they already know more about it spiritually than the 50 people upstream from them in the chain of command who put the whole mission together. And then these gentlemen soldiers defeat the bad guys and also shame their inefficient and corrupt handlers by employing the common tool of all earthly philosophers throughout time: The holy hail of unnamed flaming bullets. Well this movie has all that, too. Plus a UFO!

Anyway, the cast of this movie is like the Traveling Wilburys of muscle man, except now that I think about it, where the hell is Rowdy Roddy Piper? That is an oral history I want to read: ”The day Rowdy Roddy Piper found out he wasn't in this movie.” I bet the birds stopped chirping in the church bells fell silent. Somewhere across the country in the offices of the person at Johnson & Johnson who is in charge of the division that sold bulk drums of baby oil you could hear a champagne cork pop. It was the 1980s, people. 

The thing you need to remember most is that the first 20 minutes of this movie, where we are led to believe that we are in a Chuck Norris movie about crypto-communists and crypto-fascists creeping around in the jungle, is a terrible terrible movie. But right about the time we learn that something is skinning soldiers alive, the UFO, and stringing them up in trees with their guts hanging out, it switches gears and becomes a great movie! The duplicitousness of pencil-pushing CIA bureaucrats is still a low-brow subplot, but the all-out war between the predator and Schwarzenegger's team leaves no room for us to worry much about plot! 

The harder they fight, the more death-dealing but still kind of weirdly commonplace gadgets the alien deploys against them. Finally Schwarzenegger proves he is the world's greatest action star, even better than Stallone in Rambo: First Blood Part II, which was a pretty low bar. So now you have everything you need to know to understand the historical context of this movie and my work here is done. I've seen some bad ass bush before man, but nothing like this!The podcast is Friendly Fire and the movie is Predator.

+ MaxFunDrive 

A: This is it! It is the last day of MaxFunDrive 2019. 

J: Is it really the last day? 

A: That's it! It's Friday! Friday is the last day of the drive! 

J: People listening to this show have a lot of work to do!

B: We have made amazing progress toward our goal of 2000 new and upgrading supporters during the MaxFunDrive and we are recording this a couple days ahead of time so, there is a chance you guys have already come through on that, but last last we know we need your help! Support Friendly Fire, go to maximumfun.org/donate right now and set up a new or upgrade your existing monthly support.

A: There are a bunch of arguments for this, the first one being the ”Fuck you! Pay Me" argument. 

J: That's a good argument! 

A: That's that's a great argument! We are doing strong work here and I think you can appreciate that. The other argument is the extra pork chop movie feed that we will give to you out of the kindness of our hearts, should you decide to pay us!

B: But you only get it if we get the 2000 new and upgrading donors and you only get to listen to it if you are one of them.

A: Watch your dream of that and should we come up short of 2000!

J: What we don't know, because we are recording this in advance of Friday, is that we may already have our 2000 donors. That may be assured. What listeners who haven't donated need to know is that: ”You are iced out!” We are going to have so much fun over there watching these dumb stupid pork chop movies that these two dorks put on there because they didn't have good male role models when they were kids. We are going to have extreme fun over there and you are not invited if you don't give us money!

A: You don't want to be podcast-ostracized, do you?

J: As the official bully of this podcast I am going to give you guys all virtual swirlies!

A: Atomic wedgies are coming should you not support us at at least the $5 level!

J: $5 level? What else can you get for $5? You can’t get a sandwich for $5!

A: That is the least you can do. 

J: If you get a BigMac now it is like $5.95 

A: It is like $14 for a BigMac now, it is crazy!

B: Boy, Seattle prices are crazy! 

A: Buy a gallon of milk? $40

J: Your $5 thing is great, but your typical $5 donor checks all the McElroy podcasts and Judge John Hodgman and Greatest Gen and Greatest Discovery and then they give some freakin pennies to Friendly Fire!

A: What do you think about people who do the least amount of work in order to get a thing? 

J: Sounds like you should be podcasters! If you like doing the least amount of work for a thing.

B: I think people need to get off their ass, head to maximumfun.org/donate, do the right thing. This show came very close to not continuing to be a show in 2018...

A: ... right up to the brink...

J: ... it is hard to do shows! It seems easy, but it is not! 

B: The main thing that podcast groups fight about is money. If there was more of it, that would be less of an issue.

J: Also it should be noted: First of all that I am sitting here in Adam's studio in this plant is fully dead. What is the matter with you? 

A: It's not dead!

J: You have murdered this poor plant! 

A: That plant is 30 years old. 

B: Buy Adam a new plant!

J: But here is the point I would like to make: If you are listening to this program some time on a time delay, let's say you are not 100% caught up or you listen to shows in random order, let’s say this is two years from now and you are listening to this for the first time...

A: You can still trying to omnibus this shit right now? 

J: No no no no no no no no. I'm not saying this is post apocalypse! I'm pretty sure this show is going to be caught in the Apocalypse and it will be gone forever. One more reason to listen to everything now. 

A: This is going to be one of the things that they burn, is this show. 

J: But you can still go to maximumfun.org/donate and contribute to the making of the show. Now, if we don't get to 2000 donors by Friday, the pork chop stuff is off the table and you guys can suck it a million times, but you can still chime in and let your dollars help us be fully grown adults. Ben and his wife, newly married, probably are going to have kids at some point... 

B: Yeah, we are making a go of it in the big city. 

J: ... They are going to have to get formula and stuff. Adam has to feed his dog and anti-social cat and keep this plant alive. 

A: See, I'm struggling with keeping life alive also.

J: I have kids all across America with a whole host of...

A: You are a real Churchill that way, aren’t you? 

J: I have to support them all and I have to do it secretly! 

A: Secrets costs money.

B: So head to maximumfun.org/donate right now! We will be back a little later to tell you more about why you should do this, but if you have already done it by then you have our permission to skip those pledge breaks, otherwise you have to listen! maximumfun.org/donate

A: maximumfun.org/donate

J: maximumfun.org/donate