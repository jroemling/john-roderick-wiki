This week, Ken and John talk about:
[[toc]]

+ John having been the Indie Rock Santa in Seattle (OM391)

For decades John has been the Indie Rock Santa of Seattle. At least one gay couple got married after meeting on his lap and is happily married to this day. John sees one of them in his book group. One of them was a rather large fellow and the other was a more diminutively sized fellow, and John was a Santa who thought the more the merrier and sometimes he would have 8-9 people sitting on top of him. It was a good time!

There is a prominent gay bar on Capitol Hill called John Roderick’s Lap, which now is more a daddy-leather-bar. The Indie Rock Santa was something that happened late at night in bars, originally at The Crocodile, but it moved around. They would put a big throne and John would sit there with his Santa suit on and people would line up. John was never happier in his life!

+ Names in John’s family and friends (OM391)

John's uncle C. Calvert Knudsen went by the name Cal. Calvert alone is a fun name that you don’t hear that often and the few children called Cal are Calvin, not Calvert. It is a name that went away. John’s dad was David Roderick Jr., but he never used the Jr. and if you mentioned it to him he would scowl at you.

John gave his daughter multiple names //(see RL405)//. He suggested her a couple of times to start her name with a single letter, a period, and some subsequent name, but so far she has rejected it because it seems more like a boy thing. She got her name from the nurse who delivered her //(see RL405)//.

In High School John was friends with Richard Winfield Garnett III and he used to object to his friend calling himself III because for it to be valid all three had to be alive at the same time and his grandfather had died which made him II. Ken looked into it and there is no formal definition.

Miss Manners believes in John’s definition that everybody moves up one if one of them dies, but that just proves that John and Miss Manners are agents of chaos because that is insane! Why should you change your name every time someone of a different generation dies?

When John was young he was being called John Morgan by his family. They never said the one without the other. All the Johns in John’s family would turn to Jack when they grew older and stopped wearing short pants and stopped needed a hook to lace up their boots, but that didn’t happen to John who just lost his Morgan. John had two uncle Jacks who both had been known as John as children. Some family members still call John for John Morgan.

Some of it was surely derived from a Kennedy’s version of John Jr., but somehow when John became a grown-up he did not transfer to Jack, partly because the old Jacks were still alive. Some people call him Jack, but that is a romantic name. Ken had never seen John’s middle name. At one point when he was 21 John styled himself as J. Morgan Roderick.

John's first entry in the phone book was John Ignatius Roderick //(see RL299)// because he was an insufferable 18-year old. Ken thinks that when you have ”I” as a middle initial you really wonder what went wrong. Morgan is a good name and John could have been known as Morgan, it sounds very Welsh. Maybe Morgan John Roderick?

The reason John is named John Roderick is that the Welsh were John son of Roderick, Roderick son of John, and in his family’s pre-English administration they just traded 5 or 6 different names, which was a problem even in a small town in Wales. You got Roderick son of John who was son of Roderick who was son of John, and how do you tell them all apart?

When the British census came in, a guy sitting at a desk, all the coal miners came lining up, and he asked: ”What is your name?” - ”Roderick son of John” - ”Okay, you are John Roderick!” and they wrote it down in a book and it entered history. You can’t follow John’s family history past that moment because genealogy in Wales starts at that first day when some guy came to town who could read and write and ruined everything.

In the past people were naming their children after the founding fathers and in John’s family history in Western Ohio there are several William Henry Harrison who was a war hero before he was briefly the president. He chased Tecumseh’s brother right out of there!

John’s daughter has tried a couple different combinations of her middle names. Her first three names actually form a name, like J. E. B. Stuart who was known as Jeb Stuart. Apparently Jeb Bush was named after him. On her softball team John’s daughter was known as the name that her first three initials make (Mia), and she is still called that name by people who know her from then. It is a cool sports name!

+ Names in Ken’s family (OM391)

Ken is Kenneth Wayne Jennings III, and he did not advance when previous Kenneth Wayne Jennings passed. His dad is Ken Jr and when Ken is named in the press as Ken Jennings and his dad as Ken Jennings Jr it is like a time travel paradox for him. Bill Gates is a Jr and they differentiate it by his father calling himself William Gates Sr. John and Ken continue arguing about whether or not you should change your name when one of your ancestors dies.

Ken’s brother's family is also The Jennings. His parents came over from Sequim to go to the ballgame with them, and they are also The Jennings. Ken couldn’t tell his sister that The Jennings were staying with them because he himself is The Jennings. He could say The Olympic Jennings or The Utah Jennings. 

Ken’s son is not called Ken IV because although Ken is not against the name Kenneth, it is not in his Top 10. It hasn’t aged well and the ”th” is a problem. It is a bit like a British child’s name and Ken is not an engineer for the BBC. If your name doesn’t have a Sir before it you shouldn’t be named Kenneth.

It is extremely posh and sounded a little priggish. Instead he gave him a cool hippie name like Dylan with a handkerchief around the head. It is actually his middle name and he could be T. Dylan Jennings, but today that is also colored as priggish by all the robber barons like J. Pierpont Morgan, J. Paul Getty, and T. Boone Pickens who did that. T. Dylan sounds very bohemian to John.

Brian Eno, the guy who developed Oblique Strategies //(see OM194)//, is Brian Peter George St John le Baptiste de la Salle Eno, which is pretty posh! The piano player of Keane, Tim Rice-Oxley also has a very posh name.