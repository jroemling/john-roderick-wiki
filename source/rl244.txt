This week, Merlin and John talk about
* microphone technique and music recording ([[[Music]]])
* Words for your private parts and complimenting people's butt ([[[Factoids]]])
* A short recap of the history of butt-lifting jeans ([[[Style]]])
* Becoming a bush pilot and having a plan to make $1 million ([[[Dreams and Fantasies]]])
* Optimism ([[[Personality]]])
* The Alaskan Lifestyle ([[[Currents]]])
* Making a record off of Kickstarter ([[[Music]]])

**The problem:** "Everybody thinks about their butt", referring to the conversation they had about different words for people's private parts.
 
The show title refers to a type of microphone that Merlin is talking about in the first segment of the show, but misspelled.

Merlin is peppy today, looking at the bright side of life. A good recommendation is to look at the chimneys and not at the ground. It will cheer you up!