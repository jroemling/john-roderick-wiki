This week, Merlin and John talk about: 
* John producing a variety of filler content ([[[Shows and Events]]])
* John's job at the Seafair knighting the marschall of the parade ([[[Seafair]]])
* Giving Seafair more than they expected ([[[Seafair]]])
* The Grand Marshal of the Seafair parade ([[[Seafair]]])
* Honorary positions at the Rotary club ([[[Seafair]]])
* The currency of respect ([[[Seafair]]])
* The King County Sheriff and his taped-up gun ([[[Seafair]]])
* John's outfit as a king ([[[Seafair]]])
* Zar Nicholas II, King George V and Kaiser Wilhelm ([[[History]]])
* Being ready for every possible dress-code ([[[Style]]])
* How to personalize a composed uniform ([[[Style]]])
* The secret people shadowing high-ranked officials ([[[Seafair]]])
* The informal hierarchy between high-ranked officials ([[[Seafair]]])

The show title refers to Chris Pratt who is one of John's favorite actresses.

It is going super-good for John! Merlin adds that it is nice to just have a normal day. Sunday night is usually Merlin's best night of sleep and they are recording this show on a Monday. When John says "super-good", it doesn't mean that today is a normal day. He slept sort-of well, which is as good as it gets for him these days. 

During their talk about filler content, they banter about different modern artists that John does not think very highly about. Like for example, why would you want to know anything about [https://en.wikipedia.org/wiki/Iggy_Azalea Iggy Azalea]? Because she is [https://en.wikipedia.org/wiki/Fancy_(Iggy_Azalea_song) Fancy (song title)]? Merlin had to learn a little bit about her because he is a fan of [https://en.wikipedia.org/wiki/Charli_XCX Charli XCX], the lady who sings the good part of that song where she is not acting like a black person from [https://en.wikipedia.org/wiki/New_Orleans New Orleans]. There was some kind of [http://www.dictionary.com/browse/dust-up dust-up] about her [http://www.dictionary.com/browse/bonafide bona fides]. John starts to rap lyrics from the song [https://en.wikipedia.org/wiki/Trouble_(Iggy_Azalea_song) Trouble] by Iggy Azalea: "I should have known you are bad news, with your bad boy demeanour and your tattooes" Merlin really likes [https://en.wikipedia.org/wiki/Monster_(Kanye_West_song) Monster] by [https://en.wikipedia.org/wiki/Nicki_Minaj Nicki Minaj]. Although he is not up to date with hiphop, he knows a few [https://en.wikipedia.org/wiki/Kanye_West Kanye] songs from a few years ago. John starts to sing [https://en.wikipedia.org/wiki/Hotline_Bling Hotline Bling]. 

Merlin finds out all kinds of stuff on John's Instagram which he normally doesn't follow, like that he is king of [[[Seafair]]]