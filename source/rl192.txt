This week, Merlin and John talk about:

* John having been sick from a virus, going to the IV, taking cough syrup but immediately increasing his dose ([[[Drugs]]])
* John wanting to avoid body modification, wanting everything to be the natural way, elective surgery, jaw surgery ([[[Attitude and Opinion]]])
* Mustaches, the area between your nose and your lip ([[[Style]]])
* Smart Drugs ([[[Drugs]]])
* The guy with the hot rod tattoos to cover up his hippie tattoos, tattoo removal not really being a thing ([[[Style]]])
* John’s dad being a delivery boy for Bartell Drugs in the 1920s ([[[Parents]]])
* The Giftschrank, locking dangerous books and ideas away ([[[Philosophy]]])
* Skeeter died of liver failure ([[[Gary and Skeeter]]])
* Gary claiming that the Mexican family living next to him messed with his van ([[[Gary and Skeeter]]])

**The Problem:** From a young age, John wanted a mustache, referring to thinking that if he agreed with his dentist who wanted to move his top jaw forward that his mustache would look cooler because it would enlarge the area between his nose and his lip.

The show title refers to John's big toes being too short and if it was as big as his second toe he could use it as a thumb and he would be King of Tahiti.

The audio starts with a short clip by James T. Green to say ”Hi!” to John, announced by Max Temkin.

[[include inc:raw]]

+ John having been sick from a virus, going to the IV, taking cough syrup but immediately increasing his dose (RL192)

It is good to be back, but John is still a little sick from a bad virus that snuck past although he has a flu shot, but it wasn’t a flu virus, but another mystery virus that conned him into thinking it was just a normal cold for a couple of days. John had a sore throat and a stuffy nose and then in the middle of the night he was 104 degrees (40°C) and then he was freezing cold, which was no longer a normal cold, and then he had a fever for 3 days and he even went to the doctor, which he doesn’t normally do. 

She told him with a smirk on her face that he had a virus: ”You are sick, huh? Happens sometimes! Good thing you came to the doctor! It could have been something worse, that is why we went through all these tests!” Then she hooked him up to an IV and gave him a whole bag of what is probably medical-grade Radon, which plumped him right back up because he had been dehydrated in addition to being feverish. He asked her to also throw some Vitamin B12 in that and whatever else of that shit that Kennedy had, the Addison-fighter. ”Give me the Hitler shit and the Kennedy shit and I will run for president!”

John is on the mend, but he may explode into a coughing fit that no-one wants to hear, and he does not have a cough button. Merlin is not going to put in some hold music or cut it out, but he is just going to listen politely. John argues that his cough is a productive cough and he is not sure if their listeners around the world are prepared for what it sounds like when someone in Seattle offloads some of that moisture that he collected when they put him on a bag.

John does not like to take cold medicine because he likes to avoid intoxicants, but what about the saline he got? Intoxicants present a risk to him whereas it is unlikely enough that he would have a street friend who knows how to put an IV in and has a bag a of saline or can make it available. It also didn’t make him feel that amazing, he just felt like he drank 6 cups of water. He didn’t want to go downstairs and drink water, he was shivering, while everybody always says that you got to keep hydrated.

The doctor was convivial and gave him a look that communicated to him that she was aware who her patient was, she almost gave him a look like she was a fan. About halfway through the IV bag he had to go to the potty because this bag had filled him up with water. He had gone to the emergency room late at night, driving by himself, which everybody criticized him for and he reminded them of the story of the time when he hit himself with the hatchet //(see RW74)// which they all claimed they had never heard before.

It was in the hospital and there was nobody around monitoring him, and in his little gown he grabbed ahold of his IV stand and headed to the bathroom and she was coming the other way looking at a chart. Up until that point she had maintained a very professional demeanor with him, but as she saw him a momentary candor went across her face and her guard was down and she gave him the look he has seen many times, almost a titter. John was not sure whether she had voted for him and he hopes that every doctor in town did. She was his age and when she was 29 she could also have been a fan of one of his bands.

John was coughing in such a way that it looked like he was going to die. He had bent over, held his lungs with his arms, and he was coughing like the alien in the first Alien movie so that his second set of teeth came out and he was coughing up stuff. It was extremely painful, and the doctor, extremely blithely, prescribed him Codeine Cough Syrup. There was a moment where his reflexive reply that he had been using for 20 years, being: ”I am sorry, I am in recovery, I don’t do Codeine Cough Syrup!” and they all understood and prescribed him Imodium, something non-narcotic.

But in this case he was sitting there in tremendous pain, he had gone to the emergency room for a thing that turned out to be a virus, in part because he had a fever for 4 days and he had mistakenly complained about a symptom on the Internet and he got 45 replies on Twitter and 400 replies on Facebook, all telling him that he needed to eat raw garlic, and several people said that they were a medical doctor and if he had fever for 4 days he needed to go to a doctor immediately.

All of a sudden, what he knew to be true, which is that if he just cowered under these blankets for two more days he would start getting better turned into a bug in his mind, which was: ”Oh shit, you got pneumonia and you are going to die!”, and he called the doctor’s office that told him that there were no appointments available until March of 2017 and he needed to go to the Emergency Clinic where there is every kind of person, all miserable.

A part of him wanted to feel that his illness was bad enough that it justified this trip on Codeine and he didn’t want to go out of there saying: ”She gave me a pint of saline solution and patted me on the head and told me I had a cold!”, and rather than tell her ”No!” he just sat there quietly. Half of the population would silently rejoice in that situation. John hadn’t taken this stuff in 20 years, he filled the prescription, he went home, and he looked at this bottle of Codeine Cough Syrup and said: ”You, sir, are a problem for me because you are narcotic and I have not taken narcotics in 20 years and this is a bad scene!”

Then John went into a cough so badly that he dropped to his knees, banged his head on the floor with a cough that was coming from his pelvis, and it was a virus, a little thing that was floating in the air and snuck inside him and is trying to send him a message that God was watching him and he had been expressing too much hubris somehow, to take him down to size. John poured himself 5 ml of Codeine Cough Syrup and he took it and he went to sleep and he woke up the next day to a racking cough and he went downstairs and took another 5 ml of this cough syrup.

He was now in a tricky place because he had taken 5 ml of Codeine two times, prescribed by a doctor for a bad cold, and where was he right now? Did he need to go to an AA meeting for the first time in 1.5 years and throw his 20 year coin at them and say: ”I start over! I am back to zero!” or does he just chill. Halfway through the day he had another terrible episode of coughing, and he doesn’t exaggerate. He would try to sequester the cough in his throat like somebody with bad asthma. There was nobody in his house, he was not performing for anybody, he doesn’t even have a cat, he wasn’t putting on a show, but he was laying on the floor, writhing in agony.

John poured another cup of Codeine and this time he gave himself 7 ml, which is still well below the normal dose because in this cough syrup cup he only poured a tiny amount, and it is certainly well below what he would have taken 20 years ago. That amount of escalation when he unconsciously poured 2 more ml in this cup he was like: ”Holy shit! Oopsie daisy!”, it was him upping the dose, hoping that he could scrape the underside of feeling something, and he put the cough syrup in the furthest darkest corner and told himself that it does not exist and it is long gone from his world and for a half a day of racking cough a little shot would help him a lot, but: ”Fuck you! Fuck you! Fuck you!” That was the furthest little test John has made!

Every once in a while John would walk through a party and there would be people smoking pot and he won’t make an attempt to not inhale. Whatever! But if somebody poured him a glass of juice and when he put it in his mouth and would realize there was alcohol in it, he would go to the bathroom and spit it out. In 20 years he has never taken an ”accidental” drink of liquor, he has spit every one of them out, and he had never had any Codeine Cough Syrup or anything like this, but this situation was a culmination of factors and all of a sudden he was tip-toeing closer and didn’t notice it at the time, but immediately recognized the tendency.

People ask him a lot if there is a point of sobriety where you stop thinking about having a drink, but in John’s experience there is never a time where it doesn’t come up. You are living in the world! There is almost no day in his life where the topic of alcohol doesn’t come up somehow. He works in bars! It is always there and as somebody who quit drinking the question is constantly there: ”What if I had a drink?”, one of these little voices that has just one line.

Nobody wants to be an alcoholic and every single person who wants to quit drinking doesn’t want to quit drinking, but they want to drink like a normal. They quit, they feel great, they think: ”What was the problem? I was just in a bad way!” and they think they can have a little bit and they are back to being a wineo in no time. A big part of going to meetings all the time is to keep that voice at bay by reminding yourself what will happen by sitting in a room full of people who are all having that experience and who were sober for 20 years and they had a bad headache and they had some Codeine Cough Syrup and three days later they were on a barge to Shanghai and they were out again for 14 years and now they are back, 16 days sober and: ”Thank you so much!” and everybody applauds. It is that easy? You just have to slip on the ice one time?

For guys like John who tell themselves that they have a lot of willpower and they have this thing licked, there is no part that thinks that he could have a drink, he is not a dummy, he has heard enough stories, but it is fucking hilarious to see how little it took for him to do what he did. Merlin adds that it was a testament to John’s vigilance, that he was able to see that two more ml were too much and he had the ability to catch that, but that was again because he had heard 1000 stories.

About 10 years ago John ran into a guy he had known for a long time in the AA program, and he looked all banged up, he had been riding his motorcycle and he had been T-boned by a car and he flew off the motorcycle through the windshield of the car and it split his helmet in half. He wasn’t sure if he was going to live or die, they were picking glass shards out of his eyes, just an insane story, and when he was in the hospital all messed up they had him on a morphine drip and when he woke up out of his stupor he had spent a day on this morphine drip until he called the doctor and asked him to take him off the morphine because staying on this morphine the result for him would be worse than suffering through this with no pain killer. He was vigilant, he had fallen so deep and he knew he didn’t want to go back.

John also doesn’t want to go back, and he was just left alone with something where he had a justification. He had been in that situation 30 times, sicker than he was now, but for some reason right this moment it was a little check, and probably he should go to his first AA meeting in 1.5 years and tell that story because it would be useful to be in a room full of people who go: ”Haha, look at you! That is your hubris, right there!”

//John starts coughing again//

+ John wanting to avoid body modification, wanting everything to be the natural way, elective surgery, jaw surgery (RL192)

John avoiding substances also has a component from his Alaskan stock: If he were a woman and was pregnant he would want to have the baby the old-fashioned way without the epidural, screaming, and ruining his body as it happens because that is how God intended. There are plenty of people he respects very much that say: ”Are you crazy? If you have a Caesarian section you can schedule the moment the baby arrives, go to the hospital, go to sleep, and when you wake up there is a baby there and everything is monitored and you did not get split in half by a giant bubble of fat”

It makes perfect sense: The technology is here, so why not employ it? Still, John doesn’t find it convincing because he thinks that in having a kid he will unlock some mystery and some lasting reverberation in him and the kid that couldn’t be duplicated. If nothing else he might always wonder what it would have been like.

There are all kinds of arguments why John wouldn’t use the most current technology. Why hasn’t he gotten LASIK surgery? He is walking around with glasses that no longer really work for him because his eyes have become dried-up little corn husks. But a part of him feels like: ”What if I am trying to be an astronaut and they are scanning me to see if I am one of the genetically pure and they will notice that he had shaved his eyes, which means he is not one of the goods!”, and just the nature of the fact that his eyes were misshaped means that he is genetically inferior.

John is 47, he is not going to be an astronaut, but he has that lingering sense of: ”Don’t modify yourself!” He is hoping that one of their listeners will name a star after him! It extends to the fact that he doesn’t have any tattoos although he can’t argue with anybody who does. But then you get into the category of elective surgery, which is akin to the eye-shaving, but where does the line on elective surgery lay?

For example, John’s teeth don’t fit together and the first thing the doctors wanted to do from the time he was 17 was to break this kid’s jaw and reset it in a different place so that his teeth fit together. Even at the time that felt crazy! Merlin has a person near and dear to him in his life who had that procedure done. Every dentist told him that they had to cut his jaw in half and move it back until some oral surgeons told him that they don’t do that anymore, but they will cut off his upper palate entirely and saw his whole top layer of teeth off from his skull and move them forward because it was not that his jaw grew too much, but the top of his mouth face failed to grow enough. Both things felt crazy!

They told him that it would make his teeth fit better together and increase their longevity because right now he is slowly grinding his face apart until it is just a soul patch touching a nose. The last time he talked to a dentist about it he was 46 and he said that at that age he was half-way to the goal line anyway, but they told him that he was going to need those teeth!

He is hoping that technology somewhere along the line can give him aluminum teeth or teeth made of plexiglas with goldfish swimming in them or something cool where they don’t have to saw either part of his face. That is what they do, that is why they drive a Porsche 911 because they saw people’s faces in half and John doesn’t see it as a necessity. 

But when the guy wanted to move the top of his mouth forward John had a thought that this was going to make his mustache look very cool because it is going to push his mustache out a little bit and he always felt like the area between the bottom of his nose and the top of his lip was a little too small.

All of a sudden John was considering this surgery, in part because many dentists had recommended it, and in part because it was potentially going to increase his mustache area, and he had a conundrum, a serious spiritual crisis because how of this was he going to consider an elective surgery? If somebody had told him that they had a new surgery to expand his mustache area he would dismiss it as ridiculous, but what if he got this necessary tooth surgery that just coincidentally expanded his mustache area and only he himself will know.

John had introduced Merlin to Hydrogen Peroxide. In his early days he had used it as a surfactant, a mouth wash, a debriding agent, but also as a way to deal with his allergy to himself, and as a side-effect of that he also got super-cool blonde hair out of it, which was a similar Dark Night of the Soul /((poem by John of the Cross)// for him, and he does not use it anymore for that reason: It feels like he was secretly doing it just for the cool blonde hair, which is like taking cold medicine just to get high and then you are riding across America on the back of a motorcycle with some guy named Steve.

There are elective surgeries that are intentionally augmentative, and there are elective surgeries where for example his second toe is longer than his first toe, which means he is smart and a descendent from Russian royalty. John is taking pride in his set of Prehensile Feet, his monkey toes, because they enable him to grab and peel an orange with his toes, which is nice to have, and better than having those old Donald Trump toes that nobody wants. But if you look down at your feet and think they are grotesque and misshapen, there is surely a surgery to have all your toes shortened.

John’s problem is in reality not that his second toes are longer, but his big toe is too small. If his big toe was long enough he would have normal-looking feet and if his big toe would be as long as his second toe it would not only look like a thumb, but it would function as a thumb and he would not be living here, he would be in Tahiti, he would be King of Tahiti, which sounds pretty good. Let’s say there is an elective surgery that shortens his second toe to make his feet look normal, or an elective surgery that elongates his big toe so that he can become King of Tahiti, which one acquits with his personal validity needs?

+ Mustaches, the area between your nose and your lip (RL192)

From a young age when John was 17 he wanted a mustache, and of course God realized that this was something he wanted and he was going to deny this to him because that is the mysterious way that God works: He wants to bring obstacles in front of you in order to test you! John has studied men’s mustaches very closely over the years, and all the mustaches that you like, the Tom Selleck’s of the world, he has an enormous area from the bottom of his nose to the top of his lip. It is a fucking soccer field! He has a huge area and a big mustache that looks great.

The area between the bottom of the nose and the top of the lip is a very crucial area for men to look trustworthy and capable. Zorba The Greek has a big area there, just like all the guys where you instinctively think that this guy is going to handle things. Sam Elliott also has a huge area that can accommodate an enormous mustache.

John’s hole young life at 21, 24, 26 he had a big huge beard and a big beard-shaped face, but a little teeny hand-strap blonde wispy little mustache that is in an area too small to contain a mustache and there is nothing he can do about it. Rob Delaney the comedian has a huge mustache trough.

Those doctors sitting in a conference in Las Vegas where all the oral surgeons get together and they as saying: ”What is a new thing that we can do?” Merlin argues with their friend John Siracusa about this: He doesn’t have any way of judging whether they are just saying some shit vs giving him life-saving advice. They never give you a percentage estimate of how much of what they are saying might be total bullshit that only makes sense for a little while.

This happens at universities all the time: Someone writes a paper or a book, like: ”What if X was Y and blue was orange?” and if it makes sense then everybody will then claim that X is Y and blue is orange. A dentist gets up at one of these conference and gives a presentation, a TED Talk, how one time they sawed a guy’s face in half and moved his teeth around and the results were amazing and they never had any problems after that, and all of a sudden everybody in the room is convinced that this is a key thing that makes modern life better and they need to encourage people who have this problem.

John is sure that half the population their teeth don’t fit together! Merlin has been around this in his household, and there is more to it than ”My teeth don’t fit together”, which is why his daughter has retainers, it is about way more than whether or not she can be a model, but you could get TMJ and all kinds of other things, and you want to nip that in the bud.

+ Smart Drugs (RL192)

To take this to a Wired Magazine place, which John and Merlin like to do because they are thought leaders and technologists: Articles about enhancement drugs that are going to make you better, faster, stronger, and you are going to be creating apps while you sleep, were a hot topic in the late 1990s, and Merlin tipped his toe into the area of smart drugs a little bit and to this day he continues to take a version of smart drugs which in his case have less of an impact because they have to fight off him. At the time you were going to be able to create your own personal cocktail of different drugs that would enable you to be your best self, not with the goal of intoxication or enlightenment, but just to unlock potential that is otherwise difficult to access.

A lot of those think pieces were just reportage and some of them were think pieces, and the ultimate twist is always going to be in the sense of: ”If you don’t take smart drugs, are you going to be able to keep up?” Are you still going to be working on your first app while the people at your school who weren’t even as smart as you are already making their sixths or seventh app? Are you still working on your first TED Talk when the little kid on the block has done six TED Talks and venture capital funding. He has an app that makes apps and you are sitting there with your dick in your hand, wondering if you should make something for the Blackberry.

He is sitting in a Cosmodrome somewhere, getting ready to be blasted up to the ISS, which he is paid for with the fucking Petty Cash, and you are driving around in a Honda Civic, trying to figure out even what an app is and you got a Blackberry Pro. You can’t even trade your fucking three year old BMW for a new BMW! How do you even set the time on a Rolex.

//John is coughing heavily, it was like a Trout was living in his lungs and now he is not there anymore.//

+ The guy with the hot rod tattoos to cover up his hippie tattoos, tattoo removal not really being a thing (RL192)

Merlin thinks tattoo removal is going to be huge, but people have been saying that for 20 years. John told the story of the guy who had his arms covered in hot rod racing tattoos, like Man’s Ruin, tumbling dice, and naked devil girls surrounded by a halo of flames. John was with him at a party and somebody said they needed somebody to go on a beer run and he had a car and John told him to go with him to the beer store, partly because he thought that this guy was going to have the hot rod of all time. He had crossed checkered flags on his forearm! But his car was a 1992 Toyota two-wheel-drive pickup.

John was really disappointed and he asked the guy about it and he never had a hot rod, but his tattoos were all cover-ups, like this one used to be a dancing bear and this one used to be Winnie the Pooh flying a Yin and Yang flag and this one over here was a fucking smiling sun and this one was Jerry’s hand missing a finger. He had hippie-tattoos all over in the past and now he got hot rod tattoos covering them all. What was he going to do when hot rod tattoos are going to become the hippie tattoos of the 2000s because [[[x makes y look like z |the 1990s are going to make the 1960s look like the 1950s]]]!

It hadn’t even occurred to him and John was wrong anyway: Punk Rock hot rod tattoos never go out of style. He had fucked up the first time and gotten hippie tattoos, and then he decided he didn’t want to live in the rarified air of rainbow gatherings and now he had gone hot rod and he is probably still very happy with those. People have been talking about tattoo removal, but John is just seeing more tattoos all the time.

A certain group of people take a moral stance on tattoos and they keep wanting to be vindicated, like: ”You are going to be sorry!”, while all the people with tattoos continue to not be sorry and plenty of people have aged to old age and died already who had tattoos the entire time and were not sorry a little bit. Merlin thinks there is never a good day to be 75 and go: ”What the fuck did I do?”

Merlin only mentioned this because his dad, who died in 1974, had a tattoo removed. When he was in Korea he got an ill-advised tattoo with the name of his then-fiancé and his mother on it when he was drunk one night on leave in Tokyo. It is the worst cliché of all time and he himself decided he wanted to get this removed. Merlin doesn’t remember ever having seen it. Apparently the late-1960s / early-1970s version of tattoo removal was not pretty and ridiculously unpleasant! Merlin doesn’t like tattoos, but who cares! He heard colors are really hard to remove. John says: ”Colors!” like the song Colors by Ice-T.

John has discovered in his long travels that there are people who do not have regret. 

What if you had a tattoo outside of your control like you had a tattoo of Bill Cosby. Even though Merlin thinks he is easily one of the great stand-up comedians of all time, he wouldn’t want him on his face.

John knows a lot of people who have one tattoo. Hodgman has an inexpert tattoo, not quite a prison tattoo, but close, up on his shoulder, a tiny blue diamond with little rays coming off. He has an explanation for it, it is in a personal area that you wouldn’t see unless you saw him without his shirt on, and he spent great many years where he avoided that opportunity for most people.

John would go swimming with him for years and he swam in an outfit akin to a 19th century swimming outfit, which is perfectly in character for him //(he apparently talks about it in Judge John Hodgman episode 115)//. It is absolutely a ridiculous tattoo, but he is not ashamed of it and he owns it because it represents that moment in time.

John wonders if you could just go to Bartell Drugs, a drugstore that started in Seattle, with your giant neck tattoo of the Virgin Mary that you got in your 20s not because you were Catholic but because you thought it looked rad because you liked Drive Like Jehu. Although if you are repping a Virgin Mary on your neck, you keep repping it!

+ John’s dad being a delivery boy for Bartell Drugs in the 1920s (RL192)

John’s dad in the 1920s was a delivery boy for Bartell in the Chinatown / Japan Town neighborhood. People would order stuff from the pharmacy and John’s dad would go in his knickers and his lace-up boots that he needed a little hook to lace up, ride his bike to the Bartell, and they put the stuff in a basket in the front of his bike. He would race around and take little bags of medicaments into Chinese Tongs, which are secret societies and there would be people in a smoked-filled room playing Mahjongg and he would walk through with his news boy cap and his knickers and he would knock on a door in the back and they would open it and there would be guys in a really smoke-filled room and he would hand a little paper bag to one of them and he would get a couple of coins. He would tell these stories endlessly.

Bartell is all over Seattle and you can go to a Rite Aid or a Walgreens that come into a neighborhood and plop down their ”signature” architecture with their little Mortar and Pestle //(John accidentally says Hammer and Pestle, which was the Soviet flag)//. Bartell is their home-grown store and John’s dad has a connection to it, which is why John will advertise for them.

+ The Giftschrank, locking dangerous books and ideas away (RL192)

//John is coughing again really hard.//

Their German listeners are surely already compiling a list of homeopathic drugs that he should have been talking the whole time. They love St John’s-wort, they love herbs because they are still animists, but they do not like Scientology and made it illegal because they have a thing about cults. Their relationship to certain ideas is a little bit like John’s relationship to certain kinds of substances, if something is a little bit like a cult or a little bit too sticky about a certain kind of authoritarianism. There was [https://99percentinvisible.org/episode/the-giftschrank/ a really good episode] of 99% Invisible about the Giftschrank where they kept copies of books that were dangerous, but they still had to keep around, like Mein Kampf.

Every time John bought a copy of The Anarchist Cookbook his mom would find it on the shelves and destroy it because it was about setting tiger traps and refighting the Vietnam War. Merlin was mostly interested in the spycraft stuff, not making bombs. John loved learning how to make bombs and this book inspired him to start making pipe bombs and he went through a phase where he made a lot of them and blew stuff up with them until he realized the next thing he was going to blow up was himself and he also disappointed his teacher and got in big trouble //(see RL63, RW84)//.

John entirely disagrees with the impulse to have a safe room with all the forbidden books, it is a bad principle. Supposedly The Protocols of the Elders of Zion ”leaks” and all the people who want to believe that stuff have reason to believe that there is a secret room with all the secret books. This one snuck out and the idea that there is a place where books like this exist which isn’t public knowledge validates the idea that this secret book must be true even though it is not.

It is not dissimilar why Merlin and others don’t want Apple to weaken the encryption on their phones. People don’t understand what is at stake!

We imbue forbidden and dangerous things with power by putting them in the Giftschrank. Instead you should put that stuff out there and let everybody hassle it out with each other!

What about kids and drinking? A lot of people say that in Italy kids have a little wine with dinner and most kids don’t like the taste of alcohol, but now they have tasted it in the house and it is not in the Giftschrank where they have to drive out and find it, it takes the magic out of this forbidden thing. John supports that, although there is a lot of smugness in Europe about drinking. The all say they give their kids drinks and they don’t have this alcoholism problem, but although they do practice moderation, but their line is pretty high.

There are a lot of people who are just drunk all the time and they don’t fetishize it to the point where they are alcoholics like you see in Shakes The Clown. In France, Italy, and Germany people are just fucking drunk all the time. They keep a lid on it, they manage to go to work the next day, but they just drink all the time, they spend all their time in restaurants which are really bars and you are just always a little shitfaced. Kids should drink a little wine with dinner, but alcohol is also a widely accepted way to be checked out most of the time.

+ Skeeter died of liver failure (RL192)

//See [[[Gary and Skeeter]]]!//

+ Gary claiming that the Mexican family living next to him messed with his van (RL192)

//See [[[Gary and Skeeter]]]!//