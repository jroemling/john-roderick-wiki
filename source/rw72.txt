This week, Dan and John talk about
* Texas attitude and spirit ([[[Geography]]])
* Legalization of marijuana ([[[Drugs]]])
* Marijuana vs alcohol ([[[Drugs]]])
* The detrimental effects of marijuana ([[[Drugs]]])
* John's own history with marijuana and becoming an alcoholic ([[[Drugs]]])
* Dan's view on alcohol ([[[Drugs]]])
* Betrayal, vice and the desire to do bad things ([[[Personal Development]]])

The show title refers to the little devil and the little angel sitting on your shoulder.

Dan is opening the show far away from the microphone again after having not done it in the previous episode. It reminds John of [https://en.wikipedia.org/wiki/Sesame_Street Sesame Street] where [https://en.wikipedia.org/wiki/Grover Grover] explains [https://www.youtube.com/watch?v=iZhEcRrMA-M near-and-far].

They talk shortly about the [https://en.wikipedia.org/wiki/Independence_Day_(United_States) 4th of July]: It has been years since John //wanted// to be anywhere near a big [https://en.wikipedia.org/wiki/Fireworks fireworks] display with crowds of people, most of them [https://en.wikipedia.org/wiki/Electronic_cigarette vaping] [https://en.wikipedia.org/wiki/Cannabis_(drug) marijuana] in your face. Back in the days when that was illegal, people were at least discrete about it.