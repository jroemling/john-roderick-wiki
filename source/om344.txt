This week, Ken and John talk about:
[[toc]]

+ Places on the Internet you go every day? (OM344)

Ken’s bookmarks of places on the Internet he goes to every day:

- [https://www.nytimes.com/puzzles/spelling-bee New York Times Spelling Bee] puzzle
- His email
- [https://kingcounty.gov/depts/health/covid-19/data/daily-summary.aspx COVID-numbers in King County]
- [https://slate.com Slate]
- reddit, where he uses one of his many sock-puppet accounts to tell people to buy stock in hickory farms in Ohio because he is trying to bankrupt the hedgefonds who shorted hickory farms

Ken has burner accounts where he argues that Ken Jennings is the greatest host of Jeopardy. They all come from different cities and he creates whole identities for them, maybe subscribes to a model train subreddit, maybe a geographical one like a Florida subreddit, and he thinks about how much that person would be into Ken Jennings. He is behind 7 proxies. As long he has 30-40 of those he can create a pretty good fan section.

John doesn’t have very many bookmarks although the Internet is full of stuff that he would be interested in. One of his friends loves [https://www.atlasobscura.com Atlas Obscura] which should be right up John’s alley, but he doesn’t care about weird places that other people have seen, he wants to be the first person to see it. There are website about abandoned buildings and that is cool to look at once, but then he wants to explore abandoned buildings himself and doesn’t care about those other guys.

John is squandering the Internet. Particularly when he was on social media he felt stupid because he was wasting his time on social media instead of being on the website of the [https://www.loc.gov Library of Congress] looking at Thomas Jefferson’s corn receipts. Now that he has all this extra time he doesn’t care. There are great websites like [https://www.folkstreams.net Folkstreams], containing documentaries about American folk people, but John never looks at it because it feels like an investment of 6 whole minutes of his time, although he should be doing this every day!

+ Books, library card catalogs (OM344)

Sometimes books have Library of Congress indexing like ”Novels, epistolary”, ”Kairo, novels about”, ”18th century food”, different ways of slicing the book. Ken misses card catalogs. When libraries first got rid of them you could buy them for cheap and he regrets not getting one. Now there are no more card catalogs but millions of hipsters who want one as furniture and you would have to give you first-born child for them. It is the same with Apothecary cabinets. John doesn’t know what people are keeping in them.

+ Bring a Trailer (OM344)

One of the few sites that John has bookmarked and actually goes to all the time is [https://bringatrailer.com Bring a Trailer], a site that features old cars. A long time ago it was curated by nerds who were going through local classified ads from people selling cars and they cherry-picked the ones that were cool. 

There is a whole universe of American people who love cool cars and the difference between an uncool car and a cool car is often a difference in pretty esoteric specifications. There is the Camaro, the Camaro RS, the Camaro SS, and the Camaro SS 454 with the special tachometer option, little things that make certain cars way more desirable and expensive than other cars.

There is a whole subset of car collectors who want cars with celebrity history. The Mercedes Benz 600 Sedan originally purchased by Roy Orbison [https://bringatrailer.com/listing/1972-mercedes-benz-600-4/ is for sale right now] and the Roy Orbison provenance raises its value considerably. Steve McQueen is the most famous car buff and car racer. He and Paul Newman had a lot of cool cars. Jay Leno has one of every car. These cars really attract a premium and if this Mustang had been owned by Steve McQueen it tripples the price.

Bring A Trailer was a way to provide for car collectors. If you only collect Ford Tauruses, how are you going to scour the Craigslist ads in every town? They are not only for sale in Los Angeles, but somebody might have a pristine (Dodge) Super Bee in Paducah, Kentucky and doesn’t think of selling it online. There are a lot of car collectors who aren’t actually multi-millionaires.

Back when the website was maintained by a group of enthusiasts it was full of wonderful things. They knew enough not to just put every Volkswagen Bug up there and they could tell the difference between a 23 Window Volkswagen Bus and a 1974 Volkswagen Bus. There would be really interesting cars like old Stock Car racers that had been rode hard and put away wet, but it turned out that it was Richard Petty’s first Stock Car and it was in some barn.

There was a Duesenberg from 1948 in somebody’s barn worth $300.000, but now it was covered in dust and bird droppings. It was ”exploit the rural!”, it was for sale on Craigslist for $1500 and the idea would be that you could buy this car, dust it off, change the oil, and you had a car you could sell to Jay Leno.

Originally the site was a small market of people, which is why it is called ”bring a trailer” because if you wanted a Duesenberg that was in a barn in Tennessee,  the logistics of it was the major issue. Restoring cars is like buying and flipping houses: Everybody thinks they can do it, but the right knob for the cigarette lighter is a $900 part because there aren’t that many, and you have to be very good at mechanics to make one of those things into something nice.

John doesn’t need a Duesenberg, but he loves the treasure hunting and he loves to read the comments section where guys would be showing up saying: ”I restored my first Duesenberg in 1961!” Those commenters became celebrities within the culture of the website, people who had owned 42 Mustangs. There are a lot of women and a lot people of color because car lovers are a certain kind of thing. We joke that they are all guys in their 60s.

The people who collected Model T’s were one generation that eventually died and there are no Millennials driving Model T’s. This was true of 1960s muscle cars, too! All the guys who wanted to drive 1960s muscle cars were Boomers and there are fewer and fewer of them all the time. These cars are expensive because they are cool, but younger buyers from Generation X like John were not interested in the stupid Fox Body Mustangs from 1986, those terrible cars like a Pinto with only 20.000 miles in perfect condition. Somebody wants that! John still loves the AMC Eagle, the first SUV 4wd station wagon, and there is nothing to recommend it, but it is a collectible.

Over time the site evolved into a place where 80% of the ads were Porsche 911 from 2000, it was just rich guys trading cars, and the people who ran it realized they could make money off of this and they turned it into an auction site. They weren’t finding cool cars in Lodi California anymore, but they were just waiting for rich dudes to bring their Duesenbergs and for other rich dudes to buy them. No online community is good for more than 18 months until somebody messes it up.

The reason John still goes there is that cool cars still come up and a lot them are being sold by rich people to rich people, but they are car people and it is fascinating. Yes, there are a bunch of dumb Porsches, Audis, and traditional MGA because there is always somebody who wants to drive around an MGA, there are Lamborghinis and Cobras, but then you see an interesting old (Pontiac) Trans Am and a funny little Volvo station wagon that you could have bought for $1500 when John was in High School and that apparently is worth $20.000 now. Who knew!

The other day there was [https://bringatrailer.com/listing/1928-covered-wagon-pony-size-historic-henry-ford-museum/ a covered wagon from 1928 for sale], not a Conestoga, but the actual kind of wagon that actually did make it across the plains. It was clearly well-traveled, wooden-wheeled, with stickers and metal plates all stuck to it. 1928 was well into the automotive age and covered wagons were now anachronistic, but the barns of America were full of them, just like the barns are full today of our dad’s and grandfather’s cars.

The other day John’s mom was talking about the day her father got a tractor, which changed everything. At first only the richest farmers had tractors that looked like steam locomotives, but the day her dad got a tractor and no longer had to plow with mules was a banner day that she still remembers. All we have today is the day we got our 3rd iPhone. This is something that happened in the 1930s and wagons were still being used until the mid-1950s. John has seen them in use in Europe even in the 21st century.

Last summer Ken drove by a farm on the San Juan Islands where they don’t use tractors. It is extraordinarily expensive to grow lettuce under those conditions, but now they can sell it for $8 a head. He first saw the mule-pulled cart plowing a field before he saw the marketing stuff and he was wondering what was going on over there. Then they ate in some restaurant later that week where they got all their produce from no-tractor-farm.

+ Going through rural guitar stores, collecting vintage denim (OM344)

When John was in Harvey Danger they used to tour across the United States, stop in little towns, and go to guitar stores. The guy behind the counter of a guitar store in Nashville or San Francisco knew what he had and you could negotiate with him, but if he had a nice guitar you weren’t going to chisel him down much, but if you were way out in the mountains somewhere outside of Coeur d’Alene Idaho you might get guitars for pennies on the dollar compared to what they sold for in Seattle because he was not selling to a global market, but to the people who were buying guitars in Coeur d’Alene Idaho.

The Internet got rid of all those inefficiencies and one time in 2000 John went into a guitar store in Hillbilly America and the guy behind the counter was a West Virginia guy who had been living in this town of 250 people his whole life. There was some super-cool guitar on the wall, John asked about it, and the guy said: ”On the Internet those go for $2200!” while John was hoping to get it for $300 and wondered: ”On the Internet?” It was the beginning of the end for ever finding a cool guitar again.

In the late 1980s / early 1990s John had a friend with a (Ford) Galaxie 500 who would go on month-long trips to Montana to go to charity stores in little towns and to figure out who the old farmers were because he was looking for really old Buckleback Levi’s and he would bring them back to Seattle and sell them for $40.000 to Japanese collectors of vintage denim. He kept a couple of them under his bed hermetically sealed in a bag that he called his escape money. If he ever had to run he could just throw them in a bag and he could survive. The market for a lot of that stuff has crashed by now. 

+ Finding a genuine Louis Vuitton bag in a thrift store for $6 (OM344)

Two years ago John was in a junky thrift store in Los Angeles. There wasn’t anything interesting there, but then he looked across the store through the blue cloud that is every Goodwill and he saw a Louis Vuitton Speedy 30, a little handbag that you couldn’t even put a cockatiel or a little dog in. John has seen many fraudulent Louis Vuitton bags because every thrift store has 4 of them.

This one was bathed in a glow and John crossed the store at a speedwalker pace to have a look. It had been loved, it was very well cared-for, but it was very old. He found the serial number, looked it up online while he was standing there with the thing on his arm, it all checked out, and it was $6. It was just sitting on a pile of thrift store garbage like old notebooks and umbrellas. This thing was worth quite a bit of money and he gave it as a gift to a young woman.

+ John’s walk across Europe originally planned to be from Vancouver to Tijuana (OM344)

On his [[[the big walk |walk across Europe]]] John would come through small towns and people would ask him what his shtick was. A lot of times in the Balkans they understood what he was doing in terms of a pilgrimage, they assumed he was going to Jerusalem, and they were wondering why he was not wearing a big cross. John saw quite a few people, usually in bicycles, where their panniers had a bunch of signs on them: ”From Beijing to Paris”, which was still a thing in 1999. They were often rich Germans, Americans, or Belgians trying to find meaning in their own lives in the ”Around the World in 80 Days” spirit, just like John was, but they were doing it with the help of technology in the form of a cool bicycle.

John’s original idea was to walk from Vancouver to Tijuana, but then he thought about it and he realized that he was going to either walk along the shoulder of Highway 1 from motel to motel, or along the tidal flats, which is lame, and along the Oregon coast when the tide comes in you can actually get caught in some places where there is a nice wide beach and two hours later you are under water with nowhere to go. He thought about what other routes would be about the same distance, he took a ruler, applied it to different places in the world, and found that walking diagonally across Europe was an option.

When John was in Hof, Germany he immediately hit it off with the woman who was running the Pension. She didn’t speak English and John didn’t speak German, but she was a vivacious woman. The next morning over his eggs and bacon she brought a man into the room and introduced him as Das Newspaperreporter, a guy who sort of spoke English, and he interviewed John and the following morning there was an above-the-fold half-page article about him in the Hof newspaper (probably Frankenpost) with a photograph and everything, and she is probably showing it proudly to John’s son every day. ”Das ist Father!” It is hanging in a frame in John’s Pension.