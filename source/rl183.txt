This week, Merlin and John talk about
* Failing delivery services ([[[Packages]]])
* Delivery tracking ([[[Objects]]])
* John's girlfriend the ballet dancer ([[[Family]]])
* John's experience when his dad moved to Tacoma ([[[Parents]]])
* Studio engineers and auto mechanics are never wrong ([[[Factoids]]])
* The kid with the giant coke at Petticoat Junction ([[[Stories]]])
* John's attitude towards asking for money ([[[Attitude and Opinion]]])
* John's fiasco with a blown-out transmission of his RV ([[[GMC RV]]])

**The problem:** "John Pulled the Christmas Card", referring to John persuading an [http://www.bobsrvsales.com/ RV repair shop] in [https://en.wikipedia.org/wiki/Redding,_California Redding] two days before Christmas to let the RV sit there for a few weeks until John makes a decision on whether to repair it or not. 

The show title refers to a Hispanic mechanic at [http://www.napaautocare.com/store.aspx?id=84470 Napa AutoCare] in [https://en.wikipedia.org/wiki/Williams,_California Williams], California who had a look at John's [[[GMC RV |RV]]] when it had an issue with the fuel mixture.

Merlin and John hadn't recorded a show in three weeks since John had visited Merlin at his office in [[[RL182 |Episode 182]]].

In the very beginning they banter about a song with the lyrics "all pig iron", probably [http://www.azlyrics.com/lyrics/johnnycash/rockislandline.html Rock Island Line] by [https://en.wikipedia.org/wiki/Johnny_Cash Johnny Cash]. "It's a good road to ride".

Merlin had previously sent John a few comic books to read and the [https://www.amazon.com/Official-Handbook-Marvel-Universe-Comics/dp/0785158030/ Handbook of the Marvel Universe], John poured over that! Merlin usually doesn't recommend a lot of things to John, but the second season of [https://en.wikipedia.org/wiki/Fargo_(TV_series) Fargo] is a treat. It is called the new [https://en.wikipedia.org/wiki/The_Wire Wire].

During the show, Mike the building manager knocked on the door. "Thanks for the key fab". Mike was the one who brought in all the packages during the previous episode. He was the second ever guest on the program, because they had a cameo from the alarm tester at one time.