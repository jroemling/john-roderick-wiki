This week, Merlin and John talk about:
* Birthday cards from John's mom ([[[Parents]]])
* John's mom thanking him that he survived to his 30th birthday ([[[Parents]]])
* Poetry by Susan Polis Schutz ([[[Humanities]]])
* John’s Valentine for Laurie Basler ([[[Early Days]]])
* John at his first cross-country meet in High School ([[[Early Days]]])
* Hearing a washing machine and a jet airplane as music ([[[Music]]])
* Bookstores ([[[Books]]])
* Student doctors ([[[Attitude and Opinion]]])
* Intermission
* Unconditional love ([[[Attitude and Opinion]]])
* John not shying away from an argument ([[[Attitude and Opinion]]])
* Punching people in the nose has become unfashionable ([[[Attitude and Opinion]]])
* John punching somebody in the nose ([[[Attitude and Opinion]]])

**The Problem:** It’s become unfashionable to punch people in the nose, referring to John thinking that this is the reason the world is going to hell and to a story about John recently punching somebody in the nose who was assuming that we were living in a post-punch-in-the-nose world.

The show title refers to John talking about the non-existence of unconditional love and suggesting to write about The Conditions of Love, which sounds like a poem by Susan Polis Schutz.

They start the show singing each others names. John rang a bell that sounded like a meditation thing because he is meditating on how great life is and how good he has it. Every minute of every day is just a blessing. He is dressed already, he has had coffee, and he just feels so blessed!

John’s general feeling is that the world is going to hell and it is largely a product of the fact that we have made it unfashionable to punch people in the nose, but that is just not accurate. What John really needs to feel is that every day is a blessing.

[[include inc:draft]]

+ Birthday cards from John's mom (RL45)

John's mom expresses herself in greeting cards, which is hard to believe and very out of character, but John has a birthday card from her for every year of his life. She wasn’t greeting him, but she was greeting each new day with an aphorism. There would be a card with a sunset on it and some onion paper in front of the card that at first you look at it through a gauss and then you open the onion paper and the sentiment would be written in cursive, like if it was an excerpt or an apocrypha from the Bible, some kind of middle portion of the Bible that got taken out.

Somebody with white cotton gloves has to take it out of a hermetically sealed case, take it out of each Bible, which is the unerring word of God, and then there is this thing that says: ”You are the best son a mom could ever want to have!” You open it up and inside it furthers that thought with a poem. His mom would sign it underneath and sometimes she would echo the sentiment by saying: ”You are a good son!” She probably did believe that because moms have certain blinders where they think that their terrible child who is a force of pure destruction in the world is actually a nice boy. To Merlin it is a lot like the Pit-bull problem: They may just be remembering the days where she didn’t tear a child apart.

+ John's mom thanking him that he survived to his 30th birthday (RL45)

John’s mom thanked him on his 30th birthday for having survived //(see RW133)//, which was a very emotional moment. She said that from the time John was 15 until he was 30 every time the phone rang at night she was sure that they were calling to say that he was dead. She can’t believe  he was 30 and congratulations and she was really glad. She was explaining  a massive truth to John that continues to settle on his shoulder: In order for her to survive she had to inure herself against the possibility that John might die at any time. Psychologically that was a lot of work and a lot of heartbreak to say.

She said: ”I loved the years I had with you. Knowing that you were certain to die I had to be happy with the great times that we had!” She was trying to make it through those years where John was having his high adventures that make him such an interesting person. He was a homicidal maniac, which is not something you would say about a puppy who pees on the carpet, which is a handful, or an old lady who wears red cowboy boots, which is a pistol. Somehow he made it and now every day is a blessing.

+ Poetry by Susan Polis Schutz (RL45)

Susan Polis Schutz had quite a run in the 1970s and 1980s writing greeting cards with watercolors on the front and Merlin is about to read several of her works. First they talk shortly about what the name Schutz means in German. Merlin had a mnemonics professor in college who years later became the preeminent scholar of the Klingon language. It is very difficult to teach people poetry, and it is very difficult to teach people things in general now that we can’t use branding irons.

Sometimes you have to teach people something based on what it is not or should not be. That is how John teaches young girls what safe touch is.  First he does the unsafe touch. Richard Hugo has a wonderful book on verse. The Real West Marginal Way was his biography. Merlin forgot the name of the other book. He is not going to spend time talking about Bird by Bird (by Anne Lamott).

A great piece of advice for anybody is to avoid the word ”Love”. You are especially not allowed to use love as a transitive word for the first 9 years you are (in a relationship?). John has been saying that to every girl he has dated since 1986!

There is a verse by Susan Polis Schutz called ”I love you so much” //(see [https://www.fanfiction.net/s/473669/1/Three-Small-Words here])// and Merlin has been trying to find a way to say that to John because he is so glad John made it to 40-something. He is going to paint John something in watercolor and send it to him with tissue on it. It is very important to have metaphysical distance. You can’t be too matchy-matchy, you can’t be too close with things like the title:

> Since I met you I have been so happy
> except that I find myself worrying all the time. -
> worrying that I might disappoint you
> worrying that our relationship might end
> worrying that you might not be happy
> worrying that something might happen to you
> I have fallen in love with you
> and I guess I worry so much
> because I care about you so much
> -- Your Secret Admirer.

It is the most fucked-up poem John has ever heard, and makes up the next part as: ”I love you so much I watch you breath while you are sleeping and I think maybe about smothering you!” 

She has a lot of these and Merlin reads another one called ”Thoughts of You” //(see [http://www.oocities.org/vienna/8128/poems.htm#sisupieras here])//:

> In the morning
> when the sun 
> is just starting to light the day
> I am awakened 
> and my first thoughts are of you
> 
> At night
> I stare at the dark trees
> silhouetted against the quiet stars
> I am entranced into a complete peacefulness
> and my last thoughts are of you

If you imagine a 50-year old woman drinking tea, it is really super-sweet, especially if she is talking about her sick husband, but imagine this being a 32-year old man who is cleaning a gun, or even a 50-year old woman who is drinking Gin! John doesn’t want anyone to think of him as their last thought of the day and their first thought of the next day. It would still be something wrong with you if were a beautiful 26-year old girl in a flowered dress riding a vintage bicycle with flowers in the basket! There is no time after you are 14 years old when that amount of obsession about another person is healthy or good.

Merlin reads a Susan Polis Schutz poem ”Be my Valentine” //(see [https://www.andrews.edu/~mtse/poem-love2.htm here])//:

> Walk with me in love
> Talk to me
> about what you cannot say to others
> Laugh with me
> even when you feel silly
> Cry with me
> when you are most upset
> Share with me
> all the beautiful things in life
> Fight with me
> against all the ugly things in life
> Create with me
> dreams to follow
> Have fun with me
> in whatever we do
> Work with me towards common goals
> Dance with me
> to the rhythm of our love
> Walk with me throughout life
> Let us hug each other
> at every step in our journey
> forever in love

”Dance with me to the rhythm of our love” sounds suspiciously like a Rod Stewart lyric. One of his songs goes: ”Spread your wings and let me come inside” //(from Tonight’s the Night)//, which is about vaginal wings, about labia. It is a thing John might say to a stand-up bass as he is wrapping his arms around a large bass violin.

Merlin learned in poetry class to be careful about saying something that could be interpreted in more than one way, but John disagrees because everything you are saying in a poem wants to be interpreted in multiple ways. Merlin says there has to be a concrete image and it is okay if it is open to interpretation, but you don’t want a whole thing of ellipses.

She has to be talking about fucking, because ”The rhythm of our love”? What is that? It is just like the scene in The Graduate: Why did he take her to that strip bar? He wanted to dance with her to the rhythm of his love! Travis Bickle loved greeting cards.

There are people who fall in love with each other and call each other four times a day for the rest of their lives. They are happy and the rest of the world is just a backdrop to the play they are writing for and about one another. John can not identify with that personally.

Hume Cronyn and Jessica Tandy probably didn't call each other four times a day. They had jobs, they might have high-fived on their way out the door in the morning, and at night they sat around while he played the piano and she told ribald stories. Cary Grant was there with 4 or 5 friends, they had Martinis and then they went off to sleep in separate bedrooms because that is what people did then.

John sometimes uses the phrase ”The screaming in your head” and Merlin doesn’t want to sound cynical, but the reason you are calling anybody four times a day might be that you don’t want to hear the screaming in your head. The devil dogs are nipping at your heels and rather than keep running you need to call somebody and ask: ”How is your day? How is it going over there? Oh good, I am just sitting here, thinking of you!”

Merlin reads the lyrics from Some Guys Have All the Luck by Rod Stewart:

> The car overheated, I called up and pleaded, there’s help on the way
> I called you collect, you didn’t accept, you had nothing to say
> Some guys have all the luck
> Some guys have all the pain
> Some guys get all the breaks
> Some guys do nothing but complain

John counters with Then Wear the Gold Hat by F. Scott Fitzgerald:

> Then wear the gold hat, if that will move her;
> If you can bounce high, bounce for her too,
> Till she cry “Lover, gold-hatted, high-bouncing lover,
> I must have you!”

Merlin counters with the lyrics to Maggie May by Rod Stewart:

> It’s late September and I really should be back at school

John counters with Trees by Joyce Kilmer:

> I think that I shall never see
> A poem lovely as a tree.

Back to Maggie May:

> The morning sun, when it’s in your face really shows your age
> But that don’t worry me none in my eyes, you’re everything

That is really sweet to say to somebody: "You look old in the morning!" In the 1960s and early 1970s you could still say that to your Mrs. Robinson. It is like talking about the holocaust: You get it out of the way! Back then a woman in her 40s felt like she was in her 40s, whereas now a woman in her 40s for the most part refuses to admit that she is in her 40s and if you make any comment about her face in the morning sun she will be way ahead of you. You don’t say ”Your face” and ”Morning sun” to a woman who is older than 26, and saying something about her face in the sun sounds like she you think she either has damaged skin or that you can see every cigarette she ever smoked. She doesn’t want to hear that!

Merlin reads the last lines from the poem ”I Promise that I Will Love You” by Susan Polis Schutz //(see [http://www.imgag.com/product/full/ap/3032956/ here])//:

> I will be completely honest with you
> I do promise you that
> I will laugh and cry with you
> I do promise you that
> I will help you achieve all your goals
> But -
> most of all
> I do promise you that
> I will love you

Unconditional love is a hell of a promise!

Merlin reads another poem by Susan Polis Schutz (see [https://www.goodreads.com/quotes/203946-this-life-is-yours-take-the-power-to-choose-what here]):

> This life is yours.
> Take the power to choose what you want to do and do it well.
> [Take the power to love what you want in life and love it honestly.
> Take the power to walk in the forest and be a part of nature.]
> Take the power to control your own life.
> No one else can do it for you.
> Take the power to make your life happy.

She didn’t use the word ”love” once in the part that Merlin read. She is getting a little Ayn Rand-y there, but probably ”Take the power, asshole!” would be more Ayn Rand.

+ John’s Valentine for Laurie Basler (RL45)

//John also mentioned Laurie Basler in RL156 and RL305.//

When John was in school in 1980 they were required to buy Valentines for all of their classmates, which was awful because there were classmates who did not deserve a Valentine from John and he did not want one from them. They deserved an empty space where a Valentine would be!

John bought the one gross of cheap Valentines for all his classmates, but he was so crazy about Laurie Basler that he spent a large portion of his pile of quarters that he kept in jars and used to pour over his head in his bed with the door closed to buy her a giant red heart-shaped box of chocolates. He took it to school on Valentines day, everybody exchanged their little paper-Valentines and he got a little paper Valentine from Laurie Basler that said ”Happy Valentines Day! — Laurie”, but he was ashamed and did not give her the box of chocolates.

Every day from February 14th until the last day of school on June 20th he opened his locker and looked at that big red box of chocolates and said that today was the day when he was going to give that box of chocolates to Laurie Basler, but he never did. On the last day of school his dad came to help him clean out his locker and he said: ”Look at that! A big box of chocolates!” and he grabbed it and he ate it. Problem solved!

John was covered in shame while his dad was wondering why he had this big box of chocolates in there. He put it on the front seat of their Chrysler Newport Imperial, they drove home from school with all of John’s papers in a plastic bag on the back seat while his dad was sitting and eating all the good chocolates out of it, saying: ”Oh, this summer is going to be great! You are going to go to Tennis camp!”

By all appearances Laurie Basler had a crush on Brian Namany who was a little teeny elf-like boy who was the same size as the girls and who was pretty like the girls with non-feathered hair. He was not particularly suave, but he was pretty like Nick Rodes. John even liked him, he was completely unoffencive, but why should Laurie Basler give him a lot of attention?

She had to have been aware of John’s feelings, but John has made the same mistake his whole life: Assuming that the person knows how he feels, even though all of his outward actions were to look away when they were there. If they had ink-wells, John would have dipped her pigtails in the ink-wells, but she didn’t even have pigtails! She had blond hair and a Dorothy Hamill haircut because that was the fashion of the time due to Dorothy Hamill’s success.

John was 8-12 years old and she was John’s first crush and really the last time he really let himself ever feel that way about anybody. Who can take the heartache? Did he want to be 24 years old and have his dad eating that self-same box of chocolates? Did he want his dad opening up the box with the engagement ring in it, being like: ”Oh, look at this, a free ring!”

John and Laurie Basler went to different High Schools and John only saw her one more time at a cross-country running meet in his Junior year in High School. To make matters worse, Laurie Basler died of Leukemia and it broke John’s heart: ”Champ! Wake up, champ!” (Old Yella) John never told her how he thought about her. He woke up every morning and thought about her and went to bed every night and thought about her.

+ John at his first cross-country meet in High School (RL45)

John’s Junior year in High School was after his dad had decided that he was going to be a cross-country runner, but before they all realized that cross-country-running was not going to be the way for John to get into the Olympics. When he had his first cross-country-running meet he had never ran cross-country before and the other kids told him that he had to get out in front of the big group because otherwise he would get back in the pack and couldn’t move.

The gun went off and John sprinted. By the time he got to the half-mile mark of this 10K (6.2 miles) race he was 200 yards (180 m) in front of the next-closest runner and he thought of himself as the king of this sport. He was the greatest cross-country runner in the world and he was not even tired! People were cheering: ”He is a genius!”, but as he rounded the first turn and was headed halfway up the first hill his heart turned to stone and he could not get enough air into his lungs, no matter how hard he tried to breathe. One by one every runner from every High School in the state of Alaska ran past him as he gasped for breath.

The cheering died down and at the 2km (1.25 miles) mark he was limping like a flogged mule, but continued to run this race like a war veteran. He came so far last that when he crossed the finish line all the other runners from all the other schools were already on the bus and in their sweats while he dragged his ass across the line and laid on the grass gasping.

No-one really had anything nice to say anymore, just: ”Yeah, good effort!” John’s dad was already in the car, listening to 8-track tapes. ”And home we brought you shoulders high” //(reference to ”An Athlete Dying Young”, see [https://www.poetryfoundation.org/poems/46452/to-an-athlete-dying-young here])// - ”Pennsylvania 6-5000”

+ Hearing a washing machine and a jet airplane as music (RL45)

A long time ago John lived in a house with some people who had lots of piercings and giant earplugs made out of sink stoppers. At the time this was still very transgressive. One time he was sitting in the bathtub when somebody was doing a load of laundry and the washing machine was going: ”Da dumb da dumb da dumb da dumb” in the other room. He might have been very stoned. Jet airplane engine noises overhead were making a chord and all of a sudden the chord of the jet synced up with the sound of the washing machine and made some fantastic music.

John was right in between those two things and he was the only thing in the world except for maybe a termite that could perceive this moment of natural music being made. He was the meat of this sandwich, he was the receptor, a tree fell in the forest and John was the only one there! He felt like he was dancing to the rhythm of the world’s love! The same is surely happening right now to a guy sitting in a room somewhere in Indonesia, like in the movie Delicatessen: There is somebody fucking on a bed next door and somebody is sharpening a knife over here and he hears the music and no-one else can hear the music.

+ Bookstores (RL45)

John never went to greeting card stores as a kid. He has never looked at a single self-help book in his entire life and there were whole sections of the bookstore that he stayed away from. He just stayed over in military history and all kinds of history. There are so many exciting parts of a bookstore, why would you go to a section where people are trying to tell you that they are thinking about you?

John was also very embarrassed to read about how to have sex. Part of the problem of being a born romantic is that he felt that if you are going to be good at something, you should just discover it and not pollute your experience by leaning about something before you are actually trying to do it. To read about how to have sex after you have tried to have sex a few times makes sense in order to figure out what you are doing wrong.

+ Student doctors (RL45)

Every doctor knew they wanted to be a doctor when they were 14 years old because they are psychotic. John didn’t even know what he wanted to do that afternoon while all of his friends who ended up being doctors were already sure they were going to be doctors. John is terrified every time he sees a doctor because he knows they have been doing this since hey were 14 and they have no other experience of the world than this.

As a young man John spent a lot of time in hospitals being treated by student doctors because he didn’t have the money to get treated by a regular doctor and he would go to the university hospital where they would look at his problem, but there were going to be nine of them and every one of the students who was incidentally John’s age was going to take a turn poking his liver. That is how you get free medical care!

John was lying on a gurney and each of the medical students took a turn poking at him with their forceps while he would sit there, trying to make light of the situation. They would give him some non-effective treatment and he would limp out of there. John saw a lot of young student-doctors during that time with their wide dewy eyes and their sense that they are one day going to have a jet ski.

Merlin notes that doctors work ridiculous hours, it is like being with R. Lee Ermey, which is part of the hazing that affects all of our healthcare. John counters that this is to weed out the weak, the ones who don’t deserve a Porsche with a personalized license plate that says ”Doctor Feelgood” or the ones who wonder if there is something else they should be doing, like swimming in the Mediterranean or maybe, like this guy on the gurney, they should be drinking themselves insensible every night and getting in fist-fights. Instead they are reading about the Anterior cruciate ligament and are dreaming of that Porsche that is going to make this all make sense. 

The Ulnar Collateral Ligament Reconstruction is called the Tommy John Surgery because the baseball player Tommy John was the first to get it. After that all baseball players started getting it. John didn’t read Batman comics when he was a kid because he didn’t like them and he doesn’t know who Tommy John was. He agrees that Tommy John was a baseball player, but he was not the one who took acid and pitched the perfect game //(that was Dock Ellis)// because that was Rod Stewart who played soccer. The guy was tripping balls and nobody got a base! Tripping Balls would be a great name for his memoirs. Merlin and John should be writing people anonymous letters about improvement opportunities and coming up with the name of sports memoirs.

+ Intermission (RL45)

John was hearing some digital artifacts because he had forgotten to plug in his DSL and he was doing the Skype call over wireless. They restart the call after John plugged in his cable and it sounded much better. Merlin left it in because he cuts out so many things every week already. They record for 6-11 hours, they argue about religion for 4 hours and then Merlin cuts it down to the best 90 minutes every time.

Merlin left in what John had said about Charlemagne and Napoleon //(see RL40)// because it was a whole 3-episode arc. Charlemagne was a Franc, meaning a German, but that was before there was a clear division between Germany and France. His favorite town and the headquarters of his empire was Aachen, which is now in Germany, but right on the border.

They should do a show ”Charlemagne 'n Stuff” //(in reference to Hitler 'n Stuff)//! It is important to communicate that Merlin and John are human beings and not just two guys with an encyclopedic knowledge of everything who sometimes forget to plug in their DSL cables or who can’t lay their hands on their bell. John was very impressed how many people knew that his bell was from an old Parker Brothers game //(the game Pit, see RL41)//. John had not seen that game since he took the bell out of it.

+ Unconditional love (RL45)

People have this idea that unconditional love is the standard below which they will not accept any other kind of love. We tell each other that anybody deserves to be loved unconditionally and that such a thing is even possible and we tell it back and forth until people of John’s personal acquaintance who are unlovable sacks of shit believe that they deserve not just love, not just the love of their mother, but unconditional love from a partner. They are a disgusting person! How do they feel like they deserve unconditional love? All of us can only love conditionally and only warrant being loved conditionally ourselves.

Saying to a kid: ”I love you, but go clean up your room”means: ”I love you, but if you don’t clean up your room I will stop loving you!”, a temporary love-stoppage or slowdown. The love is going to pile up on the sidewalks and smell bad in the sun until they get the room cleaned. You see this all the time in our relationships with one another: ”I love you! I love you, but you really screwed the pooch last night on the whole washing-the-dishes-after-dinner thing, so I am going to respond monosyllabically for an hour or two.” It is all conditional love!

Unconditional love is like the word Incipient: You never hear people talk about "cipients". You hear unconditional love, but you never hear people talk about the obvious fucking elephant in the room, which is what should be the conditions of love. The Conditions of Love would be a terrific Susan Polis Schutz poems! Wasn’t that a Peter Gabriel record? It is a Kate Bush album and a Raymond Carver story //(novel by Dale M. Kushner)//. The number one condition people put on love is that the other person must love them unconditionally first. Going in with the condition that the other person can’t have any conditions is like asking the Genie for more wishes.

+ John not shying away from an argument (RL45)

John is a Zionist, a Rationalist, a Hobbesian, a man who is not afraid to argue with John Vanderslice about the price of a trailer that was not going to be bought and he is still arguing with him about it. Seeing that was a harrowing moment in Merlin’s life! He has seen John argue with people about things that didn’t need to be argued about until he realized after he heard John argue about it that it sure as shit needed to be argued about. 

John is not afraid to discuss a condition and they should establish what the conditions are: ”This is not what I signed up for! These nacho chips and this 14-year old girl in my dressing room is not going to put gas in my van!” Don’t try to pay John with Dasani water, he wants American dollars!

The first thing John says is: ”What are your conditions?” He is not the guy who comes with: ”Here are my conditions!” Part of the problem with the Vanderslice trailer was that John had even completely negotiated that, but the part John had negotiated had been re-negged upon vis-a-vis a third party that John had no litigious control over. Agreeing with somebody over the telephone about how something is going to go, and the next time you see them they will say: ”I didn’t agree to that!” really gets John going.

In cases like that you have to punch fucking reality in their face and if they say: ”I have no recollection!” you have to retreat back to what is at stake and ask yourself if you are prepared in this hypothetical instance to drive this large hypothetical van trailer back to Washington from California just on principle. The answer is: ”Absolutely yes! On principle I will take this trailer away from you and burn this trailer in the desert rather than give it to you for one Pfennig less than we agreed to on the phone!” You don’t do the thing of: ”Yeah, we will figure that out when you get down here!” and then be like: ”I thought we had it all figured out!”

+ Punching people in the nose has become unfashionable (RL45)

Until very recently you could avail yourself in a heated argument of the option of punching somebody in the nose. President Andrew Jackson punched a lot of guys in the nose and Charlemagne would take them out to the river and tell them to either convert to Christianity or try to breathe under water. Either we are going to have a Christian world or we are going to have a lot of dead people in this river!

Until very recently we could still say that a punch in the nose was on the potential list of rewards and consequences of a negotiation, but we have decided as a civilization and a culture that punching people in the nose is barbaric and is no longer an option, yet we have not evolved beyond the behavior that would warrant a punch in the nose.

People still exhibit certain behaviors all the time, like assholeishness or doucheitude that really don’t have any other recourse than a punch in the nose, but we have taken the punch in the nose off the table. Now we get situations where somebody is a douche, but you can’t punch them in the nose because that is uncivilized. We are at an impasse!

You can’t respond to a douche in any other way. You can’t argue with them, you can’t convince them, and there is nothing you can hold over their head. They are dancing in a field of flowers, douching it up, and they know there are no consequences because even making a fist, the threat of a punch in the nose, is considered an assault.

John doesn’t want to get punched in the nose more than anybody and he agrees that punching people in the nose is barbaric, but unlike a lot of his contemporaries he has been punched in the nose and he has good reason to not wanting to be punched in the nose. The threat of being punched in the nose actually changed his behavior at a certain point.

John was getting punched in the nose with surprising regularity and he wanted it to stop, so he started holding up his side of the bargain because he knew what was on the table. Either this was a huge conspiracy or he was getting punched in the nose because of something he was doing. He started to think that maybe he should stop being such a flaming asshole to people who are bigger than him or have violent eyes and he became a more moderated person.

John does not like to be the one who delivers punches in the nose either, but there are certain situations in life when someone at the supermarket does not [[[Keep moving and get out of the way]]] and when you ask them to either keep moving or get out of the way, preferably both, they turn to you and give you some horse-shit entitlement lecture.

You look at them and realize that the threat of a punch in the nose is the only thing you have because the situation warrants it and it is your only recourse, and yet if you do it this person is literally going to call the police and then you are going to lose your place in the big line, which is the line up the head of the class where people start listening to your podcast. You lose primogenitor!

+ John punching somebody in the nose (RL45)

A few years ago John was at an event where there was a hectic brawl where people were falling down, others were in trouble, and people were shouting his name, like: ”Come, help me!” A guy was shouting in John’s face so that he was getting spit on him, John told him a couple of times to step away and back up, but the guy was drunk and believed that he was funnier than he was and that he was smarter than he was and that he was quicker witted and had faster reflexes than he did. 

He was right there in John’s face and he said something about there being consequences. He was talking about a friend of John’s who was across the room calling John’s name. John doesn’t even remember how it happened, but when the guy said: ”There are going to be consequences!” and ”Pow!” John punched him right in the nose. Unfortunately it was a well-landed punch in the nose and the guy began to bleed all over the place. It did not de-escalate the situation, but now it was a free-for-all. The guy was screaming and the aid car (the ambulance) had to come for some other person who got hurt in this brawl. John felt like such a ding dong!

After the guy retreated and got surrounded by people who were petting his hair and telling him that he was going to be okay he got bold again and with blood on his shirt he stood up and confronted John again screaming, now getting blood on John, and John punched him in the nose again. 

John is really not proud of it! The following day the guy took to Facebook, it turned out that he was gay, and now it was a hate-crime and John had gay-bashed him. He was living in a world where people believe they are post-punch-in-the-nose and you can get up into the one-sober-guy-in-the-bar’s face, drunken-spittle on him, scream at him about bullshit and tell him that there are going to be consequences and believe you are living in a post-punch-in-the-nose reality. Then you get punched in the nose and you realize that you are not in a post-punch-in-the-nose reality.

Even when he sobered up and weighed the situation he still believed he was living in a post-punch-in-the-nose reality because he began to threaten legal action and began to call it a hate crime and used shaming tactics, operating on the assumption that a punch in the nose above all else, to resort to physical violence in a scenario where you are being emotionally violent (was off the table).

People who believe they are living in a post-punch-in-the-nose believe they can get up in your face and say there are going to be consequences and be physically threatening, but since a punch in the nose is impossible they don’t perceive themselves as threatening physical violence, even though everything they are doing suggests it. They think it is a giant costume drama or something they have seen on television where people get up in each other’s face and that it happens in a vacuum and it isn’t a 100.000 year old tradition of escalating a situation until somebody punches somebody else in the nose.

John confesses to being a little bit prehistoric in this regard, but there are some situations, particularly when you are standing on his shoes and spitting in his face. He was a university person and he was threatening all the different levels of which this was bodily assault. As a graduate student he had all the language and could write a long series of Facebook posts, but he didn’t know that John also is an amateur lawyer who knew that he didn’t know what he was talking about and John just blocked him.

At that point John was getting conscious of being a public figure and the sense was not that John was a guy in a bar who had punched an asshole in the nose, but that John was a person who’s name was recognizable, so it was the ”I spilled McDonalds coffee in my lap and I am suing them for $1 billion!” The guy's reaction was not commensurate with the incident. But that coffee actually was too hot! ”The hot tub is too hot, I flew all the way down here! We got the small chair just for you and you are never happy! It is not funny, it is too hot!” //(reference to [https://youtu.be/5p0QtJMKt1s this one]: ”A Vodka Movie by Zach Galifianakis, Tim and Eric”, also shorter version [https://youtu.be/YEvdAK049LA here])//.

He was blowing his injury out of proportion, he posted a picture of himself and he did look terrible with black eyes. John does not think that punching somebody in the nose is an appropriate reaction and it should not be on the list of consequences we have in civilized society. He does not want to be punched in the nose either and he doesn’t like punching somebody in the nose. The point of the whole thing is that if you don’t want to be punched in the nose, don’t drunkenly stand on somebody’s shoe tops! That should be on the title page of the owner’s manual for being an adult.

A lot of people inhabit their own body and soul and think that they are smaller and less significant than they appear to other people. This guy’s perception of himself was probably that he was an elven delicate flower and that it was obvious to everyone, but he did not realize that he was a 5’11” (180 cm) screaming monster of an alcoholic. His reaction to being punched in the nose was somewhat based on his self-perception of being this defenseless victim that he has felt he has been his whole life.

If you really get to the bottom of most people's problem who cut you off in traffic or who are total cocks to you in the Supermarket, you will find that they feel like a victim all the time. Everybody is stepping on them and that is how they are justifying being a shit-head all day. As somebody who is a giant pussy Merlin is not qualified to really address this, but it seems that there actually was an opportunity for both John and the guy to learn.

John should have talked to him afterwards and explain why he was being punched in the nose, but the problem with alcoholism is that it clouds your thinking and it makes you not interesting to talk to. John learned that even though he had not punched somebody in the nose in many years he was still able to do it and walk away, but he was not feeling good about himself and he felt ashamed because he had had more than a few cups of the Kool Aid that says: ”Violence is not a solution!”

When John replayed the events in his head he absolutely arrived at the punch-in-the-nose moment and each time he punched him in the nose anew. John also replays all these podcasts in his head although he doesn’t listen to them, but he wonders: ”Should I have punched Merlin in the nose at that point? No, I shouldn’t have! He is a long way away!” John has made major progress in life by gradually coming to the understanding that he is not a victim of anything and anyone and he has eliminated the sulky option of feeling like other people are inflicting life on him.