This program is hosted by judge [https://twitter.com/hodgman John Hodgman].

Their two guests are novelist Kevin Vennemann, 37 years old, the famous German author of [https://www.amazon.com/Close-Jedenew-Contemporary-Art-Novella/dp/1933633395 Close to Jedenew], and translator Ross Benjamin, 33 years old.

John Hodgman recommends the novel [https://www.amazon.com/Leftovers-Novel-Tom-Perrotta/dp/1250054222 The Leftovers] by Tom Perrotta that is currently made into a television show for HBO.

> Welcome to the Judge John Hodgman podcast. I’m your guest bailiff John Roderick. This week: Visitation Rights. Kevin brings the case against his friend Ross. Kevin lives in Brooklyn and Ross lives in Nyack, a small town further North. Kevin is tired of making the trip to visit his friend in the country and finds that Ross should come to visit the city once in a while. Ross said that things are pretty much fine as they are. Who is right? Who is wrong? Only one man can decide! Please rise as judge John Hodgman enters the court room. 

+ Kevin and Ross (JJ154)

Kevin and Ross first met in Frankfurt, Germany at a literary conference where Kevin was reading and then again in Berlin where Kevin lived. Ross lived in Brooklyn. They immediately clicked and started working together. A couple of years later, exactly 5 years ago, Kevin moved to New York City right around the time when Ross moved to Nyack, a lovely river town in the lower Hudson valley. Carson McCullers and Joseph Cornell lived there, and Edward Hopper was born there who's house was converted into a gallery. Ross moved to Nyack because he didn’t want to raise a child in Brooklyn. Kevin is not married, but he is working on something. He has no children and no pets. At his phase in life it is appropriate to live in Bushwick, Brooklyn. His a smaller network of friends could not tolerate to lose him.

Kevin wants Ross to visit Brooklyn more often, but he doesn’t. They only get to spend time together when Ross visits Kevin in Nyack. Door to door the trip it is about 2 hours by train and Ross visited Kevin almost every week in the beginning. Now he still does it about every 3 weeks. Kevin had a hard time finding a hospitable apartment and Ross’s family gave him some refuge. He was not a nuisance at all! Because Ross had already moved to Nyack when Kevin came to the states, they never lived in the same city together. 

Ross has visited Kevin three times, but not in his current apartment. They have seen each other in Manhattan instead and Ross' wife was there one of those times. Kevin had to leave his first apartment because the gas was leaking on the water boiler, so he didn’t have safe hot water. Now he has a one-bedroom apartment with a non-foldable, but comfortable IKEA couch.

Ross admits that he has not managed to visit Kevin in Brooklyn despite best intentions. It wasn’t that Kevin had invited him numerous times and Ross had been saying no, but they just got into a habit. As a novelist and a translator, how can they be very busy? Kevin is writing a doctoral dissertation and Ross is translating Kafka’s diaries. Kevin is not mad or anything, but he would think it would be good for Kevin to get out of his comfort zone and spend the weekend in Brooklyn. They could see some art exhibition by Robert S. Duncanshon, the single African American member of the Hudson River School, or they could go to some bars and hang out, which Kevin not normally does.

Ross suspects that he will be ordered to visit Kevin, but he would like to retain the possibility to visit him voluntarily. John wonders if a cultural difference might be part of the frisson. European friendships might have a more formal component while American friendships have more of the casualness that Ross is showing. There is a loyalty component in German friendships and when Kevin lived in Berlin he could be sure that friends would call him back or visit him. He also doesn’t want Ross to be ordered to visit him, but to be ordered to leave his comfort zone more often. 

+ The ruling (JJ154)

Ross has been a terrible friend, even though he has the most ironclad excuse of all times: Having a little baby. He does himself and his friendship a disservice if he does not maintain contact with the humans in his life who do not have children. As gracious and non-confrontational as Kevin is, he does not want to come up to Nyack and binge-watch Walking Dead every week with Ross and his wife. It is important for everybody including Ross’ wife to maintain a remnant of their independent adulthood. It is imperative that Ross will visit his friend, especially now that he has been bullied into actually making a plan. Neither of them wants John Hodgman to order a visit in New York City and for precisely that reason he is ordering it, otherwise it will just fall by the wayside. Also: Specificity is the soul of narrative, not ambiguity.

John suggests to bring the child to the city, not that the child is supposed to remember anything, but the adults need to get in the habit of thinking of their child not as an impediment to living a fully fledged life. It also builds up their anti-bodies. For the first visit though they recommend Ross to leave the child with the mother. 

John Hodgman orders them to open their calendars right then and there and fix a date. They go back and forth a couple of times. Ross’ wife is supportive of Kevin’s case. They agreed to meet on April 5th at the American Galleries at the Metropolitan Museum.

Ross agrees that having been ordered to socialize will be good for him. He is glad that there is a concrete date and it will no longer hang over as a neglect or failure, but he does feel deprived of the opportunity to have voluntarily demonstrated his will to do it.

+ John does not speak German (JJ154)

John doesn’t speak German despite his interest in WWII. He does understand quite a bit of casual German and he really aspires to speak the language, because it is his favorite of the European languages. He didn’t learn about it until late and he has never undertaken a concentrated study of it. Berlin really is fantastic! 

+ Clearing the docket - Listener mail (JJ154)

++ Dougold Lamant of Vinnipeg

Dougold explains the etymology of Winnie the Pooh: Winnie comes from a real bear cub called Winnipeg that was picked up by a Lieutenant in White River, Ontario. He took the bear to England where the bear ended up in the London zoo and was a favorite for children for years. There is a film called ”A Bear Named Winnie” that proves this story.

++ Lisa

Lisa and her boyfriend have opposing views when it comes to parking. She always wants to look for a good spot first before parking in the back, while he enters the first spot he sees which is often the furthest away. John says that the goal is always to park as close to the spot where you are going to go after you have parked in order to eliminate unnecessary walking.

+ Outro (JJ154)

John and John Hodgman will have a show in Santa Fe, New Mexico on June 2nd. The onus is on John to read Game of Thrones, a Lord of the Rings style page turner in order not to stand there making faux passes as he is trying to talk to George RR Martin about a book that everyone he meets adores and lauds. John doesn’t want to be the fool who stands there and asks ”Are there hobbits?” and everyone glowers at him. John Hodgman tells John that these books are not at all like Lord of the Rings and they are really great. They inject the harsh reality of medieval life and details of medieval warfare into a fantasy setting. The weaponry, gorgets and murder holes are going to be right up John’s alley.