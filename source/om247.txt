This week, Ken and John talked about:
[[toc]]

[https://www.omnibusproject.com/247 Official Website], [https://aphid.fireside.fm/d/1437767933/8658dd0c-baa7-4412-9466-918650a0013d/c8ae9e78-29b2-4b07-af1b-b92952664214.mp3 Download MP3]

+ Astrology (OM247)

John is not a believer in Astrology, but he finds it fascinating. He is a Virgo man, and when he lived in a community in his early 20s that followed astrology everyone rolled their eyes because of course he was. Apparently it means that he is particular, controlling, creative, and breaded. The same adjectives often work for multiple signs and sometimes an adjective and its opposite can both apply for the same sign: You are super-dependable, but also whimsical.

Ken is a Gemini, of course! His issue with astrology is that it does not suggest a mechanism. What is the Midi-chlorians of astrology? Organized religion at least posits a mechanism and no matter if you are a Pagan, Norseman, or Methodist it is essentially the same mechanism with some omnipotent Sky-God who put us all here. Ken would be happy with Astrology if there was some pseudo-science, like: When the planets line up against a certain constellation, gravity pulls the brain-cells of a developing newly-birthed baby in one direction, therefore making you more stubborn if you are born in August.

Astrology is the ultimate [[[Spooky Action at a Distance]]] and it is a very plausible form. If something is going to affect whether or not your are temperamental because you were born in October, why not a big giant star? It is a Schroedinger’s Cat, a function machine, a red light on a box, all those things at the same time!

+ The song at number one of the charts when you were born (OM247)

Ken’s version of astrology has always been: What was the Billboard Number 1 song the week you were born? It surely has more cultural influence on anyone than where Mercury was. John was born in the waning days of the Johnson administration in 1968, but Ken has a really hard time guessing John’s birthday on the show. It was not in the spring, but in September. John missed Hey Jude by one week and his birthday song is People Got to Be Free by The Rascals. At least he missed Hello, I Love You by The Doors! August was pretty rough! It seems to prove Ken’s theory because what song could sum up John Roderick better than People Got to Be Free?

They continue to talk about what songs were at the top of the charts in 1968. John says 1968 was a great year for music and although People Got to Be Free is not bad it is not the top tune. It doesn’t have to be the best song, but it still influences your personality and that is why John is a free spirit and a rascal! When he was a tiny little baby this was blaring out of car radios everywhere they went, he was sitting on I5 with his mom in his little bassinet and they were taking him back to Kingston, Washington. Nine months earlier the song on top of the charts is Hello, Goodbye by The Beatles, which totally makes sense. September-babies are basically all conceived on New Year’s Eve.

Ken was appalled to find that in late May of 1974 the number one was a novelty song, and not even the good kind, but Ray Stevens’ The Streak. They continue to talk about songs on the top of the charts in 1974. The Streak was not so indelible that it propelled the fad of streaking, but the fad was such a big deal that even a song like The Streak could keep Band on the Run, a beautiful mini-symphony out of the number 1 spot.

Ken wished it was a John Denver or an Elton John song, that would explain him much better. John is 5.5 years older than Ken which accounts for a lot of his greater sophistication and worldliness. The song The Streak influenced Ken in the way that he later became famous for his streak on Jeopardy! There is something of The Streak in him, but it is a yellow streak, not the nudity angle.

+ John’s cousin having a naked kid rule in her house (OM247)

John does not come from a naked family and Ken almost seems like a never-nude to John. Even when he streaks he wears sweatpants and hoodie and he calls it Jogging. John’s dad was from the Greatest Generation, he was born in 1921, and they were no hippies, but John's older brothers and sisters were.

John’s first cousin once removed was a generation older. She was a hippie and when she famously married a priest it was officiated by Timothy Leary. They were in Life Magazine and they were part of a whole hippie scene. She was a nut and not a good mom either, and she had a rule in the house that kids under the age of 8 had to be naked.

John once went to her house at the age of 7 when he was already self-conscious about nudity and he was told to take off his clothes and to run around in the yard with a bunch of other naked kids. It was not freeing! He got used to it halfway through the day, but then he looked up and saw a neighbor kid watching them over the fence and he was so humiliated that he ran into the house and into a closet and hid in the closet until the nightmare was over.

Ken wonders if all naked families are a post-counterculture thing. Europe has sauna-culture and the Japanese are always bathing with their kids. Nudism is more accepted in Germanic countries, places where it has not been sexualized so much. It doesn’t have to be a counter-culture. John thinks it was the other way round and has not been desexulized in America.

If a family of 8 people are all sleeping in a one-room house you can’t be that precious about it. Nudity is fine if you are under 10 or over 40, but not if you are 13, and that is really the only time American society is really all into naked boys hanging out together, that is when you start showering in middle school or high school because you smell, or you are going down the Mississippi on a raft.

John was a shy kid who didn’t let his parents into the bathroom after he was 7 or 8, but all of a sudden he was 12 and had to be in a gym shower? It was awful! At his school if you didn’t shower you couldn’t get an A in the class and the coach would stand in the dressing room and say repeatedly: ”[[[Shower down to get an A!]]]” John didn’t want to shower down and he didn’t want to get an A. He never got better than a C in gym anyway. Ken is a non-athletic kid who always got an A because they said: ”Look at him trying out there!”, but John didn’t try, he sat in the corner sulking like he did throughout school.

Ken had European friends who came from naked families and that was default-status at home. Europeans are always rubbing the American’s noses in that, like Canadians do with healthcare. It seems hard to see the upside of knowing a lot of your middle aged parents’ naked bodies or your siblings or your friends or your friends’ parents. John is not a never-nude, get him naked as far as he is concerned, but not around other people //(in RW111 he said that he was a bit of a never-nude in the past)//.

+ Nudists at the beach of the Danube (OM247)

At one point John was [[[The Big Walk |walking across Europe]]], which the listeners have heard about. When he was walking along the Danube out of Vienna he suddenly started to see naked people. Pretty soon he was on a stretch of bike trail along the Danube which was understood to be the naked beach for a mile or two, but not sequestered behind a fence.

At first he was scandalized, after 10 minutes it became somewhat normal, after 20 minutes he started to feel: ”What is the big deal?”, and after 30 minutes of waving and tipping his hat to naked people he thought that nakedness was normal. By the time he was out the other side he wondered why not everybody was naked! It was a hot day and it stopped being scandalous or even titillating, but he did not take off his clothes.

+ Nudity in America (OM247)

One time John was swimming in the Caribbean with a friend who took off his swim suit and was swimming naked and dove down to pick up a conch shell at the bottom of the ocean. John noticed he was naked and as they swam around for a while John felt dumb because his friend was naked in the ocean and John was in a swim suit, so he took off his swim suit and they swam around naked, which was nice. John didn’t want a turtle to bite his wee-wee.

In Seattle there is nude bike riding, but only on the solstice (World Naked Bike Ride Seattle) and when it is warm clothing becomes optional in parts of certain beaches, like the North part of Golden Gardens. In the early 1990s they used to have a nice naked beach off of Capitol Hill, but it turned into a creep beach. They sat in the corner in the tall grass, but didn’t do anything.

When Ken was in Belize earlier this year //(see OM245)// there was a sign: ”Clothing optional beach. No staring, no gawking!” and he literally couldn’t tell if it was an actual sex-positive best-practices sign or if it was a funny beach sign like ”No working during drinking hours!”, or a ”Welcome to our ool” sign (because their pool has no pee in it).

Last summer Ken climbed up to the sun dial at Gas Works Park with his kids and there were a bunch of nakeds with their bikes hanging out in the sun dial during solstice. There is a strength in numbers if you are naked. If it had been one naked person, you would say: ”Put your clothes on!”, but if it is a bunch of nakeds you assume that there is a thing happening and the cops don’t care.

+ John holding up the toy train at the Woodland Park Zoo (OM147)

When John was a kid they would go the Woodland Park Zoo and there was a little narrow-gauge railway that went around a little gated amusement park area inside the zoo. It was a wonderful little Knott’s Berry Farm half-scale railway that fascinated him and when he was 7-8 years old he started to feel a little proprietary about his train and it was no longer enough for him to just ride the train passively like a little tourist or some kid on a field trip because his family went to the zoo all the time.

One time he asked his mother if he could borrow her handkerchief and he tied it around his face like a bandit’s hankie and waited in the bushes for the train to come by, jumped out of the bushes, ran behind it and jumped up on the back. He hooky-bobbed on the back of the train, pretending to be a cowboy, had a wonderful time, and the people on the train thought it was funny, so after that every time he went to the zoo he would bring a cap gun and a handkerchief and rather than see the animals or go to the arcade to pop balloons he would lurk in the bushes, waiting for the train to go by, run after it, jump up with his cap gun, and say: ”This is a stick-up!” and then ride the train.

John did that until the zoo had to put restrictions on. They posted a guy on the train and fenced off the bushes. The kids got into it, it had become a thing and John ended up making it a thing he did whenever he visited amusement parks, like the Nut Tree outside of Sacramento and at Knott’s Berry Farm where they actually had adults doing that. John would always run after the train and after a while it became the only thing he liked about amusement parks.

+ John going streaking at Gonzaga (OM247)

John went to Gonzaga University in the late 1980s, but at that time it was like other places were in the 1970s, still trapped in time. One night someone came through the dorms and said that at 11pm they would all be streaking over to the library. The streaking fad was only really a thing during about 6 months in 1974, but in the 1980s everybody was weirdly nostalgic for what seemed like a really fun time that had just come to an end. By 1985 AIDS had made sex not fun, Reagan had made drugs not fun, Animal House had just happened, and why did they miss it? Maybe they could recapture the glory days if they had a kegger and go on a panty raid?

+ Pinching (OM247)

Streaking was mostly a sausage-fest and whenever girls would go streaking they would get pinched. It was a harmless-seeming form of sexual harassments and a comic punch-line. The secretary straightens up at her desk when the lascivious boss goes by in a comic in Playboy Magazine, or a stewardess would get pinched. What a weird come-on: "Let me pinch someone’s buttocks and they will know of my interest!"

John’s mom was a waitress in the 1950s and to this day she hates Shriners because they pinch waitresses. The only thing Ken knows about them are the little hats, the little cars, and the little fingers. For most of John life he hated Shriners because they pinched his mom. Then people started wearing Fezes as part of celebrating nerdiness and all of a sudden John was surrounded by Shriner Fezes and had to make peace with them. John's mom hates Napoleon and Shriners, but the list is longer than that, they just haven’t covered all the topics on the show yet.

+ John running naked in the Palouse (OM247)

One time John was down in the Palouse, a farming area in South Eastern Washington with big rolling hills and very fertile ground. He was there with a group of people in the middle of the night and who knows what they were under the influence of, but they all took their clothes off and ran naked through freshly plowed fields. It was extremely liberating!

You can run really fast when you are naked, which might be an illusion because you are feeling breeze on parts of your body that don’t normally feel breeze and you are getting more jiggling from parts of your body, the kind you would feel if you were running at 100 mph (160 km/h) with clothes on.

+ John rolling in the snow naked in Alaska (OM247)

One time John was in a hot tub in Alaska at -10 degrees and they jumped out of the hot tub and ran across a snowy field and threw themselves down and rolled in the snow around until their skin was burning.

Another time John was in a sauna and they ran down to the end of the dock and jumped into the lake.

In his war movie podcast [[[Friendly Fire]]] John often sees David Niven. Just recently they have watched The Guns of Navarone. They took two years to get to that movie because they pick films at random with a 120-sided die and they have over 200 movies on the list. This is why the first 6 months wasn’t all the greatest films and then they would spend the next 7 years watching weird war-themed Dutch white supremacist porn movie productions of Norsemen.