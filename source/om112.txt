[[toc]]

+ John’s grandfather and grandmother met in World War I (OM112)

John’s grandfather and grandmother on his father’s side met in France during World War I. He was an officer in the infantry and she had studied opera in France before the war and went back to France to entertain the troops in some kind of proto-USO. She wrote a book about her experience called [https://www.amazon.com/nightingale-trenches-Louise-Rochester-Roderick/dp/B0006BPRJC A Nightingale in the Trenches] (see RD2016 and FF44). Ken imagines how good-looking John’s grandfather must have been because there was one woman for every 300 guys and he was the one who snared the nightingale in the trenches. 

The book is basically a long recounting of all the handsome officers who wanted to take her out. She has stories of riding with General Haig in a car to a party where champaign was flowing out of a 10-foot (3 m) tall tower. John’s grandfather won her heart primarily because he was a Welsh liar and convinced her that he was really extraordinary when in fact he was just a normal person. John’s experience in his family is that if you are competing against other suitors who are handsome, rich, suave or talented, all you need is to be able to sing and tell half-true stories to win over your sweetheart. It is very hard to imagine how John could be a descendent of these two people, one of whom was a talented singer and stage presence and the other one told elaborate lies.

John thinks his grandmother had a pretty good time in the war. When John was growing up, her brother, John's great-uncle Al, would speak to him in war-time French and they would sing the songs of the war. World War I was a pretty merry jaunt for them, but John’s grandfather was broken by the war and never really recovered. He died drunk in a hotel in Los Angeles without a penny to his name. Prior to World War I war was pretty mobile. Battles happened on a field of battle where two armies came from opposing sides and clashed. The victor took the ground and the defeated army either retreated from the ground or was destroyed. It was a war about conquering territory. World War I pioneered a static war of attrition.

+ John getting Welsh Rarebit for his birthday and for Christmas (OM112)

Christmas has become weaponized by this idea of whether it is a super-religious holiday with all the culture that entails, or whether that is offensive. What Ken always liked about American Christmas is this weird no-mans-land between the two extremes, this quasi-secular Christmas that is not just snowflakes, evergreen trees and the stuff you can get away with in a public school. It has elements of Peace on Earth, Good Will, and specific ideas and images that come from carols and European traditions. 

Yet we accept it as a common civic season of weird, diffuse religion. It has pagan roots and in America you can celebrate Christmas in 1000 different ways, but Ken disagrees and opening your presents on Christmas Eve is wrong and weird because you should open them on Christmas morning. Having pasta or pizza on Christmas is weird and you should not do that either! Ken normally eats ham, but roast beef would be more British and traditional. You got to have your Yorkshire Pudding!

John has Welsh Rarebit on Christmas morning, which is just Cheese Toast, but his mom makes it with biscuits with ham and cheese sauce on top of it, like a Benedict-ish thing except with cheese. They never really put eggs on it, but they certainly could. John’s mom has been making this meal for him on Christmas morning his entire life and it is not Christmas if he doesn’t get his Welsh Rarebit. It doesn’t work the other way around because there has been Welsh Rarebit inflation in his family and John started asking for it on his birthday as well. 

You could make it any day of the year, but no-one ever does and John is not even sure if he could make it the way his mother makes it, but she makes it for John two times a year, on his birthday and on Christmas morning. It is the only thing in the world with Welsh in the name and it is very on brand for John. He didn’t even know that it was called that for most of his life, but it was just the thing he had for breakfast on Christmas. Only later did he realize that it was Welsh Rarebit.

+ Ken arguing for fruit cake (OM112)

J: If I was working for the Chicago Tribune and I had a style manual that just was devoted to Ken Jennings, it would say ”Loves fruit cake!” 
K: Too many people are against fruit cake just because of... 
J: It is terrible!
K: It is fine!
J: It is terrible!
K: It is fine! It got those little jelly... if you were just eating candied fruits, you wouldn’t be like ”Nah, candied fruits!”
J: There you go: You don’t eat candied fruits! That is what is terrible! Who eats candied fruits?
K: There is nothing wrong... if you were just sitting eating candid fruits, you wouldn’t be like ”Ugh, those are awful”, but you would be like ”Mmm, these are like fruits, only candied”
J: I don’t think you would! I think you would not be eating candied fruits unless you were a grandmother or unless you lived in [https://en.wikipedia.org/wiki/Bob_Cratchit Bob Cratchit]’s house.
K: See, this is the thing: You are worried about the image of liking fruit cake or the ingredients thereof!
J: No, I am not, I don’t care!
K: I you were actually eating these things, you would be like: ”This is actually delicious!”
J: My house is decorated so much more grandmother than even your grandmother’s house!
K: In that there are piles of newspapers from the 1990s? That is true of both houses!
J: I’m not opposed to grandmother-optics!