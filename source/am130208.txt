This show is hosted by [https://twitter.com/OphiraE Ophira Eisenberg] and puzzle-guru [https://www.npr.org/2012/05/10/152460796/meet-the-puzzle-gurus John Chaneski], recorded live at the Bellhouse in Brooklyn New York.

++ Round 1: As seen on TV

Their first two contestants are Valeri Glassman and Adam Jaeger. 

After the first round of questions, they welcome their house musician John Roderick as a replacement for Jonathan Coulton. They have traded their regular bearded guitar player for another more enchanting and more beautiful bearded guitar player. He is also taller and can eat more in one sitting. John is playing the Jonathan Coulton song Skullcrusher Mountain in homage to him and also as a form of mockery. 

++ Round 2: Something in Common

Their next two contestants are Tom Waisuanga and Dean Rosenbloom, playing a game called ”Something in common”

++ Round 3: Sing What (Misheard lyrics)?

For the next round, their guitar-wearing fool and musical guest John Roderick and their puzzle-guru John Chaneski are on stage. Their contestants are Catherine Sommerfeldt and Stephen Straffort and they start the round by talking about Karaoke. The game is about misheard song-lyrics, also known as Mondegreens. John mentions the Creedence Clearwater Revival song Bad Moon Rising as an example that is often heard as ”There is a bathroom on the right” instead of ”There is a bad moon on the rise” John is going to intentionally mangle the lyrics of famous songs and the contestants have to sing the real lyrics. During the round they find out that Mellonball is John’s stripper name. John admits that he barely knows one of the songs because he is metal and he doesn’t know anything about pop songs. For one of the lyric he sings ”Flu, rickets and parasites” instead of ”Two tickets to paradise”

++ Round 4: He was in that (Movies)

Their next two contestants are [https://twitter.com/vanwinklehannah Hannah Van Winkle] and Jason Shapiro. They talk about movies.

John is playing Scared Straight by [[[The Long Winters]]].

++ Round 5: No I in Team (Sports)

Their next two contestants are Leo Arey and Brian Colley and they have to replace some vowel in a sports team name with the letter I. 

++ David Rees, How to Sharpen Pencils

Joining them is their VIP (Very Important Puzzler) writer and artisanal pencil-sharpener David Rees, the author of the book [https://www.amazon.com/How-Sharpen-Pencils-Theoretical-Contractors/dp/1612193269 How To Sharpen Pencils] and of the Rolling Stone comic strip [https://en.wikipedia.org/wiki/Get_Your_War_On Get Your War On]. 

When people ask David what he does, his one-line answer is ”I don’t make much money. I’m a struggler” One time he was on a Caribbean cruise in a hot tub with an elderly woman who asked what he did and he told her he had a pencil sharpening business, which was a paradigm case for that type of conversation: 1) He was in a hot tub with an elderly woman 2) there were a few bewildered back-and-forth until she got really interested in his pencil sharpening business and had questions about techniques, pencils and stuff like that. 

David offers a really sharp pencil at a premium. He has a variety of tools in his toolkit, everything from a box cutter or a pen knife to the world’s most expensive double-burr hand-crank sharpener to single-blade pocket sharpeners and sanding blocks, all manner of devices and techniques. He also bags and returns the pencil shavings along with the pencil and a certificate. The pencil is placed in a shatter-proof display tube with a label showing the tools he used to sharpen the pencil, the conditions under which the pencil was sharpened, the date of the sharpening, the sharpener’s rating, all these things. 

It is this whole suite of objects that his customers can either chose to utilize in a very industrial instrumentalist way or something they can share with their friends as an inspirational talisman or conversation piece. ”He found a void and turned it into a niche” In 20 years, when MBA students at Harvard are going to study his business model and his success, that is the catchphrase they are all going to get tattooed on their foreheads.

David used to be a left-wing political cartoonist, but eventually he got tired of all the money, the babes and the cocaine and he decided that he would quit cartooning when Bush left office, but he had no backup plan and it happened to coincide with the collapse of the global economy. He didn’t have much money, which means he didn’t have any money at all, and got a job working as a door knocker for the 2010 census. He went around knocking on doors as part of the census bureau. 

The census forms were Scantron sheets that had to be filled out using government-issued No 2 pencils, sharpened with government-issued single-blade pocket-sharpeners and on the first day of staff training they had them all sharpen pencils by hand. It had been a long time since David had sharpened a pencil, it was really satisfying and really nostalgic and he decided to figure out how to get paid sharpening pencils.

His only initial goal with the business was to not lose more money with all his little projects and to recoup his initial investment costs, which were more substantial than you might think because David has about $1000 worth of pencil sharpeners. After that, his goal was to get 100 people to pay him to sharpen a pencil, then 500 people and then the book ”How to sharpen pencils” came out. 

When he passed the 500 pencil threshold, the obvious next benchmark was 1000 pencils and when there was this great deluge of orders and he didn’t have time to put his [https://writingexplained.org/idiom-dictionary/finger-in-the-dike-mean thumb in the dike] he reached 1500 pencils and obviously the next benchmark was 10.000 pencils. At this point David was charging so much per pencil that it would be the height of folly to refuse a pencil order. He is charging $35. For those people who do think that this is an elaborate parody, his response is ”Send me your money and wait and see what comes in the mail!” Ophira is sold and is going to give him $35.

++ Pencil-related questions with David Rees and Dalton Ghetti

David participates in a round of Ask Me Another with John Chaneski, John Roderick and [http://www.daltonghetti.com/ Dalton M. Ghetti] whom David was extremely excited to see. Dalton is the artist who carves little sculptures into pencil leads. 

One of the questions is a music question played by John with the song Hot for Teacher by Van Halen. David is the winner by one point. They had asked each of their contestants to bring a pencil and the loser will have to sharpen the winner’s pencil. Dalton couldn’t bring his knife and he will have to do it at home and send it to David in a shatter-proof tube. David thanks the host for bringing Mr Ghetti, because for him as a pencil enthusiast and as someone who has had Ghetti’s photographs sent to him 1000 times where he had to say ”No, I’m not him, he is doing something different!”, it was such a thrill and pleasure to meet Dalton Ghetti and he has so much respect for him.

John is playing the song Hindsight by [[[The Long Winters]]].

++ Final round: Capital / not the Capital 

This round is played spelling-bee style, meaning by the first wrong answer the candidate is off the stage and the winner is the last person standing. The winner ends up being Leo Aray who wins their grand prize, a copy of David Rees’ book How To Sharpen Pencils. David will also send him his very own artisanally sharpened pencil with its own protective sleeve.