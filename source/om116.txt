[[toc]]

+ The origin of John's time on Earth (OM116)

John has seen Area 51 not by climbing up to a peak, but when they tried to dissect him when he originally arrived on Earth. Area 51 is his Ellis Island and he goes back with his family, like ”Here it is, guys! This is how I came to this great land!” John adopted the shape and form of an Air Force major, killed everyone in the room and then took the elevator to the surface. There he chose a third form, spread his black wings and flew to Seattle where he assumed his final form. This is the secret origin on John’s time on Earth. We don’t know where he came from or who he really is!

+ John’s dad’s private train car (OM116)

Having a private train car used to be very popular. In the 1970s John's dad was the chief legal officer of the Alaska Railroad, which at the time was federally owned. It is not connected to the rest of the system and it is run by the department of Transportation. They had a private train car from the 1940s with bedrooms, a big living room and a balcony on the back and they would connect it to a train if an executive, basically the president of the railroad or John’s dad, would want to go somewhere along the railroad.

This was a different time! John’s dad would have this car brought up and connected to the train and John, his sister and their dad would go on official business up to Fairbanks. It is a wonderful experience! This car was kitted out in top 1940s technology, which then seemed pretty amazing, with black & white TV and a phone that went ringadingading.

+ Rochester New York (OM116)

Rochester New York is named after Nathaniel Rochester, one of John’s  ancestors. The Rodericks married into the Rochesters, and the Rochesters are an old American family, but the Rodericks are quite a new one. The Rochesters were not rich and the money didn’t percolate down. They were doing okay, but John doesn’t think of them as having money because they never gave him any money.

+ The subway system in East Berlin (OM116)

Berlin's vibrant U-Bahn is one of the early subway systems and predated the Berlin Wall. In the early 1960s the East German government cracked down on migration through Berlin and built the Berlin wall after having lost 20% of the population because people realized it was better over there. They built a wall in the middle of the city and called it the anti-imperialist protection wall or something. The parts of the U-Bahn that were contained in either side of Berlin could continue to operate, but other train lines from one side to the other were walled up with bricks.

A few lines from the Western side to the Eastern side and back to the West remained open and on its way from one West German stop to the other the train went past subway stops in East German with East German guards standing there, giving you the hairy eyeball as you went through. The trains had to slow down as they went through East Germany because the tracks weren’t maintained to the same standard. There was a stop where you could get off in East Germany as a West German and Ken wrote a bit about this in one of his weird books for children. John has read a lot of Ken’s written work because it appears in dentist’s offices and stuff where John is often waiting.

Friedrichstrasse is right now the busiest station in Berlin, but during WWII it was where all the Kinder Transport kids were fleeing the Holocaust for England. They used it in one of the Bourne movies where Jason Bourne jumps down from a train station onto a barge. During the Cold War it was a big checkpoint right at the border and when they put up the wall they had to divide it in two. Many S-Bahn trains and all U-Bahn trains through Friedrichstrasse were only for West Berliners. The S-Bahn are the trains that went out to the suburbs and the U-Bahn is the subway in the city. You could cross an international border by changing floors, you went down a flight of stairs in East Germany and there was West Germany on the other side of the checkpoint directly below, probably the only place on Earth were a national border went on the Z-axis between up and down. 

When Berlin was unified, all these ghost stations, called Geisterbahnhof in German, had been preserved in their state in 1961 with advertisements on the walls and little direction signs and old phones in the offices. The German Transit Authorities wanted to reopen these stations, but they didn’t quite have the sentimentality for Eastern Europe that we do now and they didn’t keep any of the old ads or preserve any of the stations in their Geisterbahnhof state, although they could have put up a plexiglas over the old tiles. What if you walked through today and it was just all these 1960s-era ads for Knorr soups or whatever. People would love it! Ken always puts Knorr on his knackwurst.

+ John's trips to East Berlin (OM116)
 
In 1989 before the wall came down John had the eery experience of going to East Berlin by accident. Later he went there again on purpose, officially through checkpoints with passport, looking through little slots and walking through a maze, but his first time was on a train through West Berlin. He was dozing, smoking cigarettes and playing with his pocket knife, thinking about a girl and whittling when the train went across a river and John knew it was maybe not the best deal and a different socio-economic system might await him on the other side. The train went across a lot of different barb wires and John said that this couldn’t be good, so as the train pulled into the next station he went off and was very much in East Berlin.

Everybody was in a Polyester suit in a gray-green color and John stood out like a sore thumb. He had long hair and was playing with a pocket knife like a British train spotter, wearing some kind of puffy ski jacket, not an anorak, but a 1980s ski jacket with lightning bolts on it. They also had puffy jackets, but they were again in the color of coffee that had been out in the rain for a year and John had a lot more puff. 

Someone told him to go down these stairs, go over to the other side and get on the train going the opposite way. No-one molested him, but had to stand there for a long time because those trains don’t come that often. Somebody told him that he could have gone all the way to Poland if he just had the right amount of money to pay it. As the train came in, John went back on it and went back across. No-one ever checked his ticket or asked for any money, so he wondered if he could have kept going all the way to Poland and be married to a Russian woman living in St. Petersburg, having forgotten English entirely?

Ken’s wife lived in East Berlin in the 1990s right after the wall came down because her dad was with the foreign service as the commercial attache to Germany. She was probably there at Unification. The place they lived was in East Berlin and had formerly been a Stasi listening post, so every room had 8 phone jacks, which was amazingly convenient until you got a mental image about what was going on in your living room five years prior. 

John visited East Berlin six months before the wall came down in May or June of 1989. He had to change money and got an astonishing number of East German Deutschmarks that he had a struggle spending. Together with a couple of friends he went to the nicest restaurant in East Berlin and they had a lavish and disgusting meal, they were drunk and loud, but they weren’t wearing college sweatshirts that said ”Dartmouth”, so they weren’t the worst kind of Americans. It was crazy that you could go over there and spend a day, but you couldn’t spend the night.

A few months later John was in Florence and picked up the International Herald Tribune that said ”Crazy things going on in Berlin” It was November 9th and there was no reason for John to be in Florence because he had already exhausted all of the people who might have hosted him and they were well ready for John to be gone, so he jumped on a train and showed up in Berlin 1.5 days later. He had really long hair, he had a hammer, he pounded on the wall, sat on top of it and there were a lot of photographers taking his picture. Since then he looked for himself in every photograph, but he never found himself in any of those books and reports. One picture was all he asked, to have his experience be documented by one notable. 

Ken loves the idea of John being some kind of Forrest Gump character who popped up at every important juncture of history. John likes that, too, but unfortunately as far as the world is concerned he is invisible. He does pop up everywhere, but he is not even sure if he is real.

+ Outro (OM116)

Shucky Darn and Slop the Chickens.org (see RL237)