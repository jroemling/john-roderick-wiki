This bit refers to Josh Rosenfeld, owner of John's record label [https://en.wikipedia.org/wiki/Barsuk_Records Barsuk]. Whenever John says something that Merlin thinks is a good band name, Merlin will say: "Did Josh just sign them?" The origin of this phrase is unknown, but it probably originates in a personal conversation that predates the podcasts.

+ Sections where it is used

[[module backlinks]]