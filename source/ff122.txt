+ The Soldiers
//poem by William Childress, recited by John Roderick//

See [https://www.swarthmore.edu/library/peace/DG051-099/DG077_WIN_magzine%20issues_pdfs/WIN_Magazine_V6_N15_19700915.pdf source, page 8] (slightly different wording with two phrases omitted,  in square brackets below).
Part IV replaced with a version from a [https://books.google.se/books?id=FtNkxl5AGMAC&pg=PA163 different source]. See even [https://www.poetryfoundation.org/poetrymagazine/browse?volume=100&issue=6&page=27 here].

++ Part I: The Men

For what comfort numbers can afford,
we huddle together, mute, frowning,
appearing neither fearful nor bored
over the quick loss of our freedom.

[No longer will we live at random,
for] now our simple lives have meaning
but our lives do not matter. "Some things
are more important!", the [https://en.wikipedia.org/wiki/Non-commissioned_officer noncoms] tell
us. The world crawls like these trucks, destined
for a resolution of those things.

"[Draftee] soldiers, we don't mean to be unkind,"
one [says], then laughs. "But war is hell!"
Now it's just a game, but tomorrow
their sterner game begins. Intuit
what we may, we are not soldiers yet.
 
A killer's knowledge can't be borrowed.
To learn properly we must do it,
and learn what soldiers cannot forget:
[To learn properly, we must do it.]

++ Part II: The Place

The barracks are roofs and barren walls,
no more. All else is in addition
to their blankness. "Everybody's pals
in my outfit,", the Sergeant says.
"All equal! I don't want no pissing
and moaning, neither The Man nor me.

If you got problems, the chaplain's hour
is yours, not mine. I got no problems
but you, and you ain't no problems.
My job is to make killers of you,
and I will! Please do not misconstrue
what I have said. And take a shower!"

So we are assembled; numbered first
then named, society's unseasoned
green youths, bound for an object lesson
in an art that never was an art.

And who's to say, if worse comes to worst,
that numbers are bad? They set us apart
and give us an identity
we couldn't feel in a battery
of similar name. Unfamiliar
as our numbers are, less familiar
are the shaven faces and shaven heads
[neighboring] us in [https://en.wikipedia.org/wiki/Camphor camphorized] beds.

++ Part III: The Training

Late spring finds us inhaling the green
smell of leaves from behind spaded earth.
Each raw trench, fresh as a new grave,
opens in mock-warfare and gives birth
to yelling recruits, while a lean
tanned Medic looks on, impassive, suave.

But uncertainty matches resolve.
Our corporals won't let us forget
the impassive foreign faces
waiting with Oriental patience.
It is a problem many can't solve:
One does. He falls on his bayonet.

The burning days engender summer
and summer brings tanks. In earthen pits
reinforced with logs, we crouch and wait.
Fifty tons of armor eclipses
the worlds we occupy: We shiver,
inhale dust, and spit it out as mud.

Our training ends. Now our final end
focuses on a paper command
that means more than God to most of us.
Those who study war in high places
deploy us now, individualized rats
to individual races.

++ Part IV: The War

In Korea, decomposing shit
chokes the perfume of the stray flower
still seen occasionally on hills,
and the [https://en.wikipedia.org/wiki/Paddy_field paddies], heavily seeded
with napalm mines, can grow red flowers
at a touch, with a blossom that kills.

From the dark immobilization
of earth bunkers, our probing patrol
infiltrates forests. Distant searchlights
paint ridges with something like moonlight,
and a gray rain chills us. Winter's cold
is not far away. It too will come.

Our ghosts meet other ghosts in the trees:
They appear pallid and luminous
in the eyepiece of a sniper scope,
a tool too complex for the Chinese.
But their simple [https://en.wikipedia.org/wiki/PPSh-41 burpguns] never stop,
and their simple power murders us.

//"There is no end to this story!" on today's Friendly Fire as we watch Samuel Fuller's 1951 The Steel Helmet.//