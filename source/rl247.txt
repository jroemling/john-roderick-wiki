This week, Merlin and John talk about: 
* Thinking things through ([[[Personality]]])
* Famous instruments and equipment ([[[Music]]])
* Continued discussion about [[[The Beatles]]]

**The problem:** "John has a barn door", referring to the literal door at John's barn that he often leaves wide open. 

The show title refers to the chair squeek at the end of the song "The Day in the life" by The Beatles and they were wondering who's butt it was.

Merlin had a big morning with lots of big announcements from [https://en.wikipedia.org/wiki/Apple_Worldwide_Developers_Conference WWDC] 2017. He did not live blog it, because doing that in public seems unseemly, as we are already watching it. It is too much to cover. iPads, computers, you can drag things. 

John woke up at a natural time, when his body naturally rose to awakeness, he played a little game of [https://en.wikipedia.org/wiki/Threes Threes] or two, think about the day, look at [https://en.wikipedia.org/ Wikipedia] for a while.