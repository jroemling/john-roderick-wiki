This week, Merlin and John talk about.
[[toc]]

**The Problem:** John was interrogating the apartment, referring to walking around in the apartment with his dad’s pistol, shooting at imaginary Japanese soldiers when he was 10 years old and alone at home.

The show title refers to John’s landlord in 1995 who when John had a rat in his apartment told him that he didn’t make the rat, but God made the rat.

They open the show whispering.

[[include inc:raw]]

+ The Beatles documentary Get Back (RL446)

Paul (McCartney) pulls off his hair pretty well in Get Back //(the documentary about The Beatles)//, he should just probably always have the beard and he has a greasy hair carriage to him. Merlin is going to talk about it on a podcast for 3 hours tomorrow, and he suggests that talking about it could be their first Patreon episode if they would ever do a Patreon, which they probably will, although Merlin is not making any promises. Or it could be their first 7 episodes. They make an effort not to talk about it and [[[Leave it]]], but they talk about it a little bit anyways.

The original Roderick on the Line was about talking about The Beatles and Hitler, but John doesn’t even remember because it was so long ago. John came with [[[Supertrain]]], Merlin came with [[[Keep moving and get out of the way]]]. John mentions Merlin’s website [[[https://www.5ives.com 5ives]]].

+ John’s daughter’s mother’s small designer microwave (RL446)

John had a Rice Krispies candy for breakfast that he microwaved to bring it back to life. Merlin recommends to start at 15 seconds. John’s microwave is only 800 Watts, like a joke college microwave. John’s daughter’s mother really likes the color teal and when she was decorating her house somebody bought her a Smeg coffee maker that is that color and then she found a toaster and a microwave to match it, which cost $100 and is not big enough for a standard microwave popcorn bag that will get wedged and not turn correctly.

John suggested to put the microwave on a shelf and get a real tool instead because it is the middle of a pandemic and they are using the microwave a lot. He showed her some that he would choose, but she stood her ground and said that she liked her cute microwave. John was just trying to help, he can have whatever microwave he wants at his own house. She grew up as a hippie, they didn’t have a microwave which means the microwave is a concession that she is making to modernity. It is only good for heating up a cop of coffee and barely that. If you cook several things in a row it will shut down.

John would disappear it, except it is the emerald in her crown. Merlin was raised in a time when people looked askance at a microwave and there were so many cautionary tales. Don’t put metal in it, or your house will catch on fire, which is true. The first one Merlin’s mom bought was during the flush a certain amount of late father insurance money in about 1976, it was a Litton brand. John’s mom got her first one in 1980. It was at a time when they wanted to sell you on a lot of extra functions, like a browning button, while that does not exist anymore and you are more likely to find a popcorn button.

+ John having his mom’s old microwave that one day turned itself on, God made the rat (RL446)

John’s mom’s microwave was a very large appliance, she had bought it because it was All Mod Cons, she had moved to Alaska into a new house, and she was going to get a microwave. They had never had Nachos or heard of Nachos, but there was a black & white food article in Sunset Magazine that recommended to take your tortilla chips, which were also fairly new on the scene, cover them with cream cheese, and then microwave it. It was a revolution! Merlin didn’t hear about them until 1983 when Taco Bell in Tampa got them.

John kept that microwave, they moved it with the house, it was massive and you could put a whole turkey in it.

Merlin’s father’s mother passed away in 1987 and they had to clean up the house, which was tough for him because it was right up against the start of the school year and he was starting a new job as an RA (Resident Assistants), it was his second year of college, and he almost didn’t pull it off. Merlin got his grandfather’s chair, the dinette-table that he had sat at for so many years, it was a weird creepy feeling, and he also got her microwave. It was so old it still had latches, it was as huge as a TV, but the walls were 8” thick. Merlin put it in the bathroom of his dorm.

John kept their family microwave all the way until 1995 //(see RL218)//. He had it on top of his refrigerator in a rental place. When his mom left Alaska in 1995 she asked if anybody wanted anything. It was the apartment where one time a big rat came out from under the refrigerator and that was the first time John has ever lived in an apartment that had rats. His landlord was from India, a very nice guy whom John had a good rapport with, but when he told him that there was a rat in his apartment, he said in a voice that Harry Can De Bolo (?) and modern sensibility has made it impossible for us to do: ”John, I did not make the rat, God make the rat. If you have a problem with rats, you have to take it up with God!”, so John moved out.

Not very long after that he came home, keyed into his apartment, and knew right away that something was wrong. It was extremely humid, all the windows were fogged up, and he was hoping that a beautiful woman spy had come in and was taking a shower. At this stage in his live, something weird happening was almost certainly better than what was actually happening, except in this case the microwave was sitting on top of the refrigerator and it was on with nothing in it. Merlin learned very early on that there are several rules of microwave and one of them is that you never run it with nothing in it.

This had just spontaneously begun and when he tried to turn it on it wouldn’t turn off and the door was locked. It just went on and he couldn’t get it to turn off. It was very freaky! He had to unplug it. It could have been on for 8 hours and he wasn’t sure if the room was full of radioactive air and he could be big blue naked guy now: ”December 5th 1995, a muscular Cherub appears before a rat!” John was already scolded by the ”God made the rat” comment and he was mad at his landlord because he was running a slum. It was a a cheap apartment close to town in a triangular-shaped flat-iron building.

As John was moving out he took all the lightbulbs, but he left the microwave. All those things back then were mechanical, so it wouldn’t help to just turn it off and turn it back on again, and he never plugged that thing in again. It was probably never meant to last 15 years because we are living in a planned obsolescence world and have been since the 1970s. John has a radio on the shelf that was made in the 1950s and has civil defense labelling on it so that in the event of the bomb you can set your radio to the right frequency. It is illuminated and it is teal, it takes a minute to get going, and it still works, but the only thing you can hear on it is old Jazz and old Swing Music.

+ Merlin getting a new microwave (RL446)

Merlin recently replaced the microwave that he really loved, but it had gotten long in the tooth and gross. In the 1990s he learned that every VCR cost $200, the feature-set went up, but they remained around $200, while in the past your Betamax in the 1970s cost $1500. When he was in military school they used to pool their resources to be able to buy Betamax tapes and in 1979 he bought a copy of M*A*S*H that was $100. There was a thing like a bench fee //(see RL293)// when you wanted to get something fixed on it, like replace a fuse. For them to even put it on the bench would cost $50, and a new one was $200.

Today you can clean the shit out of your disgusting microwave all you want, but at a point the patina has become so mature that you need to get a different microwave if you want to keep your wife. They got the new version of the exact same model, and it is a piece of shit! It is the same Panasonic that he always gets, it has a digital read out and every time it is done cooking it says: ”Enjoy your meal!”, which he hates so fucking much //(see RL153)//.

You need to know your wattage and you need to have wattage that reflects the needs of your household. It is going to be a whole chapter in Merlin’s wisdom document, how to do microwave. As a boy coming up there are only two setting on the oven, which is 350 (175 °C) and broil, but as you get older you realize it can do other things. Merlin works the percentages and he uses Sensor Reheat that is measuring moisture and Sensor Reheat + 30 seconds on High works for almost anything. Merlin likes piping-hot food. Pizza you have to do on the range-top in a pan, never do that in a microwave.

+ John’s daughter not being as self-sufficient in the house at 10 years old than John was at her age (RL446)

Merlin suggests that John’s daughter should disappear the microwave, but she doesn’t know the difference because she is in the waning days of her life where except for opening cans //(see [[[beandad]]])//, everything is taken care of for her. She has just recently started to make her own macaroni and cheese.

By the time John was 10.5 there was no-one home when he came home from school. He had a key around his neck on a piece of red yarn that got him into the house, he was making cakes and he was cooking pork chops. John’s dad could come home at 5pm or at 15 and he always had a pork chop for dinner.

At the time John was taking his dad's 1911 model World War II pistol from above the refrigerator and walked around the house shooting at imaginary Japanese soldiers, interrogating the apartment. He was also lighting his dad’s girlfriend’s Aqua Net //(hairspray)// on fire and burned it into the fireplace. Merlin would take his mom’s Cricket lighter and some Right Guard //(deodorant)// and you could make a pretty good suburban flamethrower out of that.

+ John's dad hiring Myrtle as a housekeeper (RL446)

In 1980 after John’s sister moved in their dad hired a housekeeper who was supposed to make them food, an old Alaska Native woman called Myrtle who had been living in Anchorage for a long time but still spoke with a thick village accent. At the time John had been coming home from school by himself for 2 years and he just wanted to read and be left alone. How was he going to play the same note on the piano for 45 minutes if there was somebody in the room?

Myrtle thought that her mandate was to welcome them home with some fresh-baked cookies, except she didn’t know how to make cookies. In John’s eyes she was just there to look after his sister because he was 12 by this point and there were kids at his school who were smoking weed and having sex at 12. 

Merlin would be flushing stuff down the toilet and he would do elaborate proto-masturbation experiments when he got the place to himself! You could look at catalogs and think about what you couldn’t afford.

Myrtle famously called parmesan cheese Parmesian Cheese and in their family they still call it that. At 12 years old he was in a correcting phase, going: ”Well, actually…”, but he didn’t want to correct Parmesian Cheese because he had enough social grace to realize that you don’t correct adults, and he even taught his daughter that it is called Parmesian Cheese. It is Myrtle’s legacy, although she has probably been dead for 40 years.

When John got out of school he didn’t need to be minded. There was a pawn shop and an Army Navy Surplus store on the way home from school and he went to both of them every day, asking the guy how much that pistol was, back when pawn shops had sheriff’s badges for sale. He knew all the pawn dudes and all the Vietnam vets that were sitting with their cigars clenched in their teeth, and he would ask about this gas mask and where it was from. The guy would explain that this was a jet pilot outfit from the 1950s, and: ”Don’t touch that, kid! That is ivory that is no longer legal!”

From the time John’s dad was 5 years old nobody knew where he was. You would get out of school and you would just roam until the sun goes down. When John’s sister moved in it wasn’t that she needed any more minding than John did, but their dad wanted to do it correctly. He was a man of a certain station and a housekeeper felt like the right thing, like Mrs. Livingston from The Courtship of Eddie’s Father, or Mr. French on Family Affair, or Alice Nelson on Brady Bunch, it was a thing back then.

Having a housekeeper was his last gasp because that also allowed him to go to his mistress’ house after work and not have to race home to parent. John was already fully independent and Myrtle didn’t know enough to be authoritative. She had no education, she was not informed about the matters of the day, and she barely knew how the oven worked. John was 12 years old and she was going to tell him to pick up his shoes? He would do it to be polite, not because she had any gravity, and Susan never listened to anybody.

Myrtle lasted for about 1.5 years and it was probably not very fun for her either. She was trying, she would make little cakes, and there was always been a bit of guilt around her because John wished he had been a better child under her auspices. If she had been there in the mid-1970s when he spent the summers up there, maybe she would have been a beloved matronly figure, but it was too late by then because John was already fighting Commando battles in Central America, he was already in Rhodesia in his imagination. He could also field-strip his gun by then, and what was she going to do?

+ John’s mom and dad in the 1960s (RL446)

By the time John’s dad hired Myrtle he was still trying to figure it out. He wanted to do things properly. For example in the 1960s he didn’t want John’s mom to work because he was a successful lawyer and it didn’t look good, like he wasn’t providing enough. John’s mom not only wanted to work, but needed to work, and could not do the Junior Glee Club style of pillbox hat and white gloves work where you were doing housewife work in the 1960s.

What made it such a dumb thing is that they were intellectual peers. They would go for long drives and talk about the issues of the day, it was not that he kept her in the kitchen. They were together great friends, but he was a man of certain station, he was a member of the tennis club, for the love of God, and he was 41 by the time they married and she was 26. He had already held public office and he had been previously married, he was a veteran, he was established in his way.

He never condescended to her, that would not have been received well, but she taught herself Fortran and machine language out of a book. John has been recording some episodes of a secret podcast with her and he doesn’t want to give too much away, but in the 1960s/70s programmers sat and stared out the window, sometimes for 4 days, and by the time they wrote the code they already had figured it all the way out. In big companies the lower level management all understood that you would walk through the IT department and you wouldn’t hear anything, there would just be people sitting and staring.

At the time it was a lot of women, they worked in teams of 5 where each person would be good at a different thing. The idea that a computer programmer was an engineer was something that only happened many years later when universities started teaching computers and didn’t know what department to put this in and the engineering department stepped forward. Before that it was thought of as a liberal art. It was a logic problem, it had nothing to do with engineering. 

John’s dad didn’t know what to tell the people at the club that his wife was not only working, but in some weird computer thing. At the same time as a Liberal Democrat he of course supported the war in Vietnam. ”It is the United States and you don’t turn your back on the Army! What are you? Some kind of Communist?” It took him until 1970s before he understood that this was a bad war. It was the first bad war that any of them had ever heard of. Like Bill Murray says in Stripes: ”We are 10 in 1”

+ John’s family having a Japanese couple living in their house (RL446)

When John was a little kid in 1969 they had a Japanese couple living in the house with them. The woman did all of the house stuff, and the husband was the handyman, he managed the house. At the time a lot of Japanese people had been living in America for generations, and before the war all of his friends were Japanese and he played basketball with them. He shot down the Zero with his .45 just because i was part of the act of war //(see RL34)//. 

John’s dad grew up in Seattle which was very multicultural and all of his friends were Asian or Jewish, while John’s mom grew up in the Midwest and she had been indoctrinated during the war as a child to be very suspicious of people from Asia. She had been getting the Midwest version of: ”If you see an Asian person, they are probably planting a bomb!” Nobody in Merlin’s family would buy a German or Japanese car, except his uncle. John’s mom was a Chrysler person and only drove her first Japanese car in the 2020s, she just bought her first Japanese car. There was a moment in the 1980s where she had a Chevy-branded car that was actually made in Japan.

Until John was 4-5 they spoke Japanese to him and he spoke Japanese. Then in 1972 they were deported at a time when you would deport people who were Japanese. They didn’t have their Visa right, they were working under the table for John’s family, and the idea was that they were going to keep living with them and John would grow up speaking Japanese. That would have been pretty great.

+ If John and Merlin had known each other in 7th grade (RL446)

What if John and Merlin had known each other when they were 11 and 12 years old? At the time Merlin was spending a lot of time making up games that one child could play with himself. He was in Military School in 7th grade, after that they moved to Florida, and he begged not to have to go back to Military School. Instead he was going to public school in Pasco Fucking County, Florida and it was a trash fire. He rarely felt as lonely in life as he did in 8th grade, but he also found himself finding some comedy with other weirdos, kids that would turn out to be gay or would become engineers.

He graduated in 1985, John in 1986, they were only one class apart because Merlin was always old and John was always young for his grade. Merlin could see them finding some common cause and forming an interesting alliance. It would have been around Dungeons & Dragons and fascination with weird stuff. 

John was still wearing velours shirts a long time after nobody else was wearing them anymore. He had orange pants and his orange flight suit //(see RL13)//, and he was wearing rust-colored pants when everybody else was wearing white Levi’s. Rust was also the name of John’s supervillain //(The Oxidizer, see RL34)//. It was not until his Junior year that he figured out he could not keep not washing his hair if he wanted to be a success with other kids.

John has seen pictures of Merlin in college, but never as a teen. In 8th grade Merlin was using Vidal Sassoon. John has posted some pictures of himself as a later teen when he was 16, but he hasn’t posted any pictures of himself at 12 or 13 because he was still very nascent, he was not even a person back then. It was pre-scallop. You already knew what was going to happen to other kids like Deminidor Gor-Belleza or Laurie Basler //(see RL305)//, but you couldn’t tell what would happen to John because he was a mushroom who had just poked up above the ground bark.

John was certainly capable of destruction, and at varying times he was a little bit cute, but he was not ready. Merlin at the age of his daughter was not adorable and also: Kids in Junior High are the worst! There is a reason they need to go [[[cutting trail |cut trail]]]. They need to get them out of the public eye for the period where they must necessarily be excruciating as human beings and grid them down a little bit and improve their national parks.

Merlin sent John a picture of him at that age and John had the same haircut and the same shirt and he was probably standing at the same podium giving the same speech. John thinks they would have been good friends. There was a picture of John in the newspaper in 7th grade at a canned food drive in that same JCPenney shirt with a can of beans in each hand, which is where it started //(see [[[beandad]]])//. John’s family shopped at Sears, they were a little bit higher class, and they have called him husky //(see RL4)//. John has a picture of Merlin on the bookcase.

In the 1990s John dated a girl who was very beautiful and very vivacious and she used to say: ”We would have been friends in Junior High!” - ”I assure you we would not have been!” Then she showed him a picture of herself in 8th grade and she had jet-black hair down to her waist, freckles, and John would have been so mean to you, he would have been awful, and she would have been thought that John was a child and beneath her dignity. John was not close friends with anybody except Kevin Horning until he was at least 16.

Yesterday was not a good day for John. He got up off the couch at 8pm to go tuck his daughter into bed at her mother’s house and then he drove home and that was the extend of his accomplishments.

By the age of 16 he finally was no longer a toadstool, but only because he went to Sears and bought a new pair of Levi’s so he didn’t have to wear the pair that he couldn’t button the top button anymore because he had outgrown them. He looked like a teen instead of a kid, but that is not to say that he was not super-cruel to other people.

John had a couple of male teachers in their 30s who misunderstood him and thought that he was doing it right and that he was cool. One of them who was 32/33 years old said to John in his Senior year when they were sitting in the video editing suite at the career center: ”You probably get laid all the time, don’t you?” and John was a virgin and he thought that sex was weird and wrong because he was too young, and he also thought that his male friends shouldn’t go to second base with those girls. One teacher was really a good friend and he said: ”Roderick, you are such an asshole!”, but he totally meant it as a compliment because John was doing what he wished he had been doing in High School, which is be a big cheese.

+ John losing the campaign for sophomore class president (RL446)

During his Freshman year in college John was running for Sophomore class president. They had already expelled him two times and he felt that if he would be Sophomore class president they couldn’t expel him because he was going to be integral to the operation. He lost the vote, which was quadruple-humiliating because they announced the results in the lunch room and everybody in the place was looking at him.

There was a kid that John had gone to High School with who was one class above him, but because John had missed a year between High School and college they were both Freshmen. John didn’t know him from High School because he was an irrelevant person and if he saw him today he wouldn’t recognize him, and when it was announced that John lost and the winner was just some wank, some Transcrit Mongerer (?), the guy made the arm-pump gesture of ”Yes!” because John lost, not because the other person won //(see RL124)//.

As John had gone around the dorm rooms, campaigning for class president, he had knocked on that guy’s room and he opened the door and there was some other kid in there and John went: ”Hey man, what’s up! East High, am I right? I am running for class president!” and he was friendly to John, but looking back at that moment there was a little bit of ”How dare you?” in his face. It was only then that John realized that being an asshole was not universally admired.

John thought about that moment when the guy did that thing with the arm because at the time John reflected back. He hadn’t seen him in a couple of years and he hadn’t done anything to him in college, but at some point in High School he was standing there with a group of people or walking down the hall or something, and he either saw John do something or John must have turned to his group of friends and said: ”What’s up, losers?” or something to the effect: ”Did your 20-sided die roll into a heater register? Why are you standing around here? What are you looking at?”, some big swinging dick attitude at him, and John didn’t even distinguish him as a person from his group, but it must have stuck with him forever.

After that John started to be nicer. No grown-up ever told him to turn it down, and John never wanted to be mean, he thought that everybody was out to get him, the girls all teased him, the bigger boys all pushed him down, and it was only his sense of humor that allowed him to feel like he could stand up straight.

+ John no longer being funny because what made him funny was that he was mean (RL446)

About a year later when John was about 20-21 years old another guy he knew from High School, they would go out to parties in Anchorage, said: ”You used to be a lot funnier! I don’t know what happened to you!” - ”I used to be mean!” - ”Yeah, and that was funny!” John was bummed!

When Paul F. Tompkins was 18 he already knew that all he wanted to be was a stand-up comic and he has been a stand-up comic his whole adult life from the time he was in High School. When John was 18 that was on the shortlist of things that he thought he might do for a living: A journalist, a DJ, or a stand-up comic were the 3 things because he knew he wasn’t going to be a lawyer. John realized in that moment that stand-up comic might fall off that list because what made him funny was that he was mean, and he had lost it.

John has still been an asshole his whole life, but the guy was saying that John was all bruty and his jokes take a minute to understand, which was not funny. They were party-hopping and John used to hop in the door, saying: ”What’s up, nerds?”, which John still says today, and the guy wanted John to be the ice breaker who walked into any party and was taking over. John would press the button on that keyboard that would make the Chinese melody that became his signature //(find reference)//. He was the guy who was going to abandon his group costume //(when he went to a Halloween Party with Kevin Horning, see RL437)//.

John would walk into a party of a different High School, walk right over to the stereo and change the music and say: ”This party sucks! What you need is some Rock’n’Roll!” When John was going to leave this party it was either going to be 20 times better or he was going to leave it because 4 hockey players were after him. All of a sudden he had turned into someone who was like: ”What is a party?” - ”What? What happened to you?”

+ Meeting a whole new group of people through Merlin, Hodgman, and Coulton (RL446)

The problem of John in his 40s was that he was still walking into rooms, going: ”Hey, what’s up, nerds?”, but feeling like they were all in on the joke, he was playing an 1980s character as a gag, but there were a lot of people who weren’t in on the joke and didn’t think it was funny. He was a wingman for most of that time. At the end of his 30s he had accomplished enough that he could stand his ground and say: ”Yeah, I am the singer of The Long Winters!” and very briefly from 2007-2009 that meant enough. Then in a way he is lucky that from 2009-2014 Merlin was the Internet’s Merlin Mann and John Hodgman was Apple’s John Hodgman and Jonathan Coulton was this guy who had covered Baby Got Back and it got downloaded a million times.

John was there as Long Winters guy, but he was in a whole new room full of people, he was in a room with Ted Danson and with Elon Musk and he felt again like he was in High School and everybody was way older than him and better and when he was introduced to people as the singer of The Long Winters, then Ted Danson was: ”Huh, who?” and if John had just walked in quietly: ”Hello, nice to meet you!” he would have very quickly felt like Hodgman’s valet at that point, the +1, the quiet girlfriend.

Through that decade of his life John was trying to establish that he belonged there and that he was there not just because he was somebody’s friend, and that was really hard. Merlin says that John is very fun to be in a room with, even when he is not the changing-the-music asshole.

John is in a dark place these days, he is going over everything, he is in his 50s, he has been forced to take a new tact, and he hasn’t picked the direction and he is doing the thing when he tells himself that he sucks and has been sucking for all this time. He knows it is not right, but something has going to happen! He probably needs to talk about The Beatles because he has no doubt that the things he is going to say about The Beatles are true and correct and they are going to help everyone understand The Beatles better. He needs to Get Back.