This week, Merlin and John talk about:
* Merlin not recording the first hour of the show ([[[Podcasting]]])
* Finding a powder-blue tube radio from 1955 ([[[Mid-century modern]]])
* John having trouble selling his house ([[[Mid-century modern]]])
* John’s baseball hat that says ”Entrepreneur” ([[[Style]]])
* Controlling your feeling of being angry ([[[Personality]]])
* What we are wrong about regarding Columbine ([[[Factoids]]]))
* John’s dad running for district attorney ([[[Parents]]])

**The Problem:** Whatever’s not in the show is not in the show, referring to Merlin having not recorded the first hour of the show.

The show title refers to a turtle being controlled by the ocean, but not caring or getting mad about it, because the turtle just goes.

> **Draft version**
> The segments below are drafts that will be incorporated into the rest of the Wiki as time permits.

+ Merlin not recording the first hour of the show (RL333)

The show starts with Merlin saying that ”Now it is recording!” His Skype Call Recorder was in the ”Do you want to record?” state, but greyed out and wouldn’t let him record. He didn’t notice until about an hour into the show that it wasn’t recording the entire fucking time! John was not recording on his end. 

Now their genius episode is fucking tears in the fucking rain and Merlin doesn’t know why it ate the bugger because he didn’t change anything. [[[What is in the show is in the show]]]! They had covered it all! Merlin claims that this was actually not his fault for once. John is not bothered by it because #aloha. There are going to be people out there like [[[Jochen]]] in Germany and [http://twitter.com/capnmariam Capn Mariam] who are going to say: ”Well, now wait a minute! What do you mean: You covered it all?” and they are going to wonder.

They don’t have any sponsors this week and Merlin is still so fucking mad he wants to spit. He offers John to skip this episode if he wants, but John is not frustrated because #aloha and he is constantly living on the very razor’s edge of what Merlin is talking about right now, which is ”Was that recording? Does all this count?” 

Merlin needs some Aloha because they had talked for almost an hour, but where they are now is where they are. They lost a lot of potentially classic stuff and they lost getting a Wikia out of this thing. Merlin is not sure if he is talking to Bran or to the raven right now, but how should he feel about this and should he even proceed, given John’s position with Aloha? What are you clinging to? The follow material is not anywhere:

* The John Siracusa material
* Marco Arment
* the Corvette
* plosives
* conflating their Weltanschauung
* onomatopoeia
* online burlesque
* being a non-player character in Fortnite
* The interpants on the undernet
* phantom power
* the revenge of the phantom dead
* entire conversation in Wookiee
 
John harkens back to the fact that the turtle practices non-attachment because the ocean does not let the turtle fixate on any one piece of sea weed. As soon as the turtle goes in to take a bite of sea weed the water comes in and moves it along. It is literally like living in a Heraclitian world, it is never the same ocean, and the turtle does not know. 

The turtle has never gone on a cruise-ship and knows that he is never going to do it again //(Merlin went on the JoCo cruise once and did not enjoy it that much, see [[[JoCo2015]])//, the turtle didn’t write a long essay about that, the turtle has never tried to eat sea weed that wasn’t on a rock under water. It is like the two bacteria making champagne drowning in their own sugar poop and never realizing that they made champagne. It is one of these situations! 

Merlin is making this about himself, which is bad, because he wants to be a good turtle. Are the turtles our role models? Sometimes you want to be an eagle! Sometimes you want to soar with the turkeys. Sometimes you would take flight over underwater breathing //(see RL331)// and that is one way Merlin and John differ. Merlin is approaching this in a left-brain way, but that defies Aloha because Aloha does not have a brain side. A big part of turtle life is to not only not be sad, but to not be cleft to the idea of what one is sad about.

Merlin’s brain will do a lot of fucking cooking to come up with what caused him to be how he is right now. If this show had recorded, they started with that Merlin felt the way he felt at the beginning, but the tears in the rain were never caught and what is it that he is mad about? It seems to go to the notion of Aloha and to a #turtlelife: Is the turtle anything about anything? If it is not in the show, then it can’t be in its show. 

What is the turtle’s show and who is listening to it? Does the turtle know what Wikia is and feel like it is overdue that he or she have one? Is there anywhere to know what is not in the show if it was never in the show to begin with? If a tree falls in a podcast, is he a great man? Are they going to say that he was a kind man or a wise man? That he had plans or wisdom? It becomes a acid-trenched rhetorical question, a lysergic monolog. 

If nobody recorded it, then Francis Ford Coppola is going to be mad and he is going to be firing people. saying: ”Where even did that go? I have been putting up with this guy for months, how did we not get the fraction talk?” What are you going to do? Land on a fraction? You go from Venus or something?

Merlin is already feeling better, although not by a lot. He should probably make sure that this show is recording. It looks like it is, but maybe he shouldn’t worry about it because even though they never collected the tears that were co-mixed with the rain, did anybody get the rain at all? Maybe it doesn’t matter? Maybe it is the friends they made along the podcast? Were John’s dreams being recorded this past year? No! When he looked back at the recording of the last year, he found no waveform and it was all greyed out. It was 0 Kbytes and nothing was captured to disc.

Nobody is going to talk about the plosives, Marco Arment is not going to make any recommendations that Dan Benjamin refutes or denies, John Siracusa is not going to be mad about things Merlin got wrong, they are never going to have an entire conversation in Wookiee. It is not going to bother Merlin, because he learned this week that whatever is not in the show is not in the show. 

What is in this show is the blue tube radio that will feature nicely in John’s notional mid-century modern house with the blond wood. It used to matter so much, but now it is sitting in a bag. John is not saying that the radio has blue tubes, but the radio was blue and it has tubes. Ultimately it comes back to semiotics and structuralism, it is dialectical business.

+ Finding a powder-blue tube radio from 1955 (RL333)

The other day John went to the thrift store and found a powder-blue tube radio from 1955. He turned it on and moved the knob and actual terrestrial radio came in through a tube. He had to buy this thing that was $5 and he bought it because it is a powder-blue space-age radio that was going to go into John’s house that is all made out of glass and whatever else. 

+ John having trouble selling his house (RL333)

When John put his house for sale, he was confronting the same feelings as Merlin had after not having recorded the first hour of their show. It felt like the culmination of a year’s worth of ”putting my house for sale”, they had been talking about it for a long time, all the way [[[House |back to Sahm]]] rebuilding John’s porch. They had put in the new thing, his mom had filled all the nail holes with toothpaste, there was a lot of this and that, and there was talk about moving to [[[mid-century modern |a new mid-century house]]] where John won’t have any swords. It was all going in John’s head and Merlin knows how John is about thinking! He sits and thinks! 

Everybody high-fived him all around the world, there was a website writing that Marc Maron [https://www.latimes.com/business/realestate/hot-property/la-fi-hotprop-marc-maron-20180517-story.html was selling his house] and so was John Roderick, lol! So many people were supposed to be rolling up on his house, just throwing bags of money at him, he was going to have a house full of space-age radios, but nobody made an offer. 

Last Thursday John woke up, stretched like a busy bear, expecting to have a busy day looking at ten offers and pick the one that appealed to him most. If the high one was some kind of developer he was going to say ”No!” and he was going to pick the one who wrote him a nice letter and who [[[keeping a small bag packed |kept a small bag packed]]]. 

About 1pm he would have expected a phone call telling him that they had to stop taking offers because it was overloading their system, but when he talked to the real estate agent about 5pm she told him that they had not gotten any offers, not a single one, despite the market John was in and despite the low price point for this house.

Merlin was shocked when he saw that. They had about 120 people come through the various Open Houses, John was a [https://www.redfinX.com/WA/Seattle/10824-53rd-Ave-S-98178/home/178979 hot home on Redfin], they put him on the news in Seattle, but not a single offer! Did the excitement freak everybody out? She had no idea, nobody had any idea! Another week went by and all of that happened all over again and at the end of the week on Thursday John woke up and didn’t stretch like a bear because he didn’t want to risk it, but they still had not a single offer. Now they were into week three, still no offers, and nobody understands why.

In the same way that they just did a classic episode of their show that was not recording, John had spent a year getting ready to sell his house and buy a new house and move into that house full of tube-powered radios and clocks that look like a sunshine, couches that are too low to comfortably sit on, but that look great from across the room, blond wood, he was going to have one of those Eames chairs that is impossible to get comfortable in, but they are very pretty, and all of a sudden not only did his house not get any offers, but his dream did not have any offers on it, a year’s worth of dream that involved 1000 moving pieces. John had been working on it, he had fixed up his freaking house, but the universe didn’t even get the call!

John can’t be mad at the real estate agent because she did a great job! He can’t be mad at himself because he did a great job! He can’t be mad at the world because how can you be mad at the world? John spent enough time doing that! It is nobody’s fault, not even John’s, but it doesn’t resolve. 

Every morning when he wakes up he wonders what he should dream about now. He got this radio, but it doesn’t go with the old house. Should he just move back into the old house? It is a nice place and he likes living there, especially now when all his shit is in storage. The photos were stunning and what they did with that joint was crazy! They even decorated it with John’s own stuff, so they could just leave, pull up their sign and take their business cards with them, but that was not John’s dream going into this! 

John doesn’t need to sell his house, it is not that he has a job offer somewhere or he is in financially bad straights, but it was just a dream to move up and go to the East Side with a treehouse apartment in the sky. John went through all this process until he was ready and then his expectation, which was not wrong, got re-aligned a little bit. He can't do anything except move back into his house and just say: ”Aloha!” or sell it for less than he thought, which he also just would have to accept. 

Merlin’s mom was a real estate agent and he knows that you can’t just leave it on the market for three years because it will feel like a distressed property. After a certain amount of time you have to take a step back and figure out what you did wrong, or what you did right. John’s sister said that the universe didn’t want John to sell his house, but stop right there! 

The universe does not telegraph its intentions. The ocean does not care. It is not antagonistic towards John, but it just doesn’t care whether he lives or dies. The Buddha has no quarrel with the ant, which Merlin would know otherwise because it would have its own Wiki and this would be 3 pages right there. Somebody made John a... but they didn’t make him a hat. 

+ John’s baseball hat that says ”Entrepreneur” (RL333)

John has a hat that says ”Entrepreneur”. It was an expensive hat that was hand-made by somebody in Hawaii and it was purchased for John by his Millennial girlfriend because she thought it would be hilarious that John was wearing a baseball hat that said ”Entrepreneur” and the only other people that had them were some San Jose entrepreneur types.

It was a thing that some dickheads had, she dressing him up like a kitty cat, and John got this hat that he likes. It has that weird flat brim that makes you look like you are on a baseball team, but you are also a little hip hop, it is not the kind of thing that John would normally wear, but she was correct that it was just hilarious enough. She put a picture of it on the Internet and someone else who John didn’t know photoshopped it and instead of "Entrepreneur" it said ”précisément”

You know how many Wikis that would have? What you didn’t hear in the fucking episode that didn’t record is that you can find out every time that Griffin McElroy says something in the Japanese style, but you can’t find out how many times John Roderick said ”précisément”.

+ Controlling your feeling of being angry (RL333)

There is a certain state of mind when you notice that you are angry. You can try to redirect what it is you are angry about, whom you are angry at, or what situation is causing the anger, and when you really get down to it you sometimes realize that you are angry at nothing, which is a good sign that you are probably mad at yourself. Sometimes the universe can serve as an anger-sink when we are mad at whatever. If we have anger in search of a source, the poor universe can be the one who receives it.

In John's turtle wisdom it doesn’t make sense to be mad at the universe or at nothing, let alone anything, because it is not going to sell his fucking house. It doesn’t make any sense to be sad either. You can’t be mad, but you also can’t be sad for the same reason. The turtle is not sad about being moved along, but the turtle just goes on to the next piece of sea weed, or not. The turtle just goes! 

John has been very sad and was laying on a bed for an entire day, but not laying on his back, staring at the ceiling, but he was laying on his front and his head was off the end of the bed, staring at the floor. The light was off, but the door was cracked and there was enough light coming in so that he could see the floor to stare at it. John sounds like a Peanuts character!

"If you can’t be mad, you can’t be sad, bro!" In Merlin’s unified field theory anger is the terminus for most feelings and emotions. Once something reaches its illogical conclusion, when you are out of other options, you end up with anger. What will inevitably turn into anger is sad and Merlin is very interested in how one gets less sad. Kabula Mantana (?) How do you not be sad that Interpants on the Undernet didn’t make it into the show? At the same time: How can you be sad? John's uncle Jack is right now trying to figure out what he is sad about and it doesn’t matter.

John’s emotions didn’t go to anger, but until he was 31 years old they used to go to catatonia. If something affected him in any negative way at all and created any feeling like anxiety, anger, or depression, all he did was shut off completely and be catatonic. He stared at the floor until the people around him got bored trying to get him to respond and went away. Then he would return because there was nobody there asking him anything, looking at him, making him mad, anxious or sad and those people were no longer there with their fucking people energy. John had no other way of expressing that emotion. 

After John had discovered that anger was a component it was terrible whenever he started being angry because now he was fucking angry and furious at everything! He was also scary when he was angry and it was a bad thing for other people because he was no longer sitting on the bed staring at the floor, but he was screaming and in some cases he would hold it in which would make him emotional in a way that can be scary.

You don’t want to stop being angry, but in some cases you have to ask yourself what you are angry about. Was he yelling at somebody in traffic because he was sad about not selling his house? If he could get to a place where his feeling about not selling his house were Aloha, then maybe he won’t be so grumpy when somebody at the cooperative grocery store didn't know how to go down an aisle conscious of the fact that there are other people in the space. They don’t [[[keep moving and get out of the way |keep moving, they don’t get out of the way]]], they did neither thing in concert! It is "Keep moving //and// get out of the way" for a reason.

If there was a Wiki it would be right there, but they have to let it go. If it were carved into a piece of burned toast or half of an avocado pit. What if somebody listened, translated it into German, paraphrased it and put it on their own wiki? But that is not in the show anymore and [[[what is in the show is in the show |what is not in the show is not in the show]]]!

+ What we are wrong about regarding Columbine (RL333)

The podcast [https://rottenindenmark.wordpress.com/podcast/ You’re Wrong About...] is currently Merlin's favorite turns-out podcast. They talk about a topic you thought you understood based on media coverage and police corruption and ineptitude, but it is not actually what happened. Merlin listened to [https://rottenindenmark.wordpress.com/2018/09/17/columbine/ the Columbine episode] (from 2018-09-17) and they were talking about how much stuff people get way wrong about Columbine. Most of what they got wrong was shown to be wrong within a month or less or latest in six months. An example is the role of bullying, another is that the girl didn’t actually say she believed in God, but she became a hero to the God people and her parents wrote a book that if Dylan and Eric had found Jesus this never would have happened and she became a martyr.

You always get mad at a bully! When some kid harasses your kid you think that this kid should go to kid jail. We forget that bullies and kid bullies in particular do not come from super-happy, well-adjusted, communicative families. There is no bully Sui Generis, but a bully comes from somewhere. These kids did get bullied in an 1800-kids-school, but they bullied other kids, too, as you do. Everybody is a dick! Almost everybody, including Merlin, felt bullied by people who had higher status and were tougher than them and then found a way to further humiliate people with lower status. Merlin did that and owns that. He did not bully anybody physically, but he said terrible things to people.

+ John’s dad running for district attorney (RL333)

//John also told this story in RW129.//

When John’s dad ran for district attorney for the state of Washington, which is the public prosecutor, the Liberal Democrat governor asked him: ”If a guy shoots up a church and it turns out that he had a bad childhood, that his father beat him, would you take that into consideration in the process of prosecuting him on behalf of the state?” - ”Well, of course!” - ”Well David, that is not what we want in a public prosecutor. The defense attorney is going to try to convince the jury that the bad childhood of the guy is salient to the question of whether or not he shot up the church, but the state does not have an opinion, the state is not taking all the mitigating factors into consideration, but the state has an obligation to prosecute the criminal person on behalf of the victims and on behalf of the state. His job is to prosecute, not to consider mitigating or ameliorating evidence, that is the jury’s job after the evidence is presented to them. The question of the guilt or innocence or mitigated innocence of someone who does a crime is not the prosecutor’s business!” - ”But we are Democrats! We are trying to make the world a better place!” - ”Even Democrats, trying to make the world a better place, have an obligation to perform their jobs in the world.”

Although bullies in a school almost certainly have reasons, that is also not really always the responsibility of the victim to consider. The victim is not obligated to think about the bully’s pain. The state has an obligation to pursue justice on behalf of the person and on behalf of the state itself. The state is not benevolent or kind, but it has interests like order and collection of taxes. It also has to prosecute crimes, you cannot have a state that says ”Well, shoplifting is okay if you are homeless” It is not the prosecutor’s job to decide whether the law is just, but to see that the law is enforced. It is the legislator’s job to make the law, it is the judiciary’s job to decide if the law meets the qualifications of a law and if it is just within the context of the constitution of the US and of the state. Everybody got a job!

A lot of people on the Internet feel like the court is unfair or the cops or the judges or the institutions are unfair, but all have governing principles and they all have a job. They are doing the job we need them to do and unless you are truly an anarchist you can’t expect there to be no police. You wouldn’t want it nearly as much as you think you do. You do not want to live in a world where shoplifters aren’t prosecuted, even if they are homeless. Maybe for a couple of days you will feel good about yourself.