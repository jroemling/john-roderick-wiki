This is a fictive type of sport that John came up with to solve the world's problems. His argument is that humans do have an urge to fight things out, but in today's society there is no outlet for those desires. The concept was first introduced in [[[RL61 |Episode 61]]] of [[[Roderick on the Line]]], but was later even mentioned in John's other podcasts [[[Omnibus]]] and [[[Friendly Fire]]].

+ Sections where it is used

[[module backlinks]]