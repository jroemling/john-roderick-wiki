This week, Merlin and John talk about:
* John has no coffee in the house ([[[Food and Drink]]])
* How Merlin and John's days started ([[[Currents]]])
* Looking at a big house early on in his search ([[[Mid-century modern]]])
* Green Construction ([[[Mid-century modern]]])
* Looking for colored appliances ([[[Mid-century modern]]])
* Buying something good, one-hatchet-culture ([[[Attitude and Opinion]]])
* The house that got sold twice ([[[Mid-century modern]]])

**The Problem:** It was to benefit the local violin makers, referring to John looking at a huge house that was built to entertain and throw parties with 400 people to benefit the local violin makers.

The show title refers to the attitude that you only ever need to buy one hatchet and you don’t need to buy a new hatchet every year, and how it can be applied to clothing.

It is going good. John is adjusting his levels and he has a lot going on!

Merlin got some new glasses and they went to Hayes Valley where there is a row on one block with Away Suitcases, Parachute Sheets, Warby Parker Glasses and another one coming soon, it is like podcast internet ad row.

Merlin kickstarted a space pen today for over $100, but it is bespoke with a PVD coating //([https://www.kickstarter.com/projects/danprovost/mark-one-apollo-11-limited-edition-space-pen Mark One Apollo 11 Limited Edition])// 

[[include inc:draft]]

+ John has no coffee in the house (RL344)

John is going with no coffee today, but not on purpose: There is just no coffee in the house and they have to do a ”just woke up and no coffee crutch” show. Merlin asks John not to say crutch, but he didn’t mean it ableist, but crutch like hot. He just wants to drink something hot, it doesn’t have to be coffee. Merlin suggests tea or oat milk, but John doesn’t feel that a teas is quite the... it is really about it being coffee and he will have a coffee later! Once they will get going it is going to be fine.

+ How Merlin and John's days started (RL344)

Merlin is doing a lot of tech things today, which is upsetting, he had //too much// coffee and he wished John had some of it. Drinking coffee in front of John is way worse than drinking in front of him. Merlin has low-acid [https://www.amazon.com/Tiemans-Fusion-Coffee-Ground-10-Ounce/dp/B00452D8GW Tieman’s] coffee and he demonstrates how his stepfather used to slurp his coffee. Merlin hated him so much! ”He is having a second cup! He never has a second cup of my coffee!” //(reference to a 1970s [https://www.youtube.com/watch?v=MJ4kCF22O2w Yuban commercial])// 

They left a lot of threads hanging when they took their little summer break:
* What is going on with John’s house situation at large?
* How did things go with John’s trips //(Motorcycle trip and vacation in the Baltics)//?
* What is going on with the Western States Hurricanes record?
All of the threads are of interest because everything is everything and time is a flat circle. This is this. It is what it is. A Solipsism is a Solipsism.

They talk about hanging threads, but hanging chads is when everything started going to shit! Merlin was thinking about it yesterday because he was listening to a podcast about the 2000 election. The smart guy on Barney Miller (Steve Landesberg), (Sgt. Arthur P.) Dietrich, said everybody started protesting against Vietnam in 1967 and he knew that something was happening in the 1950s because of the French and whatnot. He was smart and he was sloppy, he was a real role-model to John. Merlin loved that actor! While a lot of people are going to tell you: ”9/11 Never forget!”, the true beginning of everything going to shit was that god-damn election. In Florida the rail started getting going off of. Brooks Brothers riot! John would have lots of things to say about that if he had a little bit more coffee.

+ Looking at a big house early on in his search (RW344)

Most recently John went by the house that continued to be the best house, a mid-century modern house near where his daughter lives. He had already been looking for a house for a year and he went to tour this house three times. One day he went back when there was nobody there and tried all the door knobs until he found one of them opened and he went in and walked around for an afternoon.

Then there also was the original one, the first one he saw that intrigued him. It had a few great things: A circular driveway, a Japanese garden, real wood roof shingles, a very intriguing-looking wide low house. The first problem was that this house was 4250 sqft (400 sqm), which is a large house 4 times the size of Merlin’s flat, unless you talk about a McManson which often have 15.000 sqft (1400 sqm) and most of it is just entry way or mud rooms. This house was built in the mid-1960s and it was a house built for entertaining. The living room, dining room, and kitchen were all on the main floor and each was a giant version of itself.

The whole front of the house was a wall of windows and you could see the ocean and the distant mountains and as you wandered around the first floor, imagining you were having a party with 400 people to benefit the local violin makers. You wondered where the people lived and in the back there is a door to a beautiful bedroom, again with a wall of windows, and another big bedroom with a thick vinyl accordion curtain in the middle so you could turn one room into two.

This family had 7 children and this room was meant to be either two boys-rooms or two girls-rooms, but they could open this wall up and do what every teenager wants, which is to have their rooms be all one big room. They had built-in desks and stuff! Downstairs was an absolute warren of rooms: Five more bedrooms and a giant room that would have been the ultimate... //(John didn't finish that sentence)//

This sounds very costly, but in this neighborhood everybody is dying, it is a kill-show down here, and every hour another 92 year old is like: ”I can’t live in this house anymore!” and their kids are like: ”Alright, dad! We will put you in a home!” and they are trying to sell it as fast as they can. A lot of these houses are occupied by their original owners and this was one of those, but none of their seven kids wanted to live in mom and dad’s house. Nobody has seven kids anymore, but they have surely been fighting over stuff since they had been born.

If it was zoned for it, you could almost turn this house into an incredible office for a start-up. In the basement there was another kitchen, a giant multi-purpose room, five bedrooms and a room that was the gardening-toolshed and they would drive their riding lawn-mower right into it and park it. Merlin had such a boner right now! This sounds amazing! Then there was a band-practice room. The basement had super-tall wood-panelled ceilings as well! 

The downsides were that it had forced hot-water heating, like radiators in a New York apartment building, but in a normal house. There is a boiler and it is piping water up to the rooms. It looks like an electric baseboard heater, but it has hot water running through it and the people were not sure it was working right now. There were six bathrooms and you could have one bespoke bathroom just for diarrhea and you didn’t even have to tell people, but you could put a bookcase in front of it.

There was a big bathroom in the basement with two sinks and down at the end there was a big shower like a gym shower. Out the other side there was another bathroom because seven kids were trying to get ready for school in the morning and John always imagined there was a boy’s side and a girl’s side and they would yell out: ”Okay, shower’s clear!”

The place felt like John was inside the Brady Bunch, inside them each individually and mom and dad and Alice. Everything was original, like the kitchen, there was a built-in desk in the kitchen with a phone built into the wall where you would sit and talk to your friends while everybody else was sitting in the kitchen. You could gossip and drink pop. Outside on the porch some gas-plumbed barbecues were just built-in to the house.

This house must have been the tits when it was built, it was dynamite! The living room almost felt like a sunken living room, the sliding glass doors opened to the front, and most of the wall-windows were also doors. An architect certainly designed it and there was not another one in the world. You could be at a party and walk out the sliding glass door of the living room, down the porch and into the sliding glass down of the dining room while in conversation with someone who was walking inside the house the whole time and you wouldn’t even notice the passage of woodworks.

The only thing Merlin thinks of is that everyone in the family didn’t have to poop in the same room, but you could poop in so many places. It was lemon jell-o colored wall-to-wall carpet in the living room because that was the style of the time.

John couldn’t afford it at the time because he hadn’t sold his house, no bank would loan him money, and he had to sit on the floor in the living room and say: ”Okay, I can’t have this house!” It was early in his search, he had only been looking for a house for three weeks and this one came up, so no reason to sound the alarm bells, these chads are surely hanging down all the time!

John let it pass onto another with no protest, but out in front a giant dumpster showed up immediately and for the next nine months this dumpster filled and was removed and came back and was filled and was removed. They were fixing it up by taking out all the cool stuff like the original kitchen and everything.

They were surely taking the original windows out and replacing them with double-pane vinyl, which basically turns your house into a toxic terrarium, and put in ubiquitous granite counter-tops and stainless steel appliances, things that every brand-new cheapo condo downtown has. People flipping houses somehow also think that this is what you want in your mid-century house. Deep sinks and all the granite! Everyone wants granite!

When John saw the granite countertops and the stainless-steel appliances in cherry-wood cabinets in his farm his heart fell, but he loved the house so much that he bought it in spite of it. His kitchen was extremely modern, but not ”Zoo Where is George Jetson?”, but ”Hey, I went to Lowe’s!”

John had to let this house go and he had to watch it get desecrated, but in the month that followed he did not see any beautiful mid-century modern homes in proliferation ”Sam, I am”, but he realized how rare that house had been. He thought it was a killing field out there with 90-year olds dropping left and right and all their architect-designed homes coming up for sale, but what he found was a lot of mid-century modern homes with granite countertops, stainless-steel appliances, canned lights and laminate floors that had already been flipped by a monster.

Many men in their 20s today look like some bad clone of Travis McElroy, all with a beard and an undercut, and that is the thing with this house: The Haight-Ashbury neighborhood was made famous by the hippies. For the longest time until the late-1990s it was a shitty neighborhood because these absentee neglectful landlords didn’t do fucking anything to these houses, which is also the story of Portland Oregon. 

It ends up looking wonderful when you come in and fix it later and restore it, but you don’t have to grow a beard and have an undercut, you can do something that is not a deep sink or granitizing the counters, that is going to be like canapés on Chick-Fil-A in the 1980s and it is going to look so fucking dated in 15 years. You are going to have the affluence of an earlier age in a way that you don’t have the foresight to see right now, like Jnco Jeans and Day-glo colored sweatshirts with ”Frankie Say Relax” on it.

+ Green Construction (RL344)

A long time ago a business fraud came along called Green Construction. It is totally a racket because people buy a 100 year old house that is just fine and they tear it down and build it green. The whole exercise basically costs the world 1 million tons of carbon. There was a wonderful Planet Money recently //([https://www.npr.org/2019/07/09/739893511/episode-925-a-mob-boss-a-garbage-boat-and-why-we-recycle episodes 925 and 926])// that was hair-curling about what the fuck is going on with recycling right now and it just blew Merlin’s mind. He continues to talk a bit about it. A lot of this green stuff makes a lot of sense, though, for example solar is a no-brainer, but when replacing something old that already works fine you are burning all the sunk costs of that thing and reuse is better than recycling. 

Most single-pane windows are fine. The heat that is escaping your house is by and large not escaping through your single-pane windows, but through your uninsulated attic and your uninsulated walls. The double-pane window racket works because people sit in their house in the winter next to a window and they feel a chill and they go: ”Oh, windows!” and they can’t feel how much heat is going through the ceiling. Especially mid-century windows have thick glass. In an 1790s house the glass is turning liquid gradually and sinking down.

Part of the reason John bought his current house was that it has all its original windows. In the first nine months he got so many flyers slipped under the door: ”Hey, homeowner! Time to replace those windows!” and you can tell it is a racket because they send kids around with clipboards to knock on your door and offering great deals, basically praying on elderly and on dumb new homeowners. They are just a summer-hire working on some really bad commission structure and they sit on your porch and pressure you to hear their spiel, exactly like Jehova’s Witnesses, except you get probably more out of believing in Jehova.

+ Looking for colored appliances (RL344)

//John also mentioned this topic in RW124.//

It is very hard and quite expensive to think outside the box when it comes to appliances because the mass-production of kitchens has become an industry onto itself. John went to an appliance store with a friend not very long ago because this is how he spends his time with friends. It was not a big-box store, but a boutique store that only sells appliances, and the sales person was not a young person on a summer-break, but it was clearly an appliance sales person who had held this job for many years.

John said that when he was growing up there were lots of different colors of refrigerators, like avocado or harvest (yellow). Especially in the 1960s they were very bright colors, but eventually it just came down to white or something else. You could get an oven that matched your refrigerator, and Merlin had an all-avocado kitchen when he was 10. 

It was a store stretching like the warehouse at the end of Indiana Jones, but everything in there was silver, black or white. John asked how he could get from here to a pink refrigerator and the sales person put their hand on John’s shoulder in a chummy way and said: ”Let me walk you over!” and they walked over to a refrigerator and said they could get it in different colors, but it will cost $1200 more because it is a custom job. 

John asked: ”Is that a good deal?” and the appliance person shook their head ruefully and said: ”No, it is not! It is the same exact refrigerator as this one!” The only people who do this are the people who have more money than God and the person who is designing their kitchen is not answerable to them. There is no budget, but: ”Make my kitchen what I want it to be!” and the designer says: ”I need a pink refrigerator!”

This is bogus! The reason that all kitchens are stainless now is that there just isn’t another option and it is not even allowed! The world has denied the possibility that a normal person would want an avocado refrigerator. All of a sudden John realized that all of those granite counter top kitchens are an emperors new clothes situation: None of us want them, but we are being told that they are beautiful because it is all there is!

Little Pink refrigerators for you and me. ”Blood on the scarecrow, blood on the plow” //(lyrics Rain on the Scarecrow by John Mellencamp)//. Suckin’ On Chili Dog //(lyrics Jack and Diane by John Mellencamp)//. Why does he suck them? It is an odd thing to do, but it is probably a Mid-West thing from Indiana. "Not that Iowa sucks, but Nebraska blows!" That is not Oscar Wilde, but a thing they say in the Dakotas: "Not that South Dakota sucks, but Nebraska blows!" Either this region goes or I do! The only thing worse than being talked about is being from Nebraska.

+ Buying something good, one-hatchet-culture (RL344)

Buying clothing drives Merlin bananas sometimes, except Mack Weldon. He can’t find pants that fit him, but he is not that strangely shaped, he is pretty much a 35/30, but nobody wants to make a 35. It is 34 or 36. It is the same with shoe sizes: Merlin has to buy Doc Martens bigger and stuff soles in there because he is exactly between the two English sizes.

Travis McElroys has made that argument for 10 years: All you ever need is one hatchet. You don’t need to keep buying hatchets and you don’t need a new hatchet every year. You can’t afford everything, you can only afford the best. Millennials feel strongly about that: You are not going to have all the pens, but have one nice pen and you are going to take care of it. You just buy one bespoke hatchet. John was part of that culture with his Filson thing: ”These jackets are $400, but you are only ever going to need one!” 

Merlin likes his Cuisinart coffee maker, but he had to buy a new one every year or so. He tries to get his kid to understand this: You don’t want to buy six of something that is inexpensive if you could buy one of something that might be costly, but not expensive. John wished someone had said this to his dad when he was shopping for stereos in the 1970s. If you wanted to go spend $500 on a pair of custom Alden boots, they would fit your foot perfectly and you wouldn’t have to get a half-size, but people just want to get some Doc Martens.

John struggles with this all the time because some Doc Martens are made in England and some are made in Indonesia or Vietnam and the ones made in England are slightly more expensive, but they are worse and the ones made in Vietnam are great. Merlin mentions that it was the same problem with Timbuk2 bags that were handmade locally in San Francisco.

With Levi’s you are better off looking at several pairs, even if they are all from the same lot. Merlin had a girlfriend who, once she found something she liked and in the size she wanted, which took forever, she would always try on three of that thing. He thought she was out of her mind, but she was not because first of all: What does L and M mean? What is a size 4? You have to try it on because two of them might be the same and one of them might be different. You are doing a little bit of Monty Hall and pretty soon you are wearing pants that don’t make any sense with a weird rise. Nothing is the same! It is what it is.

+ The house that got sold twice (RL344)

Later last summer John saw another house for sale and from the moment it arrived on his phone at 8am he thought: ”There it is! That is the house!” The open house didn’t start until noon and John had somewhere to be, so he raced over to this house. He had no idea this street existed and it is an incredible street. It was like walking through a door in his house and there was another house. John drove up this street. You can’t really even see the house from the street.

The owners were there and he was Windexing the mirrors, getting it ready, while she was still in her housecoat. They were still living in the house, it had just gone on sale that day, and they were getting it ready for the first open house. John rang the door bell, they opened, and John explained that he had just dropped his daughter off at school and he has to go do his podcast with Merlin Mann and he thought he could just come in here for a second and look at the house because it was very beautiful. 

They were like: ”Oh, yes of course, come in!” and they gave him a tour of the house. They were a 92 year old couple who had built this house from an architect's design in the 1950s, they lived there ever since, they raised their family there, and they continued to live there into their 90s. Later the neighbors told John that they still play Tennis and he dyes his hair black. She wanted to excuse herself that she was in her house coat, but John was the intruder here and he just wanted to have a look at their beautiful house. They showed him all the things and everything he saw was perfect. This was a perfect house!

John said: ”I bid you adieu! I love your home! It is really perfection!”, but they said that they were in their 90s now and it was just too much house. That was the house, but John couldn’t buy it because he had to sell his farm, otherwise the banks wouldn’t give him money, so he watched this house sell and it broke his heart. He kept looking, but he was always thinking about this house and he would continue to drive by this street.

++* Selling a second time

A few months later the house came on the market again, but not because the sale fell through. The old couple had moved on to sunnier climbs, but a young 45-year old couple, presumably tech-peoples, had bought it on the assumption that they had a job lined up at probably Amazon and the job didn’t come through and so they had completed the sale of a home and it had cost them $50.000 to do this. They put the house back on the market and it sold a second time for more.

The house is completely intact! It was built in the 1950s, the owners lived there their whole life, they never had a problem, the stove was avocado, but they were fine with that because it kept working. The bathrooms where pink, at one point in the 1990s they put hand-holds in the bathroom, but other than that it was original.

In the living room there was wall-to-wall carpet and they didn’t put oak under it because there was a time when it made you feel like a poor to not have carpeting. One of the first things Vito Corleone does when he gets crooked is to bring his wife a rug. He also wrapped a pear in some piece of wax paper. It didn’t matter to John because he would also have wall-to-wall carpet in that space because that is what the room wants.

John watched the house sell a second time and it was worse because that many more months had gone by where he was looking for a house and there was none to find, none that he liked. Houses come on sale all the time with one granite kitchen after another, but this house with its avocado stove was really what John wanted. Is he crazy? Yes, of course he is! He is not trying on 5 pairs of Levi’s at the Fred Meyer, looking for the one where the legs aren’t proper matching, but like a passport, a pair of pants can call out to you. John wants linoleum and he wants shag!

John talked to his mom about it. This was horrible and he wasn’t sure how many more times this house could sell that he could bear, and because his mom was very influenced by his sister who was very influenced by The Secret, they both spend an awful lot of time saying: ”Just manifest it!” - ”Stop talking!” John’s mom said she had a feeling about this house and that John was going to live in this house one day, so just ”Let go, let God!” - ”Alright! I don’t know what shoebox full of belt buckles to put that in!”

All that manifestation talk had stopped several years ago because it had morphed and morphed. John’s sister is not one to renounce things and probably hasn’t renounced it, but she is not on that trip anymore. You start with Elizabeth Gilbert, you move into The Secret and then eventually you are leading guided meditations in Guatemala, like she does now. You eat and you love and eventually you pray!

Right now John is the living room in his house, which is why he doesn’t have coffee because the house is evacuated. Right next to him is a box of pretend sushi that his daughter was playing with, if that gives you a sense of how bourgeois he is: ”Darling, that is the Maguro!”

++* Finding things out about the owner

In the 6-9 months since the house sold a second time John continued to drive by and there was nothing going on! There was no dumpster out front, there were no lights on inside, there was no car parked out front, nothing had changed! After a big windstorm the driveway was covered with pine cones for a month. John parked, went up, peered in the windows and saw some boxes on the floor and some children’s shoes in the entry way, but no-one was living there. There was a very elaborate security camera setup of the kind that you can get at CostCo. It looks expensive because there are 30 cameras, but basically you lick the bottom of a suction cup and you stick it to the window.

After a few months a very expensive BMW showed up in the carport, but it never moved. John continued creeping for a while, but eventually he got frustrated, did some research, and found the tax records of the person who bought the house. They live in Los Angeles and John was able to find their house in Los Angeles, a brand new super-modern loft-style town house in Silverlake. John reached out to the real estate agent and found out that the person who bought the house was a recently divorced wheeler dealer who bought the house to have a place to hang out with his kids.

If you need a place in Seattle to visit your kids every other weekend you should get a three-bedroom apartment that when you lock the door and leave it will be same when you get back. A house is a lot of maintenance, just get a condo! The new owner knew that John had talked to his real estate agent and he indicated that he might be convinced to sell if the price was right, but John let it ride because he didn’t have the money.

++* Being ready to make an offer

Now John has completed the sale of his house and it has cleared with the bank and put him in the position where the banks will loan him the money he needs if he has the downpayment. If it is in John’s ballpark depends on how wheeler dealer he wants to be. When the house sold the first time it was in John’s price range. When it sold the second time it was still sort of in John’s price range, but if he wants to be like: ”You know...” The market has flattened, which is in John’s favor, and there is a lot going on and there are a lot of factors to take into consideration.

The other day John wrote a letter to the owner, saying: ”Hi, my name is John! You might have heard of me from such popular shows as Roderick on the Line with Merlin Mann! I am the guy who was looking at your house several months ago. I am still interested in talking, why don’t you give me a ringy-dingy?” John slipped it under the door and the guy called! He said he is real busy, he flies a lot, but he also told John how much he has fallen in love with the house. He has been living there for 6 months every other weekend, between 3 and 13 weekends, not a lot, but he did put a big TV in the living room, and one time John saw him playing a first-person shooter game while standing in front of his television.

John liked him! He was not impersonal, he gave John some background, and he said he was an architecture minor in college and he really likes the mid-century modern style and is emotionally invested in it. His ex-wife and kids live over in Bellevue, which is weird because it is 45 minutes away, and why wouldn’t he get a condo over there? His answer was that the house was close to the airport and he flies a lot and he is busy.

Then he said that he got some money invested in the house, which is where we start to drill down the brass tax, but John has been around the house several times in the night and he doesn’t think that is true. He did a lot of yard work, which meant he hired somebody to come and rake up all the leaves and pine cones and that was maybe $500.

He said he is a big fan of the mid-century modern style, but it needs some updating and he hired an architect who has drawn out some plans. He was going to gut the kitchen and the bathroom, he was going to open up the kitchen into the living room as a more open plan, and he said he was going to get granite countertops and stainless steel appliances to bring it into the modern era and make it look cool. It is going to be a big job and he was looking to spend a bunch of money on this place.

John was really holding his tongue at this point and replied: ”It sounds like I am a little bit more rustic than you and I would probably just live in it as it is!” and he gave him a big laugh. John didn’t want to reveal that he had looked at his house in Los Angeles, but he should have said that his house he had in Los Angeles was probably the house he also wanted in Seattle: Modern, granite, turn-key. ”Why don’t you get one of those close to your kids in Bellevue or just abandon the whole pretense that you want to see your kids and just follow your career!”, but instead he said: ”This is a lot of house, this is a lot of upkeep, and it seems it is probably not the best place for every other weekend!”

++* Waiting for the call

He asked: ”What kind of ballpark are we talking about here?” - ”If we do this transaction privately without the use of an intermediary we would be saving ourselves $50.000 in cost to sell this house! Let’s just get this done off the book!” His sister was a lawyer who works in the real-estate thing and she could probably draw up a contract really fast. ”Yeah, exactly!" John’s sister was leading a guided meditation in Guatamala. John put a number on the table between the price he bought it for and the price he told his real estate agent that he was willing to sell it for, right down the middle, because John did not want to pay the amount that he asked.

He replied: ”Well God, the money I put into it! I feel like... I just don’t... I’m not sure I want to sell. I’m really... We said everything there is to say... I just feel like I need some time think about it. Why don’t I think about it over the weekend and I will give you a call early next week, Monday or Tuesday?” - ”Great talking to you! I really feel like talking to you has taught me a lot about how the world works and I look forward to hearing from you!” - ”Great, I’ll give you a call next week!” Now as they record this episode it was Monday and it was Anything-can-happen-day!

The phone could ring and he could go on about how much he had bonded with that house and how much money he put in as a ploy to get John to ask: ”What is it going to take?”, or he could come back and say: ”I want x amount of dollars!” If he had wanted a crazy amount of more dollars when John gave him a number he would have laughed, but instead he went: ”Ah... mm... yeah” He was wheeler-dealer-ing because he was still talking!

At the end of today John could have talked to this guy again and he could have said: ”Yeah, I am willing to sell the house to you, why don’t we get this ball going?” in which case John would be in a transaction. Merlin warns John to pre-jinx it and he thinks the guy is probably not going to call because he sounds like a dingeling and not like a serious man, but those are who make the world go round. Merlin flew in super-business class on a United flight recently and there were a lot of insufferable people up there.

If you like business, this transaction is an opportunity to do another business transaction that will stand in place of having real feelings and going to church on Sunday. John was saying: ”Maybe you are not going to get rich, but the market has flattened out” John thinks he is going to come back to negotiate because that is what business people do. The number john said wasn't outrageous, but it was perfectly reasonable and in fact probably represented the value of the house.

Maybe he will do some due diligence on John and listens to 50 episodes of their award-winning podcast Roderick on the Line and say: ”I really like the latest episode where Merlin called me unserious!” - ”Well, that was Merlin! If you love the show like I love the show, you know that Merlin... amirite?” Merlin says: ”Do it! Sacrifice me! I don’t have any dignity! Do whatever it takes to find a place to live!” John has just left even more threads hanging, so tune in next week for John crying in front of the business man!