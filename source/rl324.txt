This week, Merlin and John talk about:
* Merlin disagreeing with musical genres ([[[Music]]])
* Feedback on the Yngwie Malmsteen-episode of The Omnibus ([[[Music]]])
* Merlin wanting to be on the Omnibus ([[[Podcasting]]])
* Cats and roosters in Hawaii ([[[Hawaii]]])
* Merlin’s cat, visiting a cat show ([[[Pets]]])
* John being a cat person ([[[Pets]]])
* Louis the cat ([[[Pets]]])
* John’s family being picky eaters ([[[Food and Drink]]])
* John’s aunt and uncle buying the house on Hawaii in the 1970s ([[[Hawaii]]])
* John trying to be aloha ([[[Hawaii]]])

**The Problem:** They all grow in the shade of the night, referring to nightshade vegetables like potatoes and tomatoes.

The show title refers to John’s sister’s dietary preferences. She will for example only eat certain types of pasta where the wheat has been free-roaming and not caged. Caged Wheat sounded like a movie title to Merlin.

When they recorded this episode John and his family have been in Hawaii for 8-9 days already and he was going to be there for another week and a half, a total of 19 days. This show will come out today on President’s Day. 

There was a rooster calling outside of John’s window. After that it sounded like on a jungle cruise. There is a guy on a boat who is about to shoot a hippopotamus. 

> **Draft version**
> The segments below are drafts that will be incorporated into the rest of the Wiki as time permits.

+ Merlin disagreeing with musical genres (RL324)

Merlin is looking at people’s Spotify playlists and noticed that some of them get their terminology wrong. Everybody is entitled to like what they like, but he is looking for Chamber Pop and he will suffer some Baroque Pop for sure, but he does not want too much Neo-Psychadelia. We can all agree that Penny Lane is a Chamber Pop song, but Merlin doesn’t need five Beatles songs on this list because he already knows about The Beatles and wants to hear some new things. He knows The Zombies and they are a fantastic band, but he wants to jazz it up a little and get some 1990s / 2000s stuff that he hasn’t heard. He wants to jazz it up, but doesn’t want it to be jazz. He could probably use some help when it comes to Jazz.

+ Feedback on the Yngwie Malmsteen-episode of The Omnibus (RL324)

John did an episode of The Omnibus about Classical Metal //(OM126)//, but not about Mannheim Steamroller, but the title was Yngwie Malmsteen. John doesn’t know that much about him, but he knows Yngwie likes some Paganini and some Bach. At the time John found him unlistenable and he still does. It is like watching somebody do their taxes really fast. 

John tried to describe, starting at Paganini, how that could have possibly produced Ritchie Blackmore’s band Rainbow through the 1970s and so on. He was in trouble immediately because he was talking about Prog although he does not know that much about Prog. He is not a Prog, but he is not an outsider either because he is a musician. It is the same with Merlin and Harry Potter. He has seen all the movies, but he hasn’t read all the books. 

John knows an ELP (Emerson, Lake & Palmer) song when it comes on, he knows a portamento on a Moog and he even knows how to pronounce Moog! He seems so qualified, but if he goes finger to finger with any serious Proginator, they are so fucking mad, like "What about Gentle Giant?" - ”I don’t know! What?” John waded right in and talked about the German band Shitwind, but people were just ”No, fuck you! You don’t know what you are talking about!” So many people were so mad! 

The original Shitwind had Van Halen style wings on either side of their logo, but the later Shitwind was using a Halo. Bands who break off like this are confusing! For a time there was a Fake Zombies who broke up even before Time of the Season or Odessey and Oracle came out. They were so mad at each other and everything and the system. The thing is that you can’t talk about Yngwie unless you talk about both phases of Shitwind. 

Merlin has a Vinyl copy of the album Steeler by Steeler from Metal Blade Records circa 1981 //(actually 1983)// with Ron Keel on vocals. It was his preeminent band before Alcatrazz and His Rising Force was the first solo album. John just wanted to mea culpa this whole thing and say that he clearly doesn’t know enough about it to talk about it. It is not interesting to a lay person at all. There were also some people on the Internet who thanked John for teaching them about Yngwie, though.

Everything John did to prepare for that episode confirmed in him that he did not find that music interesting and that he is not the target audience for the Mixolydian mode (white keys from G) or the Phrygian mode (white keys from E) as pioneered by Ritchie Blackmore, a very talented musician. He has that one song on that one album with the Bach in it. 

Machine Head by Deep Purple was one of the first albums John ever got, it was one of the first legit Metal albums and John didn’t know that the lead singer of Deep Purple Ian Gillan was a very handsome man and also sang on the Jesus Christ Superstar album. He was shockingly good-looking! Ted Danson played him in the movie and on One Night in Bangkok Murray Head sang the very challenging Judas part. Merlin loves musical theater and John should do an Omnibus on that and get all those people mad. 

+ Merlin wanting to be on the Omnibus (RL324)

Merlin wonders why John doesn’t do an Omnibus on him and have him on the show as a guest, but John thinks Merlin would be so mad at John if he did. John keeps threatening Merlin that he can be friends with Ken but it never works out and they haven’t even done Korean Fan Death yet, so Merlin doesn’t know what the fuck is going on. He is sitting with his dick in his hand, trying to improve the medium, making connections, talking about unleashing a dragon! 

Merlin could tell the story about snorting Rush at an Yngwie concert and he has all kinds of little anecdotes that could make John’s little show really good! John admits that a lot of people don’t understand how important Merlin is, not just in podcasting! A lot of exes are not appreciating the legacy and Merlin has left a long brown stain across the United States, like a comet shooting through the stars, like a skid-mark from a chopper. 

+ Cats and roosters in Hawaii (RL324)

There are jungle noises in the background because John is recording from the jungle in Hawaii. The most annoying thing about Hawaii for Merlin were all the roosters and he had no idea why there were so many fucking birds in the morning. That and Ceviche just ruined him. There is some story that chickens and roosters used to be in cages, but then there was a hurricane that broke up the chicken coops and now they all roam free. 

It is the same story as with Hamilton meets Nutria (Coypu) in New Orleans: Nobody wanted these fucking fake beaver jackets anymore, so they just let them loose and now they have these giant rat-like things roaming around everywhere in New Orleans. The New Orleans cajun cooks tried to put Nutria on the menus of the restaurants because they had too many of them and they tried to invent cajun dishes, but anybody who has ever seem a Nutria doesn’t want to order that. You don’t want to eat a giant rat! In Hawaiian food culture, chicken plays a role, albeit not a major role, there are free chickens everywhere you look, and you can just grab one! 

The roosters have been been plaguing John to the degree that he needed to buy earplugs. There is one rooster living outside his window that is either a very young rooster who has not figured out how to crow, or a very old rooster who has lost the ability to crow musically. John can hear other roosters in the distance that sing like roosters normally do, but this one outside John’s window doesn’t know how to do it properly. 

John wants to go out and talk to him about a lot of things, like his choice of where to crow, because outside of John’s window he is not reaching the largest possible audience, he is like busking outside of a police station: The cops are never going to put any money in his hat. John wants to see if he is a thread-bare rooster on his way out and if so maybe he could hasten that process, or if he is a rooster who needs to start a new life somewhere. Rather than go out and confront him at 4:00am John got some earplugs.

Merlin has been on Maui before and they were chock-a-block with fowl and kitten. John’s cousin Libby was at the house a month ago and began a tradition or feeding the Feral Cats. She bought cat food, but not just dry kibble, but a case of canned wet salmon. When John’s family arrived his daughter embraced this as one of her special projects: They have to feed the cats three times a day, which is like buying food for ghosts. 

Now the cats congregate at the house because the word has gotten out in cat town and right about the same time that the rooster starts his mournful plaintiff cry the cats also start either fighting, which is one of the great sounds. Maybe they are fucking or just talking, but they are making an awful lot of noise in the middle of the night. John's family has attracted this nuisance! They have brought ghosts into the home! 

John has nothing to do with it, but he just stands there shaking his head, mourning as other women in his family, of which there are too many to mention by name, encourage his daughter in her project. She didn’t start this, but John’s cousin Libby started it who is a full-grown person and should know better. Now it has become part of their family tradition to spend $80 feeding stray cats so they can fuck outside of John’s window in the middle of the night. 

Merlin does not want to deny any animal food, or any child pleasure, but it has to stop! Somebody in their park spreads corn to feed the pigeons. Go to a nursery school and feed some kids, give them some corn, spread corn, not hate! What kind of monster would John be if he walked into the center of the household and said ”Listen, we are going to stop feeding the cats!” He would be history’s worst! 

He would be attacked on all sides and increasingly he is just sitting in his chair pretending to read the newspaper while things happen around him. He suddenly understands the archetype of the American father who looks up over the top of the newspaper, takes a puff on his pipe and goes ”Say what?” and then goes back behind the newspaper. ”We are feeding the cats” - ”Oh, okay. What?”

+ Merlin’s cat, visiting a cat show (RL324)

Everything happening in Merlin’s life right now is like ”What? Why? What are we doing?” The other day he was at a cat show, which was awesome! It was like a dog show, but with cats, and it was one of the best family trips he has ever had. There was an agility course for cats and they met a very famous agile cat, a Japanese Bobtail named Zoom who could do the whole course in 20 seconds. 

Merlin would not have put that on the calendar on his own, but he has now reached an age where people will just let him know when it is time, like ”Oh, by the way, this weekend we are going to a cat show for a couple of nights!” It was in Santa Rosa, a little over an hour away. It was close, it was low pressure, it was inexpensive and they got to see people trying to make cats do things.

Merlin’s family made the transfer from a young family that did not have a cat to a mature family that had not only a cat, but a cat with problems. She is a precious angel and Merlin absolutely loves the cat very very much, but she is a grotesquery! John assumes Merlin is preparing for the inevitability when the cat with problems becomes a dying cat. It is definitely discussed very often and the cat show was probably the worst possible environment to go for when that topic is in the air week-to-week and month-to-month. 

Merlin’s Twitter header photo of one of the judges gives a flavor of the cat show. Every cat in the picture has a story and there was a harlequin mask because the show was Mardi Gras themed. The judge looks like the actor Donald Pleasence, his shirt was very laundered and he is holding a cat that is the same breed as Merlin’s cat. It doesn’t look exactly like that, but it is a very fluffy Persian cat.

Like with the trip to the cat show, Merlin was just ”informed” they were getting a cat. He got a FaceTime call that somebody was going to give his girls this cat that they were about to bring home. They said she is nine years old. It is not the cat that Merlin would have chosen! The cat is not covered with tumors, but she does have knots. 

A lot of the cats at the cat show got judged. There was a pet competition where they tried to find the champion. Merlin asked the owner of number 51 why their cat didn’t win and the problem was that there are champions and grand-champions and you have to go up the ladder and beat 200 cats. Their cat was just doing its grind right now. These people are really serious about it, which makes it extra fun.

John wonders if they had any of those Siberian forest cats at the show, but Merlin doesn’t think so. He heard that Persians are a very popular breed although they are very costly and have health problems. There were a lot of hyposophilic (Merlin surely means hypoallergenic) tiny-head Egyptian style of cats, a lot of tuxedo cats, a few Maine Coon style, a few of the larger cats, and there was one who claimed to be a Bengal cat, but it was a pretty small one. Merlin did not see a Sawannah cat or any of the very large Feral-type cats. 

These cat-breeder people are a lot about making money and getting status. They were certainly trying to sell them cats. Merlin saw a sweet precious angel that looked like Crookshanks from Harry Potter, which Merlin hasn’t read, and she was so nice. The woman was really friendly but as soon as Merlin had the temerity to say that they were not getting a cat today, she was gone immediately.

John thinks a Maine Coon is very related to a Siberian or Scandinavian forest cat. They are 3.5 feet (100 cm) long, they are massive! One of the great things about a Maine Coon is that they are nice cats, they are sociable, and they are like the big guy on a sketch comedy team. They are lovable and fun, their heads are large like a bear, and you do want a large head on a cat! They act like dogs and apparently they are hypoallergenic and they have another kind of fur, or maybe they don’t have fur, but hair and John doesn’t even want to ask what the difference is.

+ John being a cat person (RL324)

John was a cat person growing up. He likes kitties, he likes cats, he likes kitty-cats. The problem was that he was allergic to them the whole time and the reason that he was a sickly child was not that he had Pleurisy, but he was allergic to the cats that were sleeping on his face the whole time. It never got interrogated because there had always been cats around and John always had a sniffle. 

In the 1970s people didn’t understand about allergies and didn’t know they existed, or they didn’t acknowledge them. There is no way that peanut allergies exist in 2019 and didn’t exist in 1979, but people just ignored it and kids died right and left! They died in storms, they forgot them at the mall, and you just make more kids because you need them for the farm. 

Their cats were called Simba Jacoby and Tiffany Michelle. Whenever a kid or even a grown-up person does a job John’s mom is not above coming along and redoing it. There was some dispute in the family how to name the cat and John’s mom solved the problem by cutting the baby in two and craft the cat-names together. Those two cats would follow Manuschka around the neighborhood and there are delightful stories in the family that John could regale Merlin with for hours.

The other day John was on a plane and they said on the intercom that there was a person on the plane with peanut allergy and not only would they not be serving peanuts, but if you brought peanuts, you couldn't eat them either. Everybody is a little allergic to cats, but people will suffer all manner of indignity to be proximate to cats because they give us so much.

+ Lucy the cat (RL324)

//see also story in RL264 and a quick mention in RW89//

One time before his daughter was born John had a cat called Louis that he loved so much and that died in a very tragic manner. John tried to replace him when a friend with a young daughter had a cat and the little girl's father had left the family in a way that enraged the mother, and he tried to resolve the issue of having left by bringing an unwelcome cat and saying ”I got you a kitty!” 

The mother was already enraged at the father for various reason and had now a quadruple rage because he brought a cat into their home and then just left. She hated this cat that was carrying some baggage and was emotionally injured by something that had happened in an earlier life. Merlin’s cat definitely has PTSD. There are a lot of ways you can damage a cat’s brain. Maybe its people were driven from their lands and it had pain reverberating through generations. 

The cat would also attack her little 1.5 year old, so she called John and asked ”Will you do something?” John had just recently lost a cat he loved and she said ”You have to help here!” - ”Actually I don’t, but I will and I will take the cat” The cat was named Lucy and she was just terrible. You would pet her and she would purr, you would pet her a second time and she would purr a little bit more and nudge into your hand and you would think ”Oh!” and you would pet her a third time and she would maul you without warning. You didn’t do anything different, you didn’t grab her tail and stick your finger in her pooper or anything. It was very challenging like adopting a political science major.

John doesn’t mind getting mauled a little bit, any of his lady friends all mauled him at one point or another, and he would let Lucy shred his arm a little bit because they were sharing a house, but she went too far and thought that it was her time to shine. It became a situation where John didn’t want any more of this cat, but he couldn't just take it out behind the wood shed, it is not like a Hawaiian rooster that you can just put it in a pot. Fortunately Eric Corson, bass-player of [[[The Long Winters]]] would come by and he thought Lucy was amazing. 

One of the great things about Louis was that he was a beautiful cat, he was small, and you would think he was a teenage cat who was not done growing. He was very attractive and proportionate, not to say that disproportionality is not attractive, but it is what John looks for in a cat and it is why he doesn’t like smooshy-faced cats because their nose is not proportionate. Lucy was one of those cats who’s head was too small, who had a weak chin and there is just no curb-appeal. 

John would fluff her head-hair up a little bit to make her head look more proportionate, but there is only so much hairstyling you could do. Eric thought she was beautiful and they loved one another. One day John asked him if he would like Lucy to live with him and today 10 years later Eric and Lucy are still living together. He comes home and calls out to her when he walks in the door. 

They are having a game that when he wakes up in the morning and goes into the kitchen, Lucy waits around the corner and shreds his ankles and he goes ”Nah...” It is the best possible ending for this story. Every once in a while John goes over to Eric and will see Lucy and have a little reunion. He doesn’t know or care if she remembers him, but he knows her and he only pats her twice and doesn’t go into that third pat.

+ John’s family being picky eaters (RL324)

//John was distracted by a parrot in the tree.//

John’s sister decided that this season was another season where she had difficult food needs. What meets the rigorous standard for consumption by Susan has always been a short list, certainly shorter than John’s and shorter than the list of most grown-up people, but hers is equivalent to ”I can’t tell you what pornography is, I will know it when I see it” When you present her with food, she will reject it, when you suggest food for them all to get and enjoy, she will reject it. 

She is one of the kinds of people, and there are a lot of them in the world now, who makes a separate meal to accompany the meal that everyone else is having, no matter what the situation is. Merlin is familiar with that, but not for a 40-year old. Even if she eats pasta with just salt and cheese on it the pasta needs to be other. How was it sourced? Did the wheat have a name and a free range or was it caged wheat? Merlin loves that movie, but John didn’t like Brat Pitt’s accent. It is a complication! 

John also has a 7.5 year old daughter who needs a separate meal, but it is not the same as Susan's separate meal. John’s mom is no longer a gamer, a go-along-get-along, but she has decided that she can not eat all kinds of foods across a wide spectrum of foods anymore. Her list of food does not follow the normal lines where somebody would say they don’t eat dairy, nuts or meat, but she does not eat certain night shades or certain beans. Some of the nuts she can eat, some of them she can’t. 

Best in Show came up a lot in the last 24 hours for Merlin. Potatoes, Tomatoes, Eggplants, Peppers, Chili pepper, Goji are examples of nightshade vegetables and they all grow in the shade of the night. It has become very difficult to eat with John’s mom, but she is one who travels with Tupperware and when she arrives somewhere and business is being transacted and things are happening, when someone says that it is the time of day when normal people eat, John’s mom pulls out a Tupperware. The great trick is when you say ”It is time to eat” - ”Yeah, let’s get some food!” - ”Great, let’s get Mexican!” - ”Oh, I can’t!” and you are on the trail.

Merlin does this is a different way: Before they go out to ”dinner” at someone’s ”house”, he will eat dinner before dinner, because he doesn’t want to hear that the host had some drinks and now they are eating five hours later. Merlin will assume the food will not even appear. He doesn’t talk about this with anybody, but he will eat a meal before the meal and that way if there is no meal he is still good. John has a pre-meal and a post-meal. There is nothing wrong with that, you are making it easy for everybody because you brought your own Tupperware. John always has a Landjäger in his back pocket whether he needs it or not!

In addition to John’s sister and daughter, they are also living with John’s 93 year old uncle Jack who only eats American food, and within that category he only eats the subset that you can look at and in a glance tell all the ingredients, but a steak would be too much work. He is right at the point in his life where he wants a bowl of tomato soup and a toast. He doesn’t have complicated needs, but when it is time to have dinner, John doesn’t want tomato soup every night. 

John is in a household with 5 people where only two of them are actually ready to get dinner at any one point in time. The little one cannot prepare her own meal, the oldest one cannot prepare his own meal, so they are being orbited by moons that need care and in the center there is this very small group that is like ”What if we got sushi?” or ”Hey, there is an Indian food place!” But they also can’t abandon the others. They can’t say ”We are going to get Indian food tonight, here is some bread and cheese!”, because arguably they are the center of their lives, not them. They are not here for themselves, but they are here for them. 

Merlin says that there are a million ways to deal with this that are not very pretty, but very effective. When you are on a vacation with your family, you do stuff together and have a thing. How do you even chose a restaurant to go out to dinner? The only thing that everybody likes is shaved ice. Hawaiian cuisine felt like a prank to Merlin. They put a chicken on a rice, they cover it with an egg, and they put gravy on it, which sounds like the Merlin Mann fantasy dinner, but that is how they get you, that is the tar baby, it looks like something you want to go and grab, but it ain’t! It is like living inside a movie poster, it doesn’t taste good. Merlin would have given up because there were too many options.

They have been eating at home a lot because for uncle Jack at 93 years old going to a restaurant is a bigger deal. John is not invested at all in uncle Jack growing up healthy and strong, he would just like him to stretch more often and stop yelling at the gardeners, but regarding his daughter he is still in a fatherly mode. She cannot eat noodles and salt every day in her life, but she has to have one other element. 

She will eat broccoli and she has gotten to the point where John can slip a third ingredient into a thing and she will go ”Yeah, okay”, but it can’t be a foreign ingredient, a green that she can’t identify or any sort of sea food unless it is tuna with mayonnaise, with, although it smells like fish, is somehow accepted by all the people who don’t like fish. 

The situation is further complicated by the fact that the adults in the room who do want to eat interesting food are not good cooks. Cooking at home is the simplest solution, but the only thing that the adults in charge know how to cook is spaghetti and tacos, and with spaghetti you have a problem nightshade, tacos you are going to have a problem with sourcing, content and spicing. You can never spice things where everybody is happy. 

There is nothing worse than a taco that doesn’t taste like a taco, and there has already been too much fake innovation in the taco environment, it has to stop! Wrapping something in a chicken patty is not a taco, but it is literally a holocaust. John had a fish taco yesterday, but it was not good. Also, a fish taco doesn’t travel and if somebody makes you a fish taco you have to eat it immediately. After 8 minutes of travel you have a tortilla stew with fish in it and everything is moist, it is already dressed with white sauce on it (not a problem).

+ John’s aunt and uncle buying the house on Hawaii in the 1970s (RL324)

In 1970 John’s aunt and uncle came to Maui, the very South of Kihei, right on the border with Wailea, at a time when the Kihei road was made of dirt and only Tiki lamps lit the way. Together with three other families they bought an acre of land (4000 m2) across the street from the beach and the four families built four matching homes in a semi-circle, a compound with a wall around it made of volcanic rock so you can’t see in. 

They all share a tennis court because they all loved tennis. At the time there were just sand dunes across the street and when John was a little kid and a medium kid, they would walk down to the beach because at the time if a group of kids wanted to go to the beach, the adults would say ”Get out of here and don’t come back until the sun comes down!” The kids would spend all day at the beach while the adults sat up there, drank and played tennis. It was a nice life!

But time passages and many years later uncle Jack //(not the uncle who owned the house)// decided that he didn’t want to be in Alaska all winter anymore and he was going to come to Hawaii from January to March because it is better than Alaska. He was going to go back and rent the house, but not the Knudsen house, but the Ketchel house. The Knudsen house was the last one of the four, the Ketchel house is the second of the four //(see also story about the Knudsen winery in RW70)//. John’s family stays at the house every year which by now feels like home. Today the beach side is all mansions and you no longer walk across the street into the sand dunes. There is a public walkway with mansions on either side. 

The other day they saw a house for sale on the other side, a very humble little mid-century modern bungalow that had already been there in the old days. It is perfect, it is small and manageable, and John wondered what they wanted for it: $12 million. It is 900 square feet (80 m2). They are selling the house with the presumption that the first thing you will do is bulldoze the house and build a 15000 square foot (1400 m2) place with a bar and a pool. It is a great location, as they would say in real estate. In the past when John used to come here, there were geckos everywhere, there were mongoose, there were giant spiders, dinosaurs, and creepy crawlies, but all these things seem to be gone. John hasn’t seen a gecko the entire time he has been here. What they have now are roosters.

+ John trying to be aloha (RL324)

John has been on a tropic vacation with Merlin in the past //(They have been on the JoCo Cruise together in 2015)//. Does Merlin get Aloha if he is on Hawaii for a week? It is complicated! He has never felt so unwelcome in his entire life, they did not like Merlin’s family and he totally understands it. Merlin heard on a podcast what they did to Hawaii and it wasn't nice. They basically took the queen and put her in a fancy jail for a while. Then they took over and made it a large fruit company. They did a number on Hawaii! 

When John learned about Hawaii when he was young he did not try to pretend that this didn’t happen. It is a little bit like the Brooklyn accent problem //(see story about the guy who was trying to fake a Brooklyn accent in RL25)//: If someone Mahalos him, he will Mahalo them back, but if he is in a transaction with someone, he will not initiate a Mahalo, it would be like speaking Spanish to a waiter in a San Francisco Mexican restaurant. 

There is a lot of Aloha in Hawaii and John really tries to aloha the shit out of stuff. When he is on this island he drives 20 mph (30 km/h) because he is not in a hurry to get anywhere and no-one else is in a hurry to get anywhere either. Anybody who is in a hurry is from somewhere else. Also in a service-situation where someone is waiting on you or you are waiting in line to get a shaved ice, John just goes full aloha and is not impatient. 

The other day two ladies of John’s age who seemed to be married set up next to them on the beach and turned a stereo on. When it first started he looked over his shoulders, thinking ”Fucking millennials!”, but then he realized they were Generation X people who have adopted the idea that the entire rest of the world was just a Truman show around them.

It felt like they were performing happiness as if they were making a Male Enhancement Commercial. They were in two outdoor bathtubs side-by-side. Every part of John wanted to say ”If we wanted to hear Paul Simon’s Graceland, we would be listening to it on our headphones. We don’t want to hear it, because we are enjoying the sound of the waves!” The beach is not where you bring a stereo.

If you are hiking on a trail in the forest you will almost certainly encounter some Millennials who have a stereo in their pack that is playing Drake as they hike through the forest. John started to construct the scenario where he, a white man on a beach in Hawaii, was going to walk over to these two performatively happy women and said ”Can you turn your music down so that I can enjoy the Hawaiian sounds with my family who were sitting here already?”, trying to think of all the ways he was going to prepare for the blowback, he wondered if he was being aloha, because the aloha had been here before him.

When John is walking on a trail in Hawaii and sees some Hawaiian people coming the other way, he doesn’t try to get them to be happy or friendly to him. He doesn’t go ”Hello!” or ”Aloha!”, but he lets them set the tone, which is generally pretending John wasn't there, very consciously wishing that John wasn’t there. As the years went on, John got in more and more situations where he was conscious of the other person wishing he wasn’t there. When he was young, people didn't want him there either, but he didn’t notice it or didn’t realize it. If they meet on the trail, John is not going to bite his lip and give you a head-bob, you don’t want that! Instead he is just hanging loose and he is shaka and aloha as much as he can be.

This is the longest vacation John has been on in a long time and it is past the point of vacation length and into some other kind of length. It was not planned, but it was a situation where uncle Jack, who is very capable, is just past the point of being fully capable. Everybody else has to get back home and get back to work, the baby has to go back to school, everybody else was just here for a vacation. 

Today everyone is leaving except for John and uncle Jack. For the next 9 days, which is in addition to the 9 days he has been here already, John is going to be making ham & cheese sandwiches for uncle Jack and uncle Jack is going to explain to John how his father bayonetted a German and that was why he became an alcoholic and that was why he needed to run for mayor in the 1970s because of course he needed to be mayor of Anchorage to make up for the fact that his father had killed this German with the bayonet, supposedly. John’s family took down a lot of access men. Aloha!