This week, Merlin and John talk about:
* Being late ([[[Attitude and Opinion]]])
* Merlin going to the wrong concert ([[[Merlin Mann]]])
* Mike Squires playing guitar solos on Instagram ([[[Music]]])
* Getting late night food ([[[Food and Drink]]])
* Merlin's wife making coffee at 2am ([[[Merlin Mann]]])
* Interstellar ([[[Movies]]])
* John writing an article for City Arts Magazine ([[[Currents]]])
* John’s dream assistant ([[[Assistant]]])
* John wanting to be a TaskRabbit ([[[Dreams and Fantasies]]])
* John’s and Merlin’s kids being sticklers to the rules ([[[Children]]])
* Private roads ([[[Cars]]])
* Waze directing people through small neighborhoods ([[[Cars]]])
* John taking a primitive trail with his daughter ([[[Currents]]])
* John thinking he is fat ([[[Depression]]])
* Failing or succeeding in life ([[[Depression]]])
* Feeling anxiety about moving ([[[Depression]]])

**The problem:** Here’s how you clever-manage John, referring to John not minding being managed as long as you are clever about it and constantly lie to him about the timeline. 

The show title refers to John’s daughter being a stickler to the law and not wanting him to drive on private roads. 

John was a little late. He likes to be on time, but maybe he doesn’t like it. He likes things to be on his own terms. 

Merlin only realized today that he is 1 year away from being a Baby Boomer which is born between 1945-1965, but that line in the sand has been shifting the whole time. 

> **Draft version**
> The segments below are drafts that will be incorporated into the rest of the Wiki as time permits.

+ Being late (RL298)

Merlin was very late to everything all the way into his 30s, during a time where he had a choice to be better, and he didn’t realize how sociopathic and disrespectful of his colleagues and friends it was. Somewhere in his late 30s he felt ashamed about it and made a practice out of being early to everything. Now it is a value for him and he is very rarely late to things. For Merlin, being late is an affliction, an Albatros, but his daughter is pre-afflicted and does not have the punctuality gene. It skipped her, which is a struggle for Merlin and he gets a little bit keyed up. If you have to leave the house at 07:30am, then Apollo has already died at 07:31. You don’t get a second chance! Every day you show up at 07:39am you keep building your late-muscle. Where is the late crown?

John has been through a lot of different versions of the feeling that being late is disrespectful, but it really is! John has experienced it first, second and third hand, people have explained it to him, people have lectured him about it, and he has been made feel the quiet shame of it. John’s mom is never late to anything, but she also goes up at 4:30am every morning. You have to be made out of a totally different material to be able to do that and it seems like a magic power, while John’s secret power, staying up until 3:00am every day, does not seem like a secret power. Nobody says ”Wow, I wish I could do that?” John is like an early 1960s Marvel character: He is conflicted about his powers. 

+ Merlin going to the wrong concert (RL298)

One time in 1984 Merlin went to see Night Ranger instead of seeing REM. He had the choice, but he had limited dollars and he could either see Starship with Night Ranger or REM with the DBs. He was there for the second band but the ticket said Starship. It was the second time he saw Night Ranger after seeing them on grad night. Similarly there was a time he went to se AC/DC when he could have seen Yngwie Malmsteen. [http://www.myconcertarchive.com/en/event?id=3741 One time] Merlin’s girlfriend was there for Billy Squier, but Merlin was there for the opening act, a little band from LA called Ratt at the Lakeland Civic Center. 

+ Mike Squires playing guitar solos on Instagram (RL298)

Mike Squires has a little Instagram series where he is trying to learn the solos of 1980s metal tunes during breakfast before he records his first take over a backing track and puts it on Instagram. He is driving John crazy because he is doing Zappa and stuff and he is a very good guitar player. What Mike Squires is doing is obviously hard and he is trying really hard, but he is doing it out of love and pulls it off 89.9% of the time. At least he is doing something and he is not just photographing his coffee. 

+ Getting late night food (RL298)

Seattle prides itself of being a sophisticated Pacific Rim international city of business, but it is a cow town when it comes to food after 10pm. It might as well be some Christian Nebraska town that rolls up the carpets. Merlin interjects that even then you can still go to SuperAmerica. Back in Florida he had access to a SuperAmerica and a Little General, but now you kind of get that with 7-Eleven. Still, San Francisco is not New York. If you want some Pork Chow Mein at 2am, you are going to sit there with your dick in your hand. 

In Seattle, [https://www.thestranger.com/locations/205018/jade-garden Jade Garden] is open late on Fridays and Saturdays. This is not the green noodle place Merlin was thinking of, which called Shanghai Garden, Merlin’s wife is still talking about that. If they want to close for the day, they will just run a vacuum cleaner right under your table. John doesn’t know if the [https://www.thestranger.com/locations/30452/ho-ho-seafood Ho Ho Seafood restaurant] ever closes. Maybe they lost the key? You can go in there at 3am and they will serve you a teapot of beer after hours. 

+ Merlin's wife making coffee at 2am (RL298)

Merlin’s wife sometimes has trouble with sleep and she will get out of bed and make a coffee because as soon as she gets out of bed, she wants a coffee, even at 2am. Merlin knows the feeling though. Coffee is the new bacon! Things have been so memefied on the internet that you can’t even talk about them anymore, like you can’t talk about a [https://www.urbandictionary.com/define.php?term=Donkey%20Punch Donkey Punch]. 

+ Interstellar (RL298)

In July of 2018, Merlin watched Interstellar with his daughter. She liked it, because it is a children’s movie. The guy in the movie does have a Filson bag and a Carhartt jacket which instantly characterizes Matthew McConaughey. John does not prefer him, but Merlin teases John that John loves him. 

+ John writing an article for City Arts Magazine (RL298)

One of John’s superpowers is that he doesn’t mind being managed if you are clever about it. Don’t tell him the truth! If John has to be somewhere at 9am, tell him he has to be there at 8am. He doesn’t mind to show up to a thing 20 minutes late to find out he is 40 minutes early. 

Every single clock in John’s house is set to a different time, which would be very upsetting to Merlin who doesn’t even like if the Microwave and the Radio don’t agree. The closest clock to actual time in John house is still 10 minutes fast, but as soon as he looks at his phone, everything goes out of the window because the phone is set by the nuclears. Merlin would completely lose his mind if he couldn’t know what time it actually was. Times is imaginary just as money is imaginary!

John just wrote an article for the Back Page of City Arts Magazine in Seattle. The editor said that his draft was due on Wednesday, but she didn’t say when it would go to print and what would be the last minute when it was due. She gave him no additional information and no sense that she was open to any conversation about it. John wrote the article in the last minute at 3am the day before and he sent it in to her. He didn’t hear from her on Wednesday or Thursday, but on Friday she came back with some notes, like ”Do this more! Do this less! Your draft is due on Monday!" 

So on Sunday night John was editing his draft and after a couple of days she wanted him to tighten it up and send it in by Thursday. Had he known he had that much time he wouldn’t have done any of this work, but would just have done it on Wednesday night. On his Seattle Weekly column, John knew when they went to press and on Sunday night at 3am he was going to write his article. What the editor of City Arts Magazine got out of John was a better article, because her notes were good and John liked working for her. She worked John to give her a first, second and third draft, which John never does otherwise. He always just writes the thing and sends it in.

+ John’s dream assistant (RL298)

John always wanted an assistant who lied to him all the time and told him he needed to be somewhere an hour before. He wants an Alfred, someone who knows whom they are dealing with. It is one of the great things about being super-rich. Think about Dudley Moore //(from the movie [https://en.wikipedia.org/wiki/Arthur_(1981_film) Arthur])//: ”Alert the media!”, which is one of the greatest lines of all time. John wants his assistant to sarcastically not alert the media every single day.

John needs a gig-economy service, maybe there is an old Englishman who could come in and run John’s life for him and give him some useful dishonesty? The thing with the gig-economy is that you have to give them tasks and you have to give them star-ratings every day. John has a friend who is using TaskRabbit and John has encountered a number of rabbits: They are young, they have wonderful names, they are moving boxes around and they are good at building IKEA stuff. 

+ John wanting to be a TaskRabbit (RL298)

John would be great at being a TaskRabbit: Coming into somebody’s place and doing tasks! He would be 20 minutes late, but he would re-arrange their silverware drawer! That is his dream-job! Merlin has actually thought about this legitimately a lot. He used to be in the advice-giving racket and a lot of what he had to say was things he himself needed to hear, like ”I know you know this, but do you really know you know this? Are you thinking about this? Are you doing it?” 

Sometimes it takes a dingeling from San Francisco with a website saying it to you in a funny way to make you realize that this is a thing you need to think about more. You need to remember to remember! Merlin could give all kinds of advice that he would need 10 times more than the other person and he could deal with other people’s shit so much better than his own. If it is somebody else, Merlin has such a different outlook about it. 

If John went into business as a silverware-organizer at all, he could bring something to it that he might even find difficult to do for himself. The cobbler’s children have no shoes!

+ John’s and Merlin’s kids being sticklers to the rules (RL298)

John’s daughter does not share any of John's problems around getting things done on time: She is punctual and she follows the signs. If John is driving her around, she will be offended when he goes down a road with a sign saying ”Dead End” or ”Private Road”, which is an affront because: Come on! Merlin’s daughter is the same, she follows the letter of the law, she reminds other people when they are breaking the rules, and she has a lot of leadership skills, while Merlin sometimes sees things as suggestions. 

If something says you have to be 13 to do this, Merlin has the override-ability for deciding when she doesn’t have to be 13 to do something. She reads every bullet in the park about what is allowed and when it closes. You want to cut through the park with the Confederate Soldier Ghosts to go to Walgreens after sunset? Dad, the park is closed at sunset! She gets very anxious and flustered whenever Merlin is moving her toward doing something that she regards as any bending of any rule as stated or as she perceives it. They let her ride in the front seat of the car for the first time, which she was very excited about, but just in the neighborhood. She is 5 foot (150 cm) and 110 pounds (50 kg) right now.

Merlin is not allowed to do anything embarrassing in public. He must never be heard, he must never make a noise or a joke, he must never wear that particular pair of pants again, he is especially not allowed to sing her name to the tune of One Day More from Les Miserables. ”Elenor! Another day, another destiny!” The exact same thing is true for John’s little girl. At first she was embarrassed by dad stuff he was doing intentionally to be embarrassing. John is literally wearing plaid pants everywhere he goes and people recognize him and ask him if he is John Roderick.  

+ Private roads (RL298)

You won't encounter a private road when you are driving through a neighborhood, but you will find a private road when you are out in the sticks, driving near the cliff, the beach or the park. You could be reaching some kind of Population Cul-de-sac or getting into the rough a little bit. In California that could be getting off US1 and onto a private road owned by somebody with a fancy house on a cliff. 

Often times you go down a private road and it turns out it is just a long driveway and then you sheepishly put it in reverse and slowly creep out of there, while John’s daughter is mortified that the squirrels and the trees are watching them or that somebody up in the house heard crunching in the gravel and is peeking at them through their peep hole on their private road. At lot of times what you find down a private road is a community of 20 houses on the beach, an enclave, and they don’t want somebody coming down their road to use the beach. 

+ Waze directing people through small neighborhoods (RL298)

The application Waze can be very useful if you are on a highway that you have never been on. It will tell you that if you take this little thing over here, you can cut 15 minutes off your trip. Using it in an urban environment with which you are familiar is monkey balls! It sends you down the most crazy ways through people’s neighborhoods, which drives people crazy. John has been in a ride sharing service once where the driver was using Waze from LAX to the Chateau Marmot without ever going 2 blocks without making a turn. They are flying blind and they do not know those roads. 

The quality of the ride becomes low because of the stopping and the turning. When John gets a ride from the airport, he wants to sit in the back of a vehicle that is driven by somebody from Russia or Ukraine who is in his 50s or 60s, who has been through the shit, you can tell he has, he has got a little headset phone and if he is making a phone-call to someone, he is talking very quietly, but otherwise he is just in traffic, driving you to the place. If you were to be assaulted by a terrorist group, he would be able to handle it. If you were going to talk to him, you would be interested in what he had to say. 

Merlin showed John a sign that someone printed and laminated //(see show art for this episode, or a better picture [https://twitter.com/roderickon/status/1021482463435079680 in this tweet])// and hung in 3 different locations inside the automobile. The most important point was that the driver does not let passengers navigate him, a verb that Merlin has not seen used transitively in that way. Never show your vulnerabilities, because they will be capitalized upon! 

+ John taking a primitive trail with his daughter (RL298)

In July of 2018, John and his daughter took a walk along a steep trail. At one point there was a sign that said ”primitive trails”, which means that the trail had not been improved. John wanted to go down the trails that are dangerous and cliff-side and eroded, and that have lots of trees fallen over with their roots exposed. They went down some steep hills into the cliffs and as they got down to the beach, they found some beach-glass and turned over some dead crabs. 

On their way back they came to a Y-intersection. On one side were the primitive trails they had came down, and on the other side was a completely groomed access road. She looked at him like ”Are you kidding me? There was a road?” and they headed up the road. People who only went down and up that road don’t know anything, while John and his daughter did a loop and do now know the whole system. They were all alone out there and she wanted to walk up that hill backwards, which was an excellent plan, but is much harder than you might think. 

As she perceived the voices of others and there was a couple coming from way up the road, she wanted to turn around and walk forward. They walked past those people like normals, but as soon as they were out of sight, she wanted to walk backwards again. John has not taught her to be ashamed like this. Either she was always like this or she picked it up from her mom or from John’s mom. She saw other people and all of a sudden the thing that had been fun and interesting was now no longer cool. 

It is like an outtake from Peggy Sue Got Married where all of a sudden she wants to be dropped off to her dance around the block. John can remember a time when he didn’t want to look like a dork, but he strove to lose it. Merlin says that the ability to be a certain kind of weirdo is a privilege. Especially young women get this message that if they are a weirdo there are going to be consequences. 

Merlin talks about his book about anxiety called [https://www.amazon.co.uk/Stopping-Noise-Your-Head-Wilson/dp/0757319068 Stopping the Noise in Your Head] and how the authors recommends to face your anxiety instead of trying to escape it. 

+ John thinking he is fat (RL298)

John was a skinny kid until he went to live with his dad. From the time he was in 5th grade to the present, John has had a voice in his head telling him that he was fat and he has never not thought he was fat from the time he was in 5th grade. Sometimes he looks at pictures of himself along the way and wonders why he was so freaked out that he was fat, because he was just normal. The ”I am fat” message came in every time there was a girl John was interested in, every time there was a sport about to get played, any time there was someone there who was good at sports, and every time there was the suggestion that they would go swimming or they would change clothes. It has been and is still a constant friend. 

John has tried 1000 times to eradicate that voice by saying something else. In 1995 when he was living on a can of chili and 14 beers a day, he was not fat, but he remembers telling himself that he will not be able to be a successful musician because of how fat he is and nobody wants to watch a Rock show about a big fattie. He still imagines that other people only found him attractive for a heavy guy and that is a voice he does not want in his daughter’s head. One day she won't want to wear plaid pants anymore, but so far she has not figured out that there is a normal style and a crazy style. She has crazy style, but for the love of God: Getting some version of ”I am hideous” into her head would be such a boner!

John put on a pair of pants this morning that he bought on the Internet and the person who sold the pants did not tell the truth, because they were a different size than they were meant to be. They don’t fit, but rather than a) send them back or b) throw them away, he put them over a chair and thought that he might be able to wear them after he would lose 25 pounds. It would also require him to lose 4 inches of height and he is unlikely to do either of those things. Those are beautiful pants and they are purple, in fact! John has never sent anything back, but if it doesn’t work they go into the Goodwill and back into the pipeline. 

+ Failing or succeeding in life (RL298)

John knows that there is a point to life, but he hasn’t figured out what it is yet and he is definitely not succeeding at it. Many other people apparently do not think that there is a point to life beyond making it and having fun. He talked to a friend who said he was trying to live in Colorado for a while. It was great, they were skiing, going out, and living, but after a while he thought he was not doing anything, but it was all just in the category of play. This was their lives, this was how they were living! 

When you go on vacation you will meet the people who live there all year round and their job is to accommodate people who are there on vacation. You see them working to provide vacation experiences for other people and the difference is that they are bringing the drinks, but that is a huge difference! The key is: What is the point? If John’s job here in Seattle was to serve people who are coming to Seattle for vacation, which happens a lot, after a while he would feel that this was not the point for him. 

When John was seeing his psychiatrist earlier this year, the psychiatrist was trying to say ”Mindfulness! Get into now! There is nothing to solve! You don’t have to validate your existence! You are not doing it wrong!”, but John has a hard time accepting that at the core level. If John doesn’t think he is failing at life, and he is surely not succeeding at it, then all he is doing is ignoring that he is failing at life. To tell yourself you are succeeding is just to tell yourself you are pretty, which is fine if that works, but so much of John’s anxiety is tied to all the things he feels he should be doing in order to be succeeding. 

+ Feeling anxiety about moving (RL298)

John has been talking about trying to sell his house and buying a house in a different part of town and it has been causing him tremendous anxiety. It would require him to finish all these projects that he has been living in and amongst for the last 10 years, like putting in a walkway upfront. It plagues him! He does not have happiness! He has been thinking about this house he wanted to buy and how he was going to sell this house. Last night he was sitting around and asked himself why he thinks he deserves another house! It was that Welsh troll that tiptoes in on kitten feet. 

Buying another house might be a betrayal of all the suffering he still has to endure in his current house. John should still be punished by this house rather than move on to some new punishment in the new house, but his mom said it was okay to move and it doesn’t have to have all that weight attached to it. How can he unburden himself? It feels like he is wearing a huge old-fashioned wood-framed backpack with a goat on it and some buckets, like John is one of these knife-sharpeners from the 19th century. 

Merlin says that John doesn’t need to feel that way, but that would be predicated on the believe that you have the option of feeling a certain way. It is the depression-question again: Those feelings are not alien to John and being able to shrug them off would require him to not feel like he deserves them, which he //does// feel. A big part of John’s world view is that people let themselves off the hook too easily and a lot of people are in voluntary LaLaLand: You see the bad versions of it all the time, people saying ”I’m not hurting anyone or creating a path of destruction where I go! I’m great!” John doesn’t want to be one of those, so he holds himself accountable to things that he has nothing to do with, like Earthquakes. 

There is no sense in John's mind of what he would need to do differently or accomplish in order to feel that he has earned a change, and that is the problem in John’s logic: What is success for somebody like John and Merlin? Merlin is totally fine with lowering the bar on himself. John sees how ridiculous it is and how it looks from the outside. He is just a small little dot in the universe and it is not all hinging on him. Nothing is really hinging on him! If he could afford it, he could just play Playstation all night and the world would keep on spinning. All the politics and all the things we keep get exercised about, all the things that fill our world, none of it is anything! Money is fake, time is fake! 

John thinks about this all the time: He and Merlin spend their whole childhoods fully expecting a nuclear catastrophe and trying to plan for it somehow. Now that is all just a weird distant memory. They used to think that every single minute was pregnant with the possibility that this could be the end. Every time a car honked or a siren went off, this could be it! 

During the whole September of 2001 Merlin was all antsy about every plane he heard. That first week after 9/11 when no planes were in the sky was very strange. John was very surprised. If he had been planning 9/11, he wouldn’t have just done all that in New York, but he would have done a pan-America. With the time behind us, we would probably all have done 9/11 differently. We would have caused a lot more confusing disinformation that people would still be figuring out. You can scoff at the jet fuel / steel beam Dingelings, but knowing what we know about what even average intelligent people can be convinced of nowadays, we still got off lucky! Merlin is not saying that 9/11 wasn’t bad!