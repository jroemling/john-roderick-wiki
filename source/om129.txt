[[toc]]

+ Thinking about leaving everything behind (OM129)

K: Has there ever been a time in your life where you planned or even just fantasized about just walking away and starting over? Just getting on a Greyhound bus or walking out your front door and throwing a lit match behind you?
J: Do you mean: ”Has there been a time this morning between the time I woke up at 10:00 and now at 10:20?”

Imagining that he would leave everything behind and have a lengthy prison term or be forced into the Bush to fight for his survival has been a major feature of John’s identity ever since he was a teenager. Being personally challenged by being forced to leave everything behind plays a large role in his cosmology, but Ken assumes that it is not much of an option anymore. A few hundred years ago people could go to sea or to some unexplored land like the West or Australia, to Gold fields or Diamond mines, join the Army, and even in the 20th century people thought they could solve their problems by going to California, Hawaii or Alaska.

John has always contended that the progeny of the West descend from the people who couldn’t hack it back East and split. John’s people have been in Seattle since the 1880s because for whatever reason they were driven out of Kentucky. They couldn’t hack Europe, split to New England, couldn’t hack New England, went through the Cumberland Gap, couldn’t hack Ohio, and headed for Colorado. John’s mom couldn’t stay in Ohio and went to Seattle, met John’s dad, but neither of them could hack it in Seattle and they went to Alaska. 

//How John's parents met, see RL161 in [[[Parents]]]//.

You don’t leave because you are a failure, but because you are looking for something that the town doesn’t provide. John ended up back in Seattle because he needed more electricity. There is still undiscovered country deeper in Alaska, but it is surrounded by discovered country. Today this impulse turns into urban backsliding to Portland or Austin. It is magical thinking that if there is something wrong with your life that you can’t fix yourself, you can fix your surroundings and you just need a bus ticket.

In the late 1990s John walked from Amsterdam to Istanbul (see [[[The Big Walk]]]) all by himself in an attempt to activate that racial memory, trying to get to a place where he could discover what it was like to be a Magyar or a Celt, someone who came over from beyond the Volga and felt that there was something in that rhythmic plotting of the feet, the lack of connection to any one place, watching the sun come up in the morning and go down at night, that John hoped would be an antidote to modernity and to softness.

+ Anxiety in our age without a concrete reason (OM129)

A lot of our getaway plans today are time travel backwards instead of looking outward. Our modernity feels like a culmination of time, some fin de siècle, which is powered by a sense of exhaustion: We have exhausted geography and there are no fast tracks left to explore until the billionaires get us into space. There is also a sense of personal exhaustion because we are not cut out for adventure and maybe there is no adventure because of technology and whatever it has done to us.

We live in an age of free-floating anxiety. At least [https://en.wikipedia.org/wiki/The_Age_of_Anxiety Auden’s anxiety] was caused by the Germans invading the Sudetenland, which was a great reason to be anxious, especially if you lived in Czechoslovakia. The Cold War was a great period of anxiety and it was pretty clear what we were anxious about, but now there is just anxiety thick on the ground and people blame it on Trump or on Climate Change, but neither thing is adequate to explain the amount of anxiety we feel now.

+ Every Day Carry (OM129)

//see [[[Apocalypse]]] and [[[Keeping a small bag packed]]], RL133//

Some people today spend $400 on a hatchet, there is work-wear fashion, there are back-to-the-landers who get a chicken or a beehive, but they are not doing the hippie thing of buying some land in Central Oregon, but they are trying to get back to the land in their urban apartment after they get home from Amazon. They dress like somebody who worked in the boiler room of the USS Maine, but they are working in computers. There is this constant desire to return to something essential. 

Marie Kondo is an expression of that: What is my daily carry? What is my go-bag? The whole premise of the Every Day Carry movement is that you would have enough on your possession to survive if it all went to hell right now. People carry fishing line in their wallet or a little packet with fishing line, little bracelets made of parachute chord that they can use in a variety of ways, tiny gewgaws that are on their person. They have never caught a fish in their life, but they have that line just in case. It is part of the Zombie culture of people who get a lot of pleasure from watching shows about civilization coming to and end. It is them against a faceless army of Other.

There are middle aged people living in cities who have built their homes to be Redoubts that they can turn into a fortress, and at least dad is really excited about it. Maybe it increases the anxiety of the kids that dad is always walking around checking to make sure the pin in his anti-personnel mines is still there. There was a time when no matter how screwed-up your life was, how worried you were about things or how unhappy you were in general, your escape plan could be outward and forward, not just in an illusion you would tell yourself, but you could strike out into the unknown and literally join the French Foreign Legion. It was the proverbial Plan B or Plan X. Many things have to go wrong in your life but it will always be there as a backstop who will take anyone who can make it.

+ Soldier of Fortune magazine (OM129)

When John was young, he gobbled up Soldier of Fortune and magazines of that ilk. The Rhodesian war was just fascinating! Rhodesia became Zimbabwe in 1979 and John started reading Soldier of Fortune magazine in 1980-81. These magazines showed the veterans of the Rhodesian conflict with dramatic scars, super-light-blond hair and hawk noses. They were fundamentally evil-looking! Now there was a surplus of thousands of these guys running around and what were they doing? Were they all going to turn into Hans Grubers in Nakatomi Plaza? There were a lot of terrible things to get into in the early 1980s internationally.

+ John missing his silver ingot (OM129)

//see RL273 and OM111//

If John had a dollar for every time he was unjustly accused of a jewel theft! He should have ran away to North Africa, but he stood and faced the music. The statute of limitations has expired of most of his jewel thievery. He was looking for his ingot door stop the other day and couldn’t find it. Maybe it is misplaced or someone perpetrated a jewel heist on John and ran away to North Africa?

+ Random silly jokes (OM129)

They talk about using a pseudonym to join the French Foreign Legion. Ken’s most famous Mormon ancestor’s name was Zebedee, which is not a name we have anymore.

John has probably six or seven shards of the true cross in his house that he found on eBay. He also has the Ark of the Covenant somewhere around here. He only opens it when he needs to melt some foreign invaders or for defrosting a Hot Pocket.

John’s thoughts about Benjamin Franklin being responsible for the French Revolution, see RL58.

During training, soldiers of the French Foreign Legion had to stand in the sun until they have collected a glass full of sweat. Wasn’t that one of the scenes in the movie Seven?

John is still waiting to hear back if he is a Kentucky Colonel or not //(see RL267 and OM103)//.

Three Nights in Chad is actually a great title for an auto-biography. Ken thought John was going to say porno, but that kind of humor might fly on Jeopardy!, but this is an adult program.

Social Media is like the French Foreign Legion: You change your name, all the bad things you done before do not follow you, and you have to go to awful places and do awful things until your knuckles bleed.

+ John on USO tour (OM129)

//See [[[Shows and Events]]]//

In the beginning of 2015 John went on an Army Entertainment Office tour, similar to an USO tour. A podcast listener of one of John’s other shows was a Lieutenant Colonel, a drone pilot and a combat veteran in the Air Force and was charged with opening and managing various drone bases in Africa on behalf of the US Air Force, which were not widely publicized. There is no Air Force base in Africa, but there are these little drone bases in coordination with other governments. Often there is an existing airport and the Air Force takes one side of it and builds a little base out of tents and shipping containers. They don’t build Quonset Huts anymore like they used to. Half of Alaska was Quonset Huts when John was a kid. Ken used to go to church in a Quonset hut with stained-glass windows in South Korea when he was a kid. They were great, but tents are cheaper.

John went on a little tour and played for the troops in Niger, Ethiopia, aforementioned Djibouti, and Burkina Faso. Aforementioned Djibouti was a great teen dance club in Seattle in the 1980s where they played Shake Djibouti. There is a huge Navy base in Djibouti and China built one next door. The Chinese are building railroads in East Africa huckeldy buck, they are going to be the major power there. The Navy Base in Djibouti has an incredibly foreign component with Japanese, French and US Special Forces of every stripe. 

In Niger they played at a little base outside of Niamey that was home to an Air Force group, but also to some Special Forces, whom you could tell from a mile away because they had beards and baseball caps and were chewing tobacco. There was a contingent of French Foreign Legion soldiers and French commandos at this base because it was during the period of Boko Haram attacks in Nigeria. Different Islamic groups in Mali were contenting for an area in Northeastern Niger that was hard to govern. All these groups were milling around, people with fascinating mustaches in camouflage dungarees who had far-away looks in their eyes. 

They had set up a stage for John and showed a bunch of anti-aircraft search-lights on him. Those soldiers don’t get any other entertainment and they were delighted that people were there to entertain them. John played a full set of songs to maybe not rapturous but still a lot of applause. The French soldiers were all drinking while the American were not. It was rowdy! Niamey is a huge city and they were not that far outside of it, but far enough that it felt like they were in the bush. There were giraffes around and it was pretty interesting.

The French Foreign Legion soldiers definitely had a different and stronger vibe. The US Special Forces guys who were covered in chew spit still felt pretty disciplined, while the French guys felt very rogue by comparison. They sat with their feet up no the desk and they had some Je Ne Sais Quoi and some Sacré Bleu because they had some back alimony and had gotten out of wherever they were from.

+ Exclusive Dating Sites (OM129)

//This dating site was called The League, see RW104. John also mentioned it on OM82//

It might be a little Black Mirror, but John is imagining a world in which social media is cordoned so that not everyone can join the Twitter or the Facebook of the Futurelings time, like a Sky Lounge in an airport. Lots and lots of Silicon Valley disruptors are trying to popularize their somewhat exclusive social media even now, where you have to apply and pay $120 a year. One of John’s friends got him an account on some dating site that was only for professional hot ladies and for not-so-hot dudes with tech jobs and tons of money. John does neither have looks nor money.

He spent a few days on there and realized that this was just as bad as anywhere and the ”exclusivity” of it was both fake and also attracts the worst elements. It was fake because the people who were trying to popularize the app wanted as many people as they could and when they discovered that every rich person in the world didn’t immediately join their site, they just start lowering the standards while maintaining the premise that you are lucky to be here. ”Invite five friends! We will have to vet them, they might not get in!” You can smell the blood in the water!

+ Outro (OM129)

J: I have known you, Ken, for many years now and I have never seen you lick anything, not a single thing! 

K: Would you like to? 

J: No!