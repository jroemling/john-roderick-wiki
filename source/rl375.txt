This week, Merlin and John talk about:

* The Coronavirus pandemic is decreasing podcast listenership ([[[COVID]]])
* Merlin doesn’t like to have to do things ([[[Merlin Mann]]])
* John’s friend Chad, shellfish rights on the ocean in the Northwest ([[[Geography]]])
* John moving logs in his ravine ([[[New House]]])
* The Coronavirus lockdown situation ([[[COVID]]])
* Homeschooling your kids during the Coronavirus lockdown ([[[COVID]]])


**The Problem:** In this economy?, referring to a meme Merlin uses when they talk about how the Coronavirus pandemic influences podcast listener numbers.

The show title refers to tribes in the Northwest having the shellfish rights on the beaches and can take a certain percentage of the shellfish according to an old treaty that is otherwise completely unfair.

One of the challenges of the sequester is that there is a lot more demand for bandwidth around the house because everybody is on the computer and John might go into robot voice or tiny voice.

[[include inc:raw]]

+ The Coronavirus pandemic is decreasing podcast listenership (RL375)

John is not responsible for monitoring the listeners of [[[Roderick on the Line]]] because when they started this show the premise was that John didn’t know anything and if he were kept blissfully unaware it would make the thing run more smoothly. Merlin is not sure he knew that, but he also has forgotten more about this podcast than John has ever known. John doesn’t know anything about the production or putting-up of this show and he doesn't do any of the ad reads. It has been 15 years and they can always ask [https://twitter.com/capnmariam Capn Mariam].

For some of his other shows ([[[Omnibus]]] and [[[Friendly Fire]]]) John actually looks at the listenership numbers, and it seems that the lack of a daily commute has reduced podcast listening rather than being increased by the sequester. It is a turns out! People are home, taking baths, and eating chili. They don’t have time! ”In this economy?” It is the first time Merlin said that unironically and it feels different. It is a real mixed bag!

+ Merlin doesn’t like to have to do things (RL375)

Merlin doesn’t like doing things or having to be places. He has two impossible fantasies:

# Somebody will put him to sleep for one solid month and rebuild him and zap out all his tumors.
# He wants day-to-day clear-runway-clear-skies. He not only doesn’t want to have anything to do, but he wants to know that he doesn’t have anything to do, except for dealing with the catastrophic changes of news twice a day that utterly changes our entire Earth planet doings.

John claims that Merlin doesn’t have anything to do tomorrow, it would even be irresponsible for him to do anything, it is in violation of the law!

+ John’s friend Chad, shellfish rights on the ocean in the Northwest (RL375)

John’s friend Chad who is a booking agent of some renown is from Olympia in rural Washington, and people from Olympia will protest that, but they know it is true and what are they going to do? Chad moved to Seattle a long time ago, he is a big city guy and runs the biggest clubs in town, he runs every band in the world. One day he was in Saint-Tropez, waiting in line at a conch shop, and he got a tap on his shoulder from Paul McCartney, like: ”Hey Chad, what is going on?” //(see RW71)//. Chad is a collector like John, except he collected circus stuff and prison photos and stuff, and he is really into Pendleton stuff and old blankets, a classic Northwest junk store collector.

John went to an estate sale with him one time and there were two lumps of lead that were primitively shaped into faces and Chad had to have those for $50 each because he thought they are something, but now they sit on his shelf of treasures and he hasn’t been able to find anyone who can confirm that they are some important thing. His collection is way deeper and cooler than John. He has albums of polaroid photos taken in prison by gang members in the 1970s and early 1980s that have somehow landed on eBay. These are astonishing and John could study those books for hours!

Chad’s dream, growing up in Olympia as a poor kid, was to one day have a house on the ocean, just a humble Northwest oyster shack, definitely an old house and not a new one, so after he became a big shot he bought this house out on the ocean in Shelton, which is BFE (Bum Fuck Egypt = Out in the middle of nowhere). If you could find a geoduck (”gooey duck”) to throw at somebody out there you consider yourself rich out there.

John talked to him the other day and he has been out there for 2.5 weeks and it is his dream scenario and he is never leaving, he is building planter boxes, he got his lumping lead heads, he owns the shellfish rights for his beach so he can collect... which sounds crazy. In Washington state there are a few things about the shellfish beds: You can own a house on the water and own the shellfish that are under the ocean in front of it, but the state of Washington also sold the shellfish rights separately from houses, so you can own a house on the beach and some other dum dum can own the shellfish rights. The tribes also have a treaty right to shellfish across the whole game and they can claim a certain percentage of the shellfish, a treaty signed 100 years ago that in every other respect screwed them out of everything.

A geoduck is a clam that looks like a dick and are the same size, they are too big to fit in a shell unless she is a MILF clam. They are hugely popular because anything that looks like a dick is popular someplace, but they are not a very good clam and they are chewy. Merlin prefers the little neck, like a micro-penis clam. In John’s experience, the smaller the clam, the crispier and more delicious it is. That is true for everything, even for your peas. 

He has a dock that is grandfathered in and if it weren’t, you couldn’t just build a big old dock out from your house. The sea lions believe that the last 100 feet of his dock belong to them and every morning he wakes up with a bunch of sea lions on his dock. There is no better thing! Not seals, but sea lions, which are much bigger. Chad also has two dogs who look like sea lions and are the same size and every morning they run down the dock and have a moment where they try to push the sea lions back into the water and it. It is great out there!

Merlin thinks it is different from preppers because preppers shorted humanity and are saying that this whole thing is going to go tits-up and they want to be out there with boxes of ammunition and Jim Bakker buckets of corn. Like people who bought too much insurance, they will be good and happy when something goes wrong because they feel like their short bet really paid off. They want it so bad! Except among the super-wealthy that is a pretty right-wing type thing, but among the super-wealthy it can be a Elon Musk libertarian nut-job type situation.

Chad is just like Merlin and all he wants is not to do anything. He got himself a little house where literally no-one is ever going to ”just stop by”, except for his next-door neighbor Dr. Falcon who used to work on some kind of missile program, and they fly kites together, but otherwise he doesn’t have to book any concerts or meet Paul McCartney, he doesn’t have to do anything, he just wakes up, makes some coffee and looks out at the sea lions and everything else is optional! If the EMP comes and all the information goes away, there is nothing more enduring than living by the sea. There is a lot of plentitude in the sea. It is not an economy of scarcity, but of abundance.

In the Northwest you just stick a shovel in the mud and pull out some clams and harness up a sea lion and swim with the fishes, but not like Luca Brasi //(reference to The Godfather)//, you could just live on ferns along if you had enough butter. You could get sea lion butter if you go out and milk them, they have walrus titties. If his dogs wouldn’t chase the sea lions away every morning Chad would have tamed them by now enough that he could get close enough to see the whites in their eyes, but he doesn’t want to be Aquaman.

Hall of Justice, some of those clubs are closing down. [https://en.wikipedia.org/wiki/Slim%27s Slim’s] just closed (on March 18th of 2020) and is out of business. 

Chad probably has been waiting for this (the lockdown after the coronavirus outbreak of 2020) his whole life and John is trying to decide if he also has been waiting for this.

+ John moving logs in his ravine (RL375)

//See [[[New House]]]/

+ The Coronavirus lockdown situation (RL375)

Seattle is way ahead of the curve, but there have been pictures of Green Lake, the local lake to job around, and there have been all these people out there, thinking this is vacation, thinking they got a couple of days off work. All the other countries are showing the US that the biggest transmission is inside of family units, but when you cross over from this family to that family it is very transmittable. If you keep exposure limited to your little group, you are mostly going to be fine. It is so obvious! Nobody is having fun with this, but you got to do this! Stay the fuck away from other people who aren’t your family!

John’s mom is always running some fucking errant. She showed up at John’s house, rang the doorbell, and then ran down the path like the guy in the neck brace in Raising Arizona. She stopped 15 feet down the path, John was standing behind the screen door, and they had a 25 minute conversation. John found it hilarious that she is out there running errants while making sure she is 20 feet from anybody, which is what she prefers and what she has been waiting for her whole life.

When John sees someone come the other way who doesn’t want to pass him, he has no problem stepping out into the road and going around people, he will step aside and wait on the other side on a park bench, he will jump behind a log like in Lord of the Rings when the Nazgûl are coming, but half the time it looks like John has a court order to stay away from kids, it looks a little sketchy, especially when he is in dirty jeans with a black watch cap pulled down over his eyes or a hat that says ”Précisément”, but now when John does it everybody is grateful.

There were so many times in John’s life where if all of a sudden a sequester had come down the other people in the house would slowly have turned their heads to look at him and gone: ”You! Out!” and John would have been standing there with a sleeping-bag half-stuffed into a stuff sack, in pajama bottoms, and all doors would have been closed for him, his hair all messed up, still sleep in his eyes, and the theme from The Odd Couple is playing, having a half-empty half-rack of Stroh’s //(beer)//.

Liquor stores and weed stores are considered essential businesses during the lockdown. They are in the top-2 most essential things for 60% of Americans and 70% of non-Muslim worldwide, but if you are struggling with alcohol dependency and are trying to get sober, this is going to be extra hard because there are no meetings and because your brain is looking for any reason why this particular moment is different, like ”It is New Year’s Eve!” or ”My wife just left me!” or ”I got a promotion at work!” and today is a day you should drink. John does the same thing, sitting at home, thinking: ”Another Banana Split is the least I should be doing as part of this sequester! It can’t last forever!”, but if he was 4 months sober, his brain would tell him he should be drinking. John encourages everybody to stay committed and call the people on your list of people you can call.

Merlin is eating way less right now and has lost 5 pounds.

+ Homeschooling your kids during the Coronavirus lockdown (RL375)

Merlin’s daughter got her final grades in March. She got 4s in everything.

Merlin would give everybody the next two weeks off on a paid leave. His wife works for a university that is fighting this and she has shit to do and she is working from home really hard. We should all get a little time off before we decide to get our little house on the prairie and before we get back to reading the great books and doing our maths. Merlin’s family made chocolate chip cookies last night and didn’t apologize to anybody. Some people need a certain level of that kind of stimulation, but Merlin’s kid still does general homework, her coding projects and she practices ukulele, she does craft projects, and she finds ways to stay busy because she enjoys that. She also plays Animal Crossing. She is not bouncing off the walls, but she is also not a 3-year old boy.

Yesterday at dinner John’s daughter had a mouthful of scabetti [sic] and he told her that next day was Monday and her mother will be back at work, making phone calls with her executive teams, while John is going to go downstairs to talk to Merlin and then move some logs and he asked her what she was going to do. She had no answer and he suggested to make a schedule to get on some math.

Thank God for the Internet! Somebody tweeted that if the economy went down, maybe they would shut down water and electricity, but that seemed a bit far fetched. Somebody slid into John’s DMs and asked why we don’t take $500 billion from the rich and redistributed it, but that would require Congress to vote on it, otherwise we would no longer have an American form of government.

John downloaded some math homework this morning, his daughter woke up on her own at 9am, made a little breakdast [sic], they sat down at the table, and John said that while he is going to talk to Doctor Merlin she will work on her math and then he will come up to work on it together and at 11:30-12:00 they will move on to science, take a little lunch break, go out and look at some crabs. But John doesn’t know how long he will be able to keep this up because one of these days he is going to get tired of it and tell her to just go play.

Merlin’s family does not have a close family except their in-laws in Gold Country, about 3 hours away. Across the street is John’s daughter’s nominal best friend and they run across the street and ring the doorbell on each other all the time, but John is not sure how stable that friendship is from both sides because they are 9-year old girls. Her mom and dad are pretty introverted and keep to themselves all the time, except mom is a lawyer and still goes to work in a law office and her older sister is a Furry, a starting-college-age cosplayer, some kind of blue cat, but she runs the gamut and also wears ren-fair (renaissance fair) stuff.

She has a rinky boyfriend who shoes up every once in a while in a Honda Civic. Neither John nor her parents want to combine those two families. They have a plug-in air freshener that burns essential oils and every person that goes over there comes back smelling like whatever that is and they made some cookies the other day that not only smelled but also tasted like air freshener.

John’s daughter sits in here with big Keane painting crying eyes, saying that her best friend is right on the other side of the glass and her daddy said that she can play, but John’s daughter can’t play, but he said: ”Go to your math homework!” That is not a tenable situation that can last forever, but it means John is inviting a Furry boyfriend with a leather hat that smells like lavender into his home and he risks ending up in a clown’s basement, which you don’t want, especially not a clown with COVID-19 coronavirus.

The weak part of John is his lungs and his eyes. He gets sick all the time and catches every little bug and every time he goes on vacation he spends the first four days lying in bed with a fever. He doesn’t know why God was like: ”I will make all of you super-big, except your lungs are going to be this vector for everything that comes along!” and John does not want to get this virus. Everybody around him, including his mom, would get it and just shrug it off, but he is going to get sick and he does not want that and is personally scared of it. He doesn’t want a Furry’s boyfriend licking his daughter’s best friend and sending her out into the street. His leather top hat is surely half coronavirus.

Merlin and his daughter watched the first half of the movie Amadeus last night. She drives him fucking crazy because for example chocolate and Toy Story are the two things that she swore at one point that she would never try because she knew that she wouldn’t like them and now chocolate is the single-largest staple of her diet. He had the feeling she would like Watchmen and she would like Amadeus and he begged her to give it a shot. Merlin loves that movie so much, it is such a tremendous movie. The Neville Marriner music from that movie was very popular afterwards because so many people heard Mozart in context for the first time and didn’t need a sherpa or a public radio station to tell them that this music is transcendent. He and Bach are on another level.

Also, ”(Leopoldo) Galtieri //(former president of Argentina) took the Union Jack and Thatcher with her tears took a cruiser with all hands” //(lyrics of Get Your Filthy Hands Off My Desert by Pink Floyd)// That was an intentionally garbled quote from the final Pink Floyd album The Final Cut, which was a Roger Waters solo album, and that was a song referring to the Falklands War and John was mis-transposing Galtieri and Consiglieri //(the actor’s name is Salieri)//. Who is the Italian guy? Gabigary? Gabi [[[Gary’s Van]]]? This morning ended with Merlin going downhill on a Segway wearing an N-95 mask with Mozart’s 40th symphony playing over the speaker this morning.

This is an opportunity to fill their kids’ brains with the sciences and maths, but it is also a nice opportunity to start little projects for now. This is going to get worse, so have a fun project now! Make some cookies!