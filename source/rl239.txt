This week, Merlin and Johan talk about

* John's earlier visit at his uncle in Palm Springs ([[[Stories]]])
* Holding court during family gatherings ([[[Stories]]])
* John's recent visit at the former Weyerhaeuser headquarter ([[[Currents]]])
* John meeting the CFO of Napster at a charity dinner ([[[Shows and Events]]])
* John's early days of puberty and the quest for his sexuality ([[[Personal development]]])
* The Pros and Cons of Hitchhiking ([[[Factoids]]])
* How the record "Pros and Cons of hitchhiking" with the naked Linzi Drew on the cover informed the beauty ideal of the time ([[[Stories]]])
* The "era of input" between the generation without Internet and the generation who doesn't know anything else ([[[Internet and Social Media]]])

**The Problem:** There's no SKU.
The people in the guitar factory want to make your dream guitar, but they can’t because there is no SKU for it.

The show title refers to John coming into the puberty ages, he called it "my agency".

John and Merlin are bantering about founding a pyramid scheme called “PeerMyds”.