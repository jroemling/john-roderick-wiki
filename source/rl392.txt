This week, Merlin and John talk about:

* Ashley Feinberg doing detective work on public figures ([[[Currents]]])
* Merlin wanting to name his child Tyler ([[[Merlin Mann]]])
* John working as a landscaper in High School ([[[Employment History]]])
* Transitioning from a bad situation to a better one, bad judgement ([[[Humanities]]])
* John being able to tell people apart who are in alcohol recovery ([[[Drugs]]])
* An abandoned house in John's neighborhood getting fixed up ([[[Normandy Park]]])
* John having workers with chainsaws outside ([[[Normandy Park]]])
* John having hired workers with too small chainsaws ([[[Normandy Park]]])
* John getting wood chips for free ([[[Normandy Park]]])
* Tree workers throwing a tree on a swimming pool ([[[Normandy Park]]])

**The Problem:** John only had rolls of nickels, referring to John having landscapers cut down some trees at his old house and having promised them he could pay them in cash, but then it turned out the cash he had were rolls of nickels and dimes.

The show title refers to one of the workmen with the chainsaw near John’s house who looked like he could have been a professional wakeboarder 25 years ago and who looked like the oldest possible Tyler Higham.

Merlin accidentally left a clip in the show where he said: ”I have to take a shit so...” in the middle of a story.

People were chopping down some trees in the neighborhood while John was recording.

Merlin remarks that [https://www.dailymail.co.uk/news/article-8572941/Guard-honor-collapsed-escorts-John-Lewiss-casket-leaves-Capitol.html John Lewis’ hearse] was so fucking bad-ass, it was the coolest hearse! It has the performance characteristics of a Ghostbusters car or a Herman Munster vehicle, but it looked totally modern.

[[include inc:raw]]

+ Ashley Feinberg doing detective work on public figures (RL392)

One of Merlin’s favorite writers is [https://twitter.com/ashleyfeinberg Ashley Feinberg] who does the wildest detective work on public figures. She found James Comey’s secret twitter //(see [https://gizmodo.com/this-is-almost-certainly-james-comey-s-twitter-account-1793843641 this article])//, she found Sebastian Gorka’s Amazon wishlist //(see [https://www.wired.com/story/trump-world-amazon-wish-lists/ here])// and she has a special obsession with the president’s son Donald Trump Jr. who on his Instagram had posted a photograph of an old Zippo lighter, some rifle casings and a recently lit cigar, with the caption: ”Getting the weekend started right!!!” //(see [https://mobile.twitter.com/ashleyfeinberg/status/1005461184764669952 here])// and she uttered a phrase that Merlin still thinks about twice a week: ”Please meet the most going-through-a-divorce man alive”. He can sometimes identify somebody who is definitely a going-through-a-divorce man. 

+ Merlin wanting to name his child Tyler (RL392)

The notional name for Merlin’s child before his wife gave birth was Tyler, which it is the most generic American name. John knew a couple of Tylers, one of them at his university. Merlin even knows some very good high quality ones, and ones who do podcasts. You have to watch out for Toby, though, but it is a great name for a girl. Siracusa wanted to name his daughter October from the [https://en.wikipedia.org/wiki/October_(U2_album) U2 album] with Toby being the canonical nickname, and that is adorable. It could be the name of the leader of an all-girl motorcycle gang.

+ Transitioning from a bad situation to a better one, bad judgement (RL392)

John was a much better student when he came back to college after years in the field because he had realized he was paying for each one of those classes out of his own pocket, and when he finally got his Bachelor’s degree at the age of 47 he felt like he was probably one of the better undergraduates for the last 30 of those years.

When we are trying to transition from a bad situation to a better one, we all feel like we want to bring some things with us from the bad times. We want to be done with the abusive relationship, the bankruptcy or the alcoholism, but we definitely want our girlfriend to come with us or we don’t want to get rid of the business because that is our nest-egg. We are done being an alcoholic, but we don’t want entirely 100% never be able to have a glass of wine at dinner again.

When you finally quit it is like being born again and you have made enough bad judgement calls in the past that you need to acknowledge that your judgement is bad, and that you are not the one to stand here and say what things your are going to pick and choose from the old life to bring to the new one, but you just have to make a clean break. It is the hardest thing to say that the problem was not the booze or the girlfriend, but that you were making bad decisions.