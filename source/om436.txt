This week, Ken and John talk about:
[[toc]]

+ What does and doesn’t pass on Jeopardy! (OM436)

Is there a code on Jeopardy of what you can say and do and how many times you can appear on camera with your fly undone? It doesn’t really come up much on a quiz show, they are not pushing the envelope, trying to see how much of Dennis Franz’s butt they can show on any given episode.

How often will they have someone do over a line without the swears? They had contestants drop some F-bombs when they get excited, muttering under their breath when they realize they got something wrong, or when they see a Daily Double that they don’t know, and it is usually pretty easy to drop the mic on that person, like what they do on Omnibus when John swears or belches or when his chair squeaks or he makes mouth sounds.

There is an understood and internalized sense of what passes on television, even in the game material. People might be eating dinner and you don’t want gristly depictions of the ravages of Cholera like they have on The Omnibus. It is also a thing in crossword puzzles where Will Shortz didn’t want enema or cancer to appear as clues because he doesn’t want you thinking about enemas when you are doing his crosswords, and Ken doesn’t want you to think about enemas while consuming any of his art, but it is too late!

A game show has an extra level of government scrutiny over the fairness of the game. The show has to be exactly fair to all the contestants and transparent to the audience about how the game is being played. If anything were not above board there could be a congressional investigation again, like in the 1950s when the quiz shows were giving out the answers  to the contestants.

There are independent auditors on scene all the way from the morning editorial meeting to the end of the taping of the 5th show, like the guy from the EPA from Ghostbusters, a government guy in a 3-piece suit. They are external lawyers from auditor-type consultancy firms that exist only so that an outside agency can oversee compliance with regulatory stuff.

Today there is more reign for humor on game shows and maybe even a slightly edgier contemporary type of humor. Category titles will often be current memes of the day. Ken’s famous answer when he said ”A hoe” when the answer was ”A rake” made it onto TV and became a beloved Internet meme, but that was unintentional. Ken asked them a decade later and they didn’t really have any particular memory of it, it wasn’t an epical thing for them because they don’t get send the YouTube clip once a week like Ken does. They swore it wasn’t a trap. They are trying to write and research and fact-check 300-400 clues every single taping day, they are not planting secret agendas.

In the 1960s the show was very straight-forward, there was not a lot of humor or any kind of cleverness or boundary-pushing or topicality in any of the clues. 

+ Ken watching a lot of movies, how his dogs react to movies on screen (OM436)

Ken has a home cinema and his family watches movies together and Ken must have watched more movies than John: Old, new, American, foreign, streaming, DVD, they watch it all! They watch about a movie a day, it is a ritual. If nothing else the dogs like it because they get hang out on the couch with them.

There is a cat in John’s family at his daughter's mother's house and any time something comes on TV that has any kind of boom sound she goes claws-in-the-ceiling. Ken was watching Power of the Dog with some distant thunder, and the dog immediately headed under the chair, they are just not wired for that and don’t recognize things on the screen.

If Ken’s parents’ dog will see a horse, a bird, or anything they recognize, it will just yelp at the screen. Ken’s previous dog who passed away, if a horse left part of the frame it would go around the corner and see where it would come out, and when it didn’t emerge from the side of the screen it would be very unhappy and confused.

+ Watching several movies in a row feeling wrong (OM436)

Ken can’t sit and watch 4 movies in a day, but he can watch 4 hours of episodic television in a row. He might have watched 4 hours of a movie, but it was Return of the King. As a younger man he would go to a movie theater and watch something and then go to a different screen and watch something else, but if he does that now he feels like he is wasting God’s potential for him if he just sits and watches all the Matrix movies in a row. John would sit and stack rusty needles all afternoon and feel like he has done a Yeoman’s Job, but to watch two movies in a row feels like: ”Why not be a heroin addict?”

John’s daughter’s mother worked at the Landmark Harvard Exit //(cinema)//when she was in college and at the Landmark Metro and she would watch 5 movies in a day, it was like reading a novel all day. When John asks how she can do this she will say that he will read a novel all day, but of course you do that, it is what God intended!

Ken does feel that distinction with video games where generationally there is something uplifting with reading a book for 6 hours and something soul-sucking with shooting goblins for 6 hours, but he probably couldn’t defend it if he had to, and he feels sinful reading during the day, too.