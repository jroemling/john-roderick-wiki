This week, Ken and John talk about:
[[toc]]

+ Fashion from the previous generation (OM316)

Ken used to look at his parents’ old elementary school readers with Dick and Jane //(two characters created by Zerna Sharp for a series of basal readers written by William S. Gray to teach children to read)// in their freshly ironed clothes having freshly-scrubbed adventures. Even though it was only 20 years old it seemed like another planet and it was hilarious. ”Look, they have chums!”

John still wants shag carpet and waterbeds! You watch those things become so unfashionable where they are examples of the worst possible taste, but then they are going through a period where they are so bad that they are ironic and when the irony wears off they just go back to being terrible. The thing about mid-century everything, just like the term Victoriana, is that it encompasses an entire breath from the crappiest, cheapest pre-fabricated stuff to the nicest most-expensive custom furniture. In the same way Queen Victoria was responsible for a period of amazing architecture, but also for the Boer Wars.

A lot gets lumped into this because ”mid-century” is a descriptor of ”everything build between 1945 and 1968”. Ken is a bit younger than John and thinks that most 1970s signifiers are laughable, like watching Barney Miller re-runs and seeing their wide ties and everything. This week he was watching Network //(a 1962 Variety Show)//, which he hadn’t looked at in a long time. He was also watching Out of Sight, the Steven Soderbergh movie, and there is a famous scene when George Clooney is in a car trunk with J-Lo and he misquotes the Network scene, like: Remember when Peter Finch goes up there and says: ”I’m as mad as hell and I’m not going to take it anymore!” //(see [https://www.youtube.com/watch?v=ZwMVMbmQBug here])//

This movie is set in the most laughable fashion of a time just before his. Every generation hates the fashions from right before them. It is recent enough for you to know that it existed, but also to know that it is the opposite of what is cool now. Now we know that the 1970s fashion is great! It has emerged and the more outrageous the better, except for the hair, which was objectively terrible and for the next 20 years it was blinding us to how good the fashion was. A lot of it is sheer volume of hair, volume of side burns, clean-shaven men with piles of unruly hair cascading everywhere over their shirt collar, and there is just no excuse for it.

Ken thinks that the giant collars and the polyester look amazing. He is not going to wear it because it would look like a party dress, but if he saw someone in that he would say: ”What an amazing look! Good for you!” It is one thing to wear that today and be singled out in a sea of conformity and it was another thing back then when everybody was dressing like that, where it was a race to the most ridiculous plaid pants.

+ Mid-century architecture (OM316)

++* Mid-century architecture being torn down without thinking

A lot of mid-century architecture has been destroyed or lost. During the post-war period before the freeways were built Aurora Avenue in Seattle was the main road in and out of town. It was where all the new motels were built, first the stream-line modern and then the mid-century GOO-gee architecture, and the space-age tiki influenced architecture of that time. Those motels were the coolest thing in the 1950s and 1960s, but by the 1970s they had fallen into disrepair and those streets became centers for prostitution and drug-dealing, they all turned into motel-rooms by the hour. Every American city has a strip like this.

Just during the time since John lived in Seattle as a grown-up he watched those motels get torn down one-by-one and replaced either by cookie-cutter box apartments or just by vacant lots, and what was once a real parade of vernacular architecture is now almost completely gone. Only 15 years ago you could probably buy a motel on Aurora for $200.000, but now they are gone completely and the ones that do remain are not even the good ones and we wonder how we could have allowed this to happen.

Similarly, on International Blvd outside the airport in SeaTac they have just torn down all of the old motels, the Green River Killer Motels, but maybe it is not so bad that they are gone.

++* John's mid-century modern house

John recently bought a house that was built in 1953, which was not a period he was interested in when he grew up. In the 1970s all that 1955 stuff was exactly in the sweet-spot of begin old, dusty and not kept up. John's house is one of few that is left intact. Like when you see somebody in a funky 1970s outfit, part of what makes it look fantastic is that it survived and it is rare. Mid-century architecture at its loftiest was accompanied by a whole philosophy of living.

Those were expensive homes to build! You have to be a certain amount of rich to live in austere conditions and the principle of mid-century architecture was that it was without adornment, without a lot of gewgaws, but it was streamlined and simplified. A house was a machine for living in and it simplified your mind that you didn’t have to look at so many guilded (?) mirrors. Don’t dress it up, just leave the ultimate Bauhaus exposure of all the conduits!

If you can agree that both things are equally beautiful, then what you want is the house that has exactly the things you want in a house and none of the extra corners or ornaments. At its worst you can take that mentality and use it to justify building completely uninspired cheap square mass-produced buildings because complexity costs money.

++* Mid-century architecture being expensive to build

Truly great mid-century architecture is deceptively more expensive to build because a wall of glass is much more expensive than a wall of wood. Each window is 3 times the cost of wood and stucco by square foot. Those houses seem like they are hardly there, they are made just of gossamer, and they are the most expensive. The workmanship has to be better as well because you can’t hide being a millimeter off. It must be impossible to insulate those buildings because the ceilings are lofted and the roofs are flat, there are not attics, no crawl-space.

There is no place to hide the electrical that you in a typical box-house just throw into the walls. Where does Philip Johnson even pee in his glass box? Where is his water heater? Where is the breaker box? You have to build all that stuff so much more sensitively in truly great mid-century architecture. John learned this from his friend Ben King who is an architect who has studied with the masters and he talks about how difficult it is if the roof of your house is only 6 inches deep to make a house that isn’t cold and drafty or baking-hot in the summer. It is an industrial design, too.

We have gotten more picky about that stuff as time has gone on. We expect to have incredible control over our comfort in a way that the people of the 1950s would have expected drafts. What we settle for is vinyl double- or triple-pane windows that look terrible and are hard to interact with, but they keep the heat in. We have central air, we have lights on dimmers, we have all-mod-cons that give us the illusion of luxury because we do have control of our environment, but there is nothing nice about a typically constructed modern house. The luxury is stapled on and no thought is put into the design. It is just about square footage!

++* The idea behind mid-century modern architecture

The wonderful mid-century homes are actually surprisingly small in their dimensions because you did not want to lumber around an oversized house, sitting at your vanity while mom and dad each have their offices, but those houses were meant to be lived in with a spirit of austerity. If you talk about architecture, art, design, or fashion with any kind of philosophical underpinning you get very quickly to the idea that your environment and the aesthetics of the things around you are going to have an effect on your virtue or lack thereof, that the house is going to inspire you to think more clearly and that the style is going to pear away the confusion and clutter in your mind and the austerity of the setting will bring peace.

Ken is happier in a room that looks good instead of one where he is constantly troubled by: ”I never liked that shelf, I liked where that window is!”, but he is not sure if it is actually inner peace or if it is the self-congratulation of having gotten a room to your specification and that is what winners do.

++* How it all started

Buying a mid-century house is consumerism at its finest. To buy a house that only has one perfect thing on every shelf is to still be convinced that there is a perfect thing, but also: A house that is made out of windows is hopefully not situated over a Freeway on-ramp, while on the other side those windows are inviting in your beautifully landscaped garden as well. This style was translated into the mid-economic strata and then became wide-spread during a period in America of tremendous growth: Suburbs, people coming back from the war, a baby boom, people wanting to buy a new house, and lots of new construction that can’t all be high quality.

As it started to filter down you still got the austerity, but it became a by-word for cheap and square. It is much harder to build something with a curve than with a square and if your style is square, what a boon to builders and property owners to build a square box with square windows and square stairs and square lights. It all packs nicely in a box and in the back of a truck. It grew by the early/mid-1950s, especially in major cities that were experiencing rapid growth like Los Angeles and San Francisco, and the suburbs of New York. 

The vernacular architecture that was native to Los Angeles was the bungalow and a lot of Spanish mission stuff, but also just the classic turn-of-the-last-century bungalow: Small, hip-roofed, with two rooms upstairs and two bedrooms downstairs, pretty humble neighborhoods of those little houses. With the rise of Hollywood and post-war Los Angeles being just boom times, and in an attempt to bring density into those neighborhoods there was a lot of tearing-down of two-bedroom bungalows and replacing them with apartment buildings, which made a real difference to the income of a property owner.

++* Connection to car and roads being available to people

This was also the time when that whole world of Highways and American cars came to fruition. They had been building highways across America for 50 years at that point, but it finally all clicked and you could drive from Boston to San Francisco in one stretch of uninterrupted road. You get on Highway 40 and go all the way across the country. The 1930s were the dawn of that realization: The roads were being connected and the cars were capable of that kind of travel, but it was also the great depression and it wasn’t until after the war that people had money, brand-new cars, and all these wonderful roads.

Part of the mid-century vernacular was targeted at the people in cars that were newly making their way around not only the city or state, but the whole country. People were going on trips just to be on trips and to be in cars! The vernacular of mid-century architecture revolved around cars and car access that took the form of a design style that predominated in businesses that catered to cars: Gas stations, motels, coffee shops, donut houses. These buildings took shapes partly as advertising signs, which you can still see many places in Los Angeles.

Some of that is because there was so much of it. Los Angeles grew rapidly during that time and cars were a big part of it. Only recently have people started to appreciate this architecture as being significant enough to save, and a lot of the great examples of it were torn down just in the last 15 years by people who wanted to replace it with something stupid, as is always the case. Just when you started to realize that a thing is beautiful, that is when it is at its greatest threat. Maybe the need of those places for movie and TV production has moved the needle a bit.

+ John being a guest at Ken’s GOAT tournament with his friend Jessie (OM316)

John recently went to Los Angeles to watch Ken's GOAT triumph. He was there with his friend Jessie and after the show they both went on a long walk through central Los Angeles, headed to Katz’s Deli, a place with GOO-gee architecture. They were walking block after block through West Hollywood past dingbat apartment after dingbat apartment and they never saw a front door. The lot-line construction doesn’t even leave room for a palm tree! John couldn’t come up with a thing that was more alienating either for pedestrians or for the people who live there themselves.

Back in the days when all of the pot dealers in Seattle lived in these buildings John had spent enough time in them to realize that they are not inviting, but they are just places to habitate. The premise of the movie Slums of Beverly Hills is that although her father was down and out he kept moving from dingbat apartment to dingbat apartment within the Beverly Hills school district in order to keep that address. It exemplified the tone and timbre of living in those dingbats.

+ Hand-blown Japanese fishing floats (OM316)

//John was talking about those floats in RW197 on September 12th, meaning this episode has been recorded exactly 3 months before it was released.//

Somebody sent them hand-blown Japanese fishing floats to their PO Box that used to wash up at the beaches in the Pacific Northwest a lot. They were used by Japanese fishermen to keep their nets afloat before plastic. Stuff would get caught in storms and then these glass bowls would float all across the Pacific Ocean. These seemed to be found in one in every 5 homes, usually contained in some kind of Macramé holder, meant to imitate the woven net they would have supported in the ocean.