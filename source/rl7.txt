This week, Merlin and John talk about:
* Merlin is sick ([[[Merlin Mann]]])
* Singing the national anthem ([[[Music]]])
* John feuding with half the world without them knowing ([[[Internet and Social Media]]])
* People in Canada ([[[Geography]]])
* Ethnicity of people working in different restaurants ([[[Geography]]])
* German sex tourists in Romania ([[[Geography]]])
* Elephant 6 ([[[Music]]])
* Merlin running out of index cards ([[[Merlin Mann]]])
* John learning about social media and the Internet ([[[Internet and Social Media]]])
* John going further out from Wikipedia ([[[Internet and Social Media]]])
* John’s article in Seattle Weekly ([[[Internet and Social Media]]])
* Bust, Grand Royal and Dynamite Magazines ([[[Factoids]]])
* John educating young boys ([[[Dreams and Fantasies]]])
* Critiquing the continuity and realism of movies ([[[Movies]]])
* Bill Murray movies shot on cocaine ([[[Movies]]])
* Letters of Note ([[[Factoids]]])
* John going back from Gore-Tex to wool ([[[Style]]])
* John reflecting on his career ([[[Career]]])
* Going into a cave and turning into a superhero ([[[Dreams and Fantasies]]])
* John walking around his perimeter in a bathrobe with a sword ([[[Sword and Bathrobe]]])

**The problems:** 
* Sidling up to German Sex Tourists °; 
* Elephant 6 bands decamping to a new porch° ; 
* more on John’s uncontrollable steaming °; 
* almost closing the thread on the Bruce Vilanch problem °; 
* FDA’s daily requirement of Femineseum °; 
* why John treasures his collection of Braille Playboys °; 
* pitching the pilot for DecencyBusters °; 
* a pledge of index cards to help deflect John’s photons °; 
* the inexcusable lack of a decent Grand Guignol magazine °; 
* the long menarche that preceded our heavy internet period °; 
* John’s studied reluctance to buying young boys °; 
* Merlin’s reflections on accepting a strong man’s syllabus °; 
* why so few teens today offer to make candy penis bang bang °; 
* grave concern for the Teutonic hitting-and-poo thing °; 
* why you never fuck with Leonard Bernstein °; 
* Merlin’s culpability for Florida’s many orphan towel-babies °; 
* how Harold Ramis’ heart broke and broke; 
* why John’s compound may be neither decadent nor depraved °; 
* chronicling our mass exodus from wool °; 
* knowing when your sword deserves its own bathrobe °; 
* strategies for rebooting John’s complex legacy °; 
* the spelling error that created a frottage industry °; 
* Wilde’s femoral focus on rentboy stickling °; 
* some benefits of packing an improbably large crossbow °; 
* the surprising trouble with faking The Loco Eyes °; 
* the tactical defense strategy of misquoting Larry Wall; 
* finding the proper cave for Cartoon Billy Barty°; 
* flying a rainbow flag of convenience °; 
* why every arsenal should make room for a mildly inconvenient rose bush °; 
* the uncanny effectiveness of John’s splintered pickets °; 
* learning what John’s been hiding behind that steel-reinforced door.°

The show title refers to Facebook’s algorithm that is compulsively sharing things (it should be spelled share-y, not sherry). 

They start the show singing each other’s names. John thinks somebody should record this and from what he has learned by having a podcast with Merlin there will be two studio recordings of those songs by the time tomorrow rolls around.

+ Merlin is sick (RL7)

Merlin used to smoke a lot of weed, but he can’t do that now. Marijuana does not make him interesting and it just shreds his immune system. Nobody likes a paranoid guy who is coughing a lot. Merlin is sick right now and it is making him a little impenetrable. He is not //that// sick and he still keeps eating half his hot dog, has a Mexican Coke and some water, but he is trying to recover and he is trying to sleep. John had a peanut butter sandwich and tried to finish it before he got on the phone call. Peanut butter has become a very complicated topic for Merlin and he has a lot to ask John about it. You can literally kill people with peanut butter and one of his bullet points is people with peanut allergies.

+ Singing the national anthem (RL7)

Zooey Deschanel sang the National anthem at the [https://youtu.be/_aPvL-lty4Y Game 4 of the World Series 2011] and she did a nice job, Merlin is not crazy about the way she sings, but she did great. She wasn’t goofy, she didn’t Christina Aguilera it by doing the octave thing, she didn’t do any of that bullshit, but she sang the fucking song and it was pretty. Merlin wants to just say to America: If you are going to sing the fucking song, make it pretty. Don’t dick around! If you want to shuck and jive, cover [https://youtu.be/E7t8eoA_1jQ Paid in Full] (by Eric B. & Rakim) and have some fun. They should start doing Paid in Full before sports events!

The Christina Aguilerafication of the National Anthem started with Mariah Carey who ruined everything and has spoiled the start of sporting events for John. Merlin says that one of the few times you can really enjoy a sporting event is when it hasn’t started. It is a time of contemplation, you make a little steeple with your hands, you think ”Aha, yes, sports” and you put your hand over your heart or you take your hat off, but now somebody is strangling a bag of cats and think they are fucking Aretha Franklin. Stop doing that! John is glad that Zooey didn’t mangle it because she is a classy lady. Merlin finds her cute as a bug’s ear, but sometimes... anyway!

+ John feuding with half the world without them knowing (RL7)

The problem with social media for John is that it is ruining his brain. He is an old man now and his brain is being ruined by every additional day it is on Earth. It is bombarded with photons and he is losing his grip anyway. John prefers communicating in text-based applications like emails, texting, tweets, or Facebook-updates to speaking to somebody //(see RW5)//, but the problem is that he is in half a dozen crazy feuds with people. 

John is feuding with everybody, just like Tom Sharpling does. He is feuding with half the world and they don’t even know it is happening because he sends out a text and he has some expectation what kind of text he wants to get in return, and when he gets a different kind of text, like when people just say ”Fine!” or ”See you there!”, it is the beginning of the photon steam which is a Physics problem. Merlin says "Plasticity!" You know everything when you are young. 

+ People in Canada (RL7)

Merlin is turning into the person that would be the result of Erma Bombeck and Andy Rooney getting all fucked up and having a kid, which is sick! She died a while back, but that doesn’t mean that Merlin is going to die. Merlin learned about that from Malcolm Gladwell, the Canadian hair fakir. Over 40% of everyone in Canada will die and it potentially could be going up all the time. John has friends in Canada, but there are some people in Canada he wouldn’t miss. Merlin likes most of the people he has met from Canada. There are seven kinds of bacon in Canada!

+ Ethnicity of people working in different restaurants (RL7)

One time John went into a Mexican restaurant in Canada staffed entirely by Anglosaxon-Canadians, which was weird. They got it backwards [https://www.nytimes.com/2011/02/23/nyregion/23chinatown.html from the thing] where Chinese food was done by Mexican people. Some jobs in the city there have been held by immigrant populations for so long that it is a shock to the mind when you get into the more rural areas over the mountains in Washington state and see a white teenage kid working at one of these jobs. Being a dignified steak waiter is a disappearing tradition. Merlin knows somebody who is a waiter as an old man, which is amazing, but it didn’t used to be that, and it also wasn’t amazing that a white kid would be pushing a broom in a restaurant. The last time you saw a white kid push a broom in Seattle or San Francisco was the 1970s. 

Merlin was in a hotel recently and the housekeeper was 100% caucasian, but she was probably from Romania or Ukraine, which would be hot. Merlin assumed it was an Air Marshal thing and she was undercover. He hopes she was watching him because he danced like she was watching, but did he also sing like no-one could hear?

+ German sex tourists in Romania (RL7)

One time John was in a hotel in Romania and the housekeeper who came in to change the sheets was wearing fishnet stockings. John was sitting in the chair as she was changing the sheets and as she was bending over at the waist to change the sheets on the bed, he could see that a part of her fishnet stockings in the upper back area was ripped to the point of being missing entirely. Using his powers of divination he began to suspect that she was a multi-purpose housekeeper, but John was very busy at that time with a project of his own and so he just shooed her away. This was a very off-the-beaten-path part of Romania and most of her normal guests were other Romanians or German sex tourists. People often mistake John for a German sex tourist when he is overseas, especially when he goes to Thailand with rolls of quarters. For some reason he just looks like one and when actual German sex tourists see John in foreign countries they always sidle up to him and say ”So, how are you going?” It is crazy over there!

Merlin sometimes accidentally stumbles over German porn and he doesn’t understand how babies ever get made in Germany because their entire thing is to hit each other and poop. Merlin does not want to see that and he has tried to put things in place to see less of that, but you just got to get comfortable with the fact that you are going to see a lot of cocks and Merlin is fine with that. Seeing a big old [https://www.urbandictionary.com/define.php?term=beaver beaver] when he was just trying to find a record by The Kinks is fine, but he doesn’t want to see a Bavarian poop face because he can’t unsee that. 

+ Elephant 6 (RL7)

John loved those guys of Elephant 6 before they all went to live under a porch somewhere. Merlin doesn’t think Olivia Tremor Control lives under a porch, but they changed their name and got a Theremin. After two records  Elephant 6 lived under a porch for 10 years. Then All Tomorrow’s Parties called you back up and would be the greatest thing that ever happened. 

Merlin thinks that John is the Bruce Vilanch of his vertical space, but John doesn’t like that analogy at all and if another person says Bruce Vilanch around him, he is going to come through the radio and pop them in the nose. He is going to have a hissy fit and change his shirt!

+ Merlin running out of index cards (RL7)

Merlin needs more 3x5 index cards because he wants to come back to so many things. He wants to talk about magical thinking, but he doesn’t want to talk about 4Chan or the tea party, and he feels like he needs to write more things down. He doesn’t want to interrupt John, because his foil is important.

+ John learning about social media and the Internet (RL7)

John has become very enamored with social media and he does now understand things about it that he didn’t used to. At one point John called Merlin on the phone, back when he used a phone, and asked Merlin why The Long Winters would need a website and Merlin laughed and laughed. It is like trying to explain to Ben Franklin why he might want an iPhone 4s instead of an iPhone 4. A lot of technological dependencies needed to be unpacked and Merlin unpacked them for John over the course of many long hours of meetings that then had to be rescheduled until he eventually managed to explain why they needed a website. Merlin imagined John reading off a card with reading glasses at the end of his nose. 

Fast forward several years and John is a maven of social media. He is a style-maker, like that big black gay guy at Vogue who used to be on the show with Cindy Crawford. Two minutes into this podcast Merlin already said ”Big Black Gay Guy”. It makes Merlin sad that John doesn’t even need that site that much anymore because everything is happening on social media sites, which is a bummer. Are we in a post-website world?

Back when Merlin used to have jobs, he learned that when you want to do something and think it is a good idea, one of the things you must do is offer to do it yourself. If you have to convince some bonehead, then you need to make them think it was their idea. With John it was useful to show how something on the continuum makes him more rather than less powerful and the pitch that made John want to create a website was control: John wanted to control the message. John decided what it would look like and John wrote wonderful things for the site. Merlin is bothered by the Facebook or Twitter thing because it is ephemeral. There is not much incentive to try hard and one day it kind of just goes away. You get to control your message and be a big douche in front of people, but you don’t really own it in the way you do with your own site.

Whenever John says anything about social media he is being facetious. Merlin likes the idea of social media as a place where you can talk to people, but it should be in an environment that is conducive to talking to people. Talking to people on Twitter is weird. If you have a modest number of followers and you are just there to talk to each other, why wouldn’t you use AIM or IRC something for that? It is maybe fine if you want to have a public conversation with somebody. The Postal Service, like the Zooey Deschanel husband (Ben Gibbard). 

+ John going further out from Wikipedia (RL7)

John used to hear that phrase ”I can’t unsee that” a lot and wondered what people were talking about because he has never been witness to a genocide and there is nothing he wants to unsee, but during the last six months he has started trailing the Internet in a way he never used to do. In the past he would just go to Wikipedia, read it all night and then put on some slippers and take a bath, but now he is actually going further out. John keeps bringing on 4Chan, which is a big ”unsee it” kind of place. Merlin thinks that it doesn’t make a lick of sense that John keeps looking at that and he might have a virus. John counters that he has a Mac which is impenetrable to viruses, but Merlin meant a virus in John’s brain.

John is not just on 4Chan, but from there he goes out into the darker world. There are terrible things on the Internet and John thinks to himself that he does wish he could unsee that and unthink some ideas. You can sit and think about stuff, like the Silence of the Lambs, and say ”Oh, it puts the lotion on the skin”, but then you can see people out in the world acting out these fantasies, even if those videos are fake. John saw a video the other day of a Mexican gangster cutting a guy’s head off with a chainsaw while the guy was still alive. John doesn’t want to see that! 

John finds the idea of Anonymous very appealing because everything everybody is doing on the Internet right now is a form of ”Look at me! Look at me!” There are many concentric circles of famous and everybody is trying to expand their circle of famous to the next larger circle of famous. John has tweeted that to John Hodgman and his wife 60 times. It is the whole premise of ”Follow me! Look at me! Look what I did!” and the idea of Anonymous and there not being a way to accrue fame to themselves is appealing to John because maybe there is some genuine material being created. Anonymity wipes away some of the uselessness of what people are saying. Even when John reads something really good these days, he has to contextualize it within the framework of ”This person said that in order to direct attention to their Tumblr page which eventually they are trying to sell ads on” or something. 

Merlin is not that old and he understands why social media is fun and he is good at parts of it, but the whole checking into Foursquare thing that goes into Facebook is bullshit all the way down. Everybody wants to be known, liked and admired in some way, while John just wants to be feared, he should cut off some dude’s head with a chainsaw and he could have a show called DecencyBusters, a show where you just do all kinds of appalling stuff while being anonymous. John could wear a beret.

Somebody posted a thing on John's Facebook wall, John replied to it and got a phone call 20 minutes later from somebody pissed off about some unconfirmed piece of information that was posted by a relative stranger and was spread by Facebook's compulsive share-y algorithm (Merlin hears "Compulsive Sherry Algorithm" and thinks that it sounds like a record by Elephant 6). That somebody was close enough to John to have his phone number, saying ”I read this thing that came up in my Facebook feed” and John was like ”Christ!” and he immediately changed all the settings so that it wouldn't share anything with anybody. It is now a completely closed system that only he can see and he is just going to go in there running around naked and throwing his poop in the air.

John watched some freaking kids on 4Chan who ruin people’s lives, which is the whole idea and which Merlin thinks they need to stop talking about it right now. Your phone puts a little GPS code on every picture you take and there are people out there who will come to your house! Somebody put out a really creepy app that is a fascinating proof of concept. People their age don’t get or don’t love this extremely interesting technology that is not hacking or cracking anything, but is just showing you what you have done in public. Somebody put out a little regular expression thing that sweeps Twitter for indications if people aren’t at home and they made a webpage where you can find out who is not at their house.

Sometimes it is a shame that we can’t divorce the awfulness of what people are doing from the fact that there is stuff we can learn from. As much as you would like to help everybody, Merlin's concern for John is that he doesn’t want the plasticity of his photons to become too watered down. If John is helping people who can’t be helped in a scalable way, a lot of people are going to miss out being set straight. John is going to get too wrapped up wrestling some fucking [https://en.wikipedia.org/wiki/Tar-Baby tar baby] and he is not going to be able to set people straight that are causing a lot of problems for Merlin in particular. John agrees that he is [https://en.wikipedia.org/wiki/Br%27er_Fox_and_Br%27er_Bear Br’er] Roderick and gets tangled up in a tar baby. It is called Song of the South. 

+ John’s article in Seattle Weekly (RL7)

John [http://www.seattleweekly.com/music/john-roderick-is-a-regular-columnist-for-reverb-his-band-the-long/ wrote an article] for the Seattle Weekly, the Reverb column, and he quoted somebody he had a Twitter exchange with. She had made some point about Feminesium [sic] and that women somehow are second-class citizens. Merlin wonders if Feminesium is an essential oil or something. There was a great cover of Bust Magazine where they said ”It is the 30th anniversary of Feminesium” and misspelled feminism on the cover. John was working at the magazine store at the time and it was so great that he actually kept a copy of it, but he doesn’t know what it said, he wasn’t trying to prove the point that people who put out feminist magazines don’t know how to spell Feminism, but it was just a nice thing, like John’s Braille Playboys, something he likes to keep around.

This gal was angry at John for misquoting her about the Twitter exchange they had where he called her a fake feminist. John talked about that with a Rock musician and they asked ”Oh, is it girl so-and-so?” and it was. They called her out because she is a feminist music critic. John has nothing against her or her Feminesium personally, but now he lost his train of thought. The photons are pouring out of his ears right now, like a photon-fall. Merlin is going to send John some index cards. John hasn’t had index cards in years, not since he was doing his book report on ”The life of a cell” Merlin gave Eric index cards and John might also have given Eric the index cards that Merlin gave him, which is fine, especially in the sense that John is now struggling and who is the mad genius now?

+ Bust, Grand Royal and Dynamite Magazines (RL7)

Bust Magazine is a lady magazine. The name is ironic, and John is not even sure if it is still in print, which would add a further level or irony to the name Bust, because they may have gone bust. There was no Grand Guignol Magazine. Merlin loved Grand Royal Magazine and he still has the Sleep with Scratch Perry issue where Mike D put on a mullet wig and went to GIT. It is so fucking great! There was Mite Magazine (Dynamite) from the guy who wanted kids to ride in the Mission. John was a massive fan of Mite, also Merlin loved it and looked forward to it every time. 

It was the time just before the Internet they pretended that Adam Rich from Eight is Enough had died. It was in the tradition of Spider Eggs in Bubble Yum, or the Little Mikey & Pop Rocks kind of shit in advance of what became Internet-based pranking. It was real-in-the-flesh pranking in a Magazine, it wasn’t Onion-y in the sense that the headlines were funny but the stories weren’t, but it seemed plausible and they talked to people. If you stepped one foot back from Mite Magazine, you realized that everything in it was a gag. They had themes for every issue. 

When John was working at the magazine store he had every original Dave Eggers publication in his hands, like the first edition McSweeny’s issue number one. He could probably buy a small economy car with all that stuff if he had the presence of mind to just keep it instead of giving it away, or he could buy a couple of boys if he would talk to his German friends, they have probably Das Amazon Das Prime for that.

+ John educating young boys (RL7)

There was a time when John tried to be attracted to boys in a [https://en.wikipedia.org/wiki/Gymnasium_(ancient_Greece) Greek Gymnasium] kind of way, but it just didn’t take. John can’t really be a public intellectual unless a part of him wants to educate young boys, but John has a very quick mind and is very impatient with children. He would be sitting around and start talking about British Steel (the album) and they would want to [https://www.urbandictionary.com/define.php?term=Fillet fillet] him, but John would rather talk about this. John doesn’t think a young man can come to adulthood without knowing about Judas Priest. They wouldn’t even call it ”fillet”, but ”candy penis bang bang” or something. 

When John was a teenage boy he never asked an adult if they would like him to do anything. He didn’t even do his homework! Kids asking adults ”May I fillet you, sir?” is another example of an adult fantasy world. Kids these days are playing their Playstation 2s and the adult has to come in and actually whack them on the head with the penis and say: ”Now!” None of that interests John, but those kids need to learn more about Judas Priest and the Thirty Years’ War. 

John has said it before that Merlin would have needed that strong male figure, not in a pulling-down-the-sweatpants kind of way, not somebody to knock him on the head with his penis, but Merlin would have been the student and he would not be in a position to say ”I don’t want to learn that kind of math! Science is more important than history, so why bother? Could you please stop hitting me literally with your penis?” Merlin would not be the one who gets to pick the syllabus for that. He likes to reserve reading, but on some level sometimes you have to kneel to rise. 

If John decided that the lesson planned for today was a little bit of penis-to-forehead-contact, you don’t get to choose which parts of military school you like, which parts are fun and which parts you want to take a pass on. Merlin was on the drill-team in military school, not with the girls with the big thighs who spin the batons, but he marched in close order with a gun, like the movie Taps, but you do not want to show a bunch of boys in military school a movie set in a military school. Taps is a little bit older than when Merlin was in military school, but ”Damien: Omen II” involved military school, or Up the Academy (the MAD Magazine movie). 

What is interesting to John about the pederasty thing is that neither John nor Merlin were gay teenagers, but Merlin was almost asexual. There shouldn’t even be the word sex in it and even the masturbation he was doing was so depraved, it was not even on a sexuality level. It certainly was not procreative, but he was just performing frottage on a hollowed-out tree. It was just anger! There may be a lot of 25-year old towels running around in Florida that Merlin is responsible for and he hasn’t paid for school or done any of that.

+ Critiquing the continuity and realism of movies (RL7)

In the first season of his show, Letterman would do movie reviews. Merlin remembers when he hosted a movie review of Flash Dance done by a professional welder and he wasn’t over the top. He said for example that he wouldn’t want to do that job having long black hair floating out of the helmet, or that there is no way the light would be like that, it is the wrong temperature for that kind of metal. If you ask the people who understand what is on screen, they will tell you whether it makes any sense or not. They are going so say ”That guy has an SS uniform when he is actually in the Luftwaffe”, that would be John. John knows a German uniform. There are also a lot of posers out there who just get away with it. 

Merlin would watch military school movies and would say ”That is bullshit, that would not happen that way, you guys were not in line, those were just actors fucking walking around in pants, that is not marching!” Merlin thinks that a lot of pederasty in literature and culture is a little bit fake. It is kind of like that great Tom Wolfe essay called [https://en.wikipedia.org/wiki/Radical_Chic_%26_Mau-Mauing_the_Flak_Catchers Radical Chic & Mau-Mauing the Flak Catchers] where Leonard Bernstein had the Black Panther benefit. It is really great and a total indictment of what //(inaudible 38:00)//nowadays, but he got in a lot of trouble for it because the tone of it was not appropriate even then. You don’t fuck with Leonard Bernstein!

John has noticed lately that he has been pausing movies and saying ”This is a hollow-core door! Whoever built the set of this movie didn’t get a solid wood door, but they didn’t have hollow-core doors in this era” Then he would push play again, watch it for a little while, pause it again and say ”That type of counter-top hadn’t been invented when this was supposedly filmed!” and he is driving himself nuts! There is a whole thing with oopses on IMDB, Merlin doesn’t even know why he looks at that because it makes him angry because he hates when people do that and when he does it himself. All kinds of movies he loved as a kid have continuity-errors. Go back and try to watch Stripes again! It is a fun movie, but the continuity is complete bullshit. 

+ Bill Murray movies shot on cocaine (RL7)

//see [[[Movies]]]!//

+ Letters of Note (RL7)

Merlin likes the site [http://www.lettersofnote.com/ Letters of Note] a lot where they post pieces of interesting correspondence every day. There are 4-5 [http://www.lettersofnote.com/search?q=%E2%80%9D+Hunter+s.+Thompson%E2%80%9D&max-results=20&by-date=true really good ones] from Hunter S. Thompson. Johnny Depp had been acquainted with Hunter S. Thompson and Merlin bought some of the old cassettes, they are out there, where he recorded himself just thinking out loud. Johnny Depp really captures that weird combination of relaxed swagger and complete paranoia, the shifting eyes and everything. He is a good actor who has made some terrible movies, but on the whole John has no complaints. Thompson would fire off these faxes to people, there is [http://gothamist.com/2011/08/17/when_hunter_s_thompson_penned_rolli.php one to Rolling Stone] that Merlin reads during the show. Merlin has read the book where Father Guido Sarducci sends letter (probably the one called The Lazlo Letters).

+ John going back from Gore-Tex to wool (RL7)

At a certain point John rejected Gore-Tex and went back to wool, which literally was a watershed moment for him (because it sheds water). Merlin asked if John was sure he was not a pederast and John confirmed that he had six 14-year old boys sitting around him Indian Style in a semi-circle, all playing Playstation 2s and none of them was listening to him at all.

A lot of people in the Northwest have shunned wool and are in a complete Gore-Tex universe, thinking it is Northwestern attire, while John is back to wool, back to analog, back to mono! Merlin bought Gore-Tex shoes and they don’t breathe. Although he has a lot of problems and being a stinky foot person is not one of them, it was still brutal. He always sands his shoe trees to put them back to life and he puts them into these shoes, but even if he sticks them into a sealed bag with baking soda he still feels like he is only taking off 20% of the problem. John thinks Merlin should just throw his shoes away and buy some nice old leather shoes.

+ John reflecting on his career (RL7)

The other day John was sitting in the bathtub, thinking about his own career. He has made a total wreckage out of it in place of an ideal career arc where he would have built on what he has made and added a new level to it. He would have seen his enemies driven before himself and heard the lamentations of their women //(quote from Conan the Barbarian, variation of quote from Genghis Khan)//. Instead John spent a lot of time in the garden doing fence work and a lot of time muttering to himself. He not just ruined his career, but he has done a terrible job managing his whole life.

He thought that maybe he should just go in the same direction as Hunter S. Thompson and just let it go, give everybody both barrels all the time and descend into lonely madness while leaving a legacy of truth. Merlin thinks it is too late for that because John has caused a lot of fucking problems for people and his version of the truth is so penetrating for a lot of people. All these 14-year old boys just sat up straight and were looking John right in the eyes when Merlin said ”penetrating”. John could threaten to unplug that game any time and plug something else. If John wanted to become like a Hunter S. Thompson he would need a compound, but the place where he lives is already a compound.

Merlin talks about [https://en.m.wikipedia.org/wiki/Oscar_Wilde Wilde] and [https://en.wikipedia.org/wiki/Lord_Alfred_Douglas Bosie], the [https://en.wikipedia.org/wiki/John_Douglas,_9th_Marquess_of_Queensberry Marquess of Queensberry]'s son who’s thigh Wilde was fucking. Wilde wanted that thigh and the whole reason he started that trouble with the Marquess of Queensberry was because he misspelled Sodomite (as ”Somdomite”). There is [https://www.amazon.com/Oscar-Wilde-Richard-Ellmann/dp/0394759842 a great biography by Richard Ellmann]. 

Wilde wanted to fuck the guy’s son in his knee because he had a leg-fucking thing. Merlin doesn’t understand all the different angles of being gay, but Wilde was a leg- and leverage man and liked frottage. His life was fucking complicated! Oscar Wilde was an essayist and a playwright, he was a knee-fucker and he didn’t like spelling errors. This was literally the 1800s and he was a stickler. He certainly stickled with rent boys, which was Merlin’s favorite Smissy piece: Stickling the rent boys. Merlin admires the fact that John is owning his mismanaging. 

+ John walking around his perimeter in a bathrobe with a sword (RL7)

//see [[[Sword and Bathrobe]]]!//